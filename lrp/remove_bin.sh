#!/usr/local/bin/bash
# Files to will be removed

find . -type f -name "*.bin" -exec rm {} \;
find . -type f -name "*.*~" -exec rm {} \;
find . -type f -name "*~" -exec rm {} \;
find . -type f -name "*.bak" -exec rm {} \;
find . -type f -name "*.tmp" -exec rm {} \;
find . -type f -name "lib" -exec rm {} \;
find . -type f -name "names" -exec rm {} \;
