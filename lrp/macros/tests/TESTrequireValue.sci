ERROR="** ERROR **";
printf("============= requireValue =================\n");
testCase(ERROR,"requireValue","val",123,123); // last argument must be a list
testCase(%t,"requireValue","val",123,list(123));
testCase(ERROR,"requireValue","val",123,"integer");

//the last argument should  here be a "list", but not a "vector"
testCase(ERROR, "requireValue", "val", "point", ["point","pass"]);

testCase(%t, "requireValue", "val", "point", list("point","pass"));
testCase(ERROR, "requireValue","x",1,list([1,2,3])); // I want a vector, NOT 3 integers
testCase(%t, "requireValue","x",[1,2,3],list([1,2,3]));
