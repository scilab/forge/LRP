function result=%mtx_ones(a_mtx)
 // eye(m)
 result=a_mtx;
 result.mat=ones(a_mtx.mat);
endfunction
