function %mtx_p(varargin)
// displays the mtx type variable
//
// CAUTION: No error checks when called with 4 arguments, this will speed things up.
// 	select length(varargin) 
// 		case 1 then
// 			printf('is a CoolMatrix (');
// 			idxMin=varargin(1).idxMin;
// 
// 			printf('%d..%d',idxMin(1), idxMin(1)+size(varargin(1).mat,1)-1);
// 			for q=2:length(idxMin)
// 				printf(', %d..%d', idxMin(q), idxMin(q)+size(varargin(1).mat,q)-1);
// 			end
// 			printf(')\n');
// 			disp(varargin(1).mat);
// 		else
// 			error('%mtx_p: NOT YET IMPLEMENTED')
// 		end




// function %mtx_p(varargin)
// displays the mtx type variable
//
// CAUTION: No error checks when called with 4 arguments, this will speed things up.
	select length(varargin) 
		case 1 then
			printf('is a CoolMatrix (');
			idxMin=varargin(1).idxMin;
			idxMax=varargin(1).idxMax;

			printf('%d..%d',idxMin(1,1), idxMax(1,1));
			for q=2:length(idxMin)
				printf(', %d..%d', idxMin(q), idxMax(q));
			end
			printf(')\n');
			oldResult="";
			oldResultForDisp="";
			delimeter="";
			idx=[idxMin', idxMax'];
			if size(idx,1)<=2 then
				// Easy stuff - just a 2D matrix. Display it and finish
				disp(varargin(1).mat);
				return
			end
			// We do not want to see the first 2 indices, as we can easily show a 2D matrix			
			idx=idx(3:$,:);
			depth=size(idx,1);
		case 4 then
			// Internally used for the recurrence call.
			oldResult=varargin(2);
			oldResultForDisp=varargin(4);
			idxMin=varargin(1).idxMin;
			idxMax=varargin(1).idxMax;
			idx=[idxMin', idxMax'];
			depth=varargin(3);
		else
			error(sprintf('Call this with 1 argument. You gave %d.',length(varargin)));
	end
	if oldResult=="" then
		delimeter="";
	else
		delimeter=",";
	end

 	if depth<=1
 		a_matrix=varargin(1).mat;
 		//We are on the last index - display the result
 		for q=idx(1,1):idx(1,2)
 			printf("\n(:,:,%d%s%s)\n",q,delimeter,oldResultForDisp);
 			execstr(sprintf('disp(a_matrix(:,:,%d%s%s))',q-idx(1,1)+1,delimeter,oldResult));
 		end
 	else
		//We are somewhere in the middle - add an index and move one level down
		for q=idx(depth,1):idx(depth,2)
			%mtx_p(varargin(1),string(q-idx(depth,1)+1)+delimeter+oldResult,depth-1,string(q));
		end
	end
endfunction




//endfunction
