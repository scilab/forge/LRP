function result=%mtx_ceil(a_mtx)
 // ceil(m)
 result=a_mtx;
 result.mat=ceil(a_mtx.mat);
endfunction
