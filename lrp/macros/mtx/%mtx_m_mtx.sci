function result=%mtx_m_mtx(mtxLeft,mtxRight)
	// mtxLeft*mtxRight
	//
	//  Let size(mtxLeft.mat)=[a,b,[c]] and size(mtxRight.mat)=[d,e,[f]]  
	// We can multiply 2 matrices if b=d and the corresponding c=f, if c exists 
	//   (i.e. ndims(mtxLeft)=ndims(mtxRight)>2 

if or(size(mtxLeft.mat,2) ~=size(mtxRight.mat,1)) then
	if ndims(mtxLeft.mat)>2 | ndims(mtxRight.mat)>2 then
		error(sprintf('Wrong sizes. Need: L: [#,%d,*], R: [%d,#,*]. Got: L: %s, R: %s.', ...
			size(mtxLeft.mat,2), size(mtxLeft.mat,2), ...
			sci2exp(size(mtxLeft.mat)),sci2exp(size(mtxRight.mat)) ...
		));
	else
		error(sprintf('Wrong sizes. Need: L: [#,%d], R: [%d,#]. Got: L: %s, R: %s.', ...
			size(mtxLeft.mat,2), size(mtxLeft.mat,2), ...
			sci2exp(size(mtxLeft.mat)),sci2exp(size(mtxRight.mat)) ...
		));
	end	
end
if or(length(size(mtxLeft.mat))~=length(size(mtxRight.mat))) then
	error(sprintf('Number of dimensions must be equal. Got: L: %d, R: %d.', ...
		length(size(mtxLeft.mat)), length(size(mtxRight.mat)) ...
	));
end

// if mtxLeft.idxMin(2)~=mtxRight.idxMin(1) | or(mtxLeft.idxMin(3:$)~=mtxLeft.idxMax(3:$)) then
// 	if length(mtxLeft.idxMin)>2 then
// 		error(sprintf('Wrong idxMin indices. Need: L: [#,%d,*], R: [%d,#,*], got: L: %s, R: %s', ...
// 			mtxLeft.idxMin(2), mtxLeft.idxMin(2), sci2exp(mtxLeft.idxMin), sci2exp(mtxRight.idxMin) ...
// 		));
// 	else
// 		error(sprintf('Wrong idxMin indices. Need: L: [#,%d], R: [%d,#], got: L: %s, R: %s', ...
// 			mtxLeft.idxMin(2), mtxLeft.idxMin(2), sci2exp(mtxLeft.idxMin), sci2exp(mtxRight.idxMin) ...
// 		));
// 	end
// end

    result=mlist(["mtx","mat","idxMin"],mtxLeft.mat*mtxRight.mat, [mtxLeft.idxMin(1),mtxRight.idxMin(2:$)]);	
endfunction 
