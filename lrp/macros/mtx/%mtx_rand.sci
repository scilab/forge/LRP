function result=%mtx_rand(a_mtx)
 // rand(m)
 result=a_mtx;
 result.mat=rand(a_mtx.mat);
endfunction
