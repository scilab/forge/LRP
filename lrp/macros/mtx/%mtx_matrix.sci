function [mtx]=%mtx_matrix(mtx,varargin)
 if length(varargin)==2 then
    sizes=[varargin(1),varargin(2)]
 else
     if length(varargin)==0 then
        error('Usage: matrix(mtx,sizesVector); See help matrix for details.');
        return;
     else
         // Caution: varargin can contain a legal value of (-1)
        sizes=varargin(:);
     end
 end
 if (or(int(sizes)~=sizes)) then
     // User might have given us a vector of real values - this 
     //  is incorrect, so we will issue an error
     requireType("sizes",sizes,"integer vector");
 end
 try
    mtx.mat=matrix(mtx.mat,sizes);
 catch
     // We have no idea, why the matrix function failed. 
     //   First of all we will check the argument
     requireType("sizes",sizes,"integer vector");
     error(lasterror());
 end
endfunction

