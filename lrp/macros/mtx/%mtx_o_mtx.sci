function result=%mtx_o_mtx(mtxA,mtxB)
// mtxA==mtxB
// CoolMatrices are equal if their .idxMin are equal and their .mat are equal
   resultMat=(mtxA.mat == mtxB.mat);
   resultIdx=(mtxA.idxMin==mtxB.idxMin);
   if length(resultMat)==1 then
      result = (resultMat & resultIdx(1) & resultIdx(2));
      return;
   else
       result = resultMat & and(resultIdx);
   end
endfunction
