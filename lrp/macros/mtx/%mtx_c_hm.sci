function result=%mtx_c_hm(leftMtx, a_matrix)
// [m m]  
// size(leftMtx,1) must be equal to size(a_matrix,1)
//  Furthermore the size(leftMtx,3:$) must be equal 
//  to size(a_matrix,3:$)  
result=%mtx_c_s(leftMtx, a_matrix)
endfunction
