function result=%mtx_round(a_mtx)
 // round(m)
 result=a_mtx;
 result.mat=round(a_mtx.mat);
endfunction
