function result=%mtx_e(varargin)
// Extraction function
//
// mtx.Field
// mtx(1,2)
// CAUTION: DOES *NOT* WORK FOR mtx(1)

// By assumption, we get the mtx structure as the last argument
//   therefore we refer to it as "varargin($)"
//disp('%mtx_e')
// First, check whether we have a field name given as a string
if type(varargin(1))==10
	// Yes, we will check which field do we want
	select varargin(1)
		case "dim" then 
			result=[varargin($).idxMin',varargin($).idxMax'];
		case "idxMax" then
			// This werid stuff is needed because of reduction of the
			//   hypermatrices dimension. When it has a last index of 1 it is dropped.
			//   We do not want this here.
      result=varargin($).idxMin+ [ ...
				size(varargin($).mat), ones(1,length(varargin($).idxMin)-length(size(varargin($).mat))) ...
			]-1;
		else
      printf("\n******************\nWrong field name ''%s'' in the CoolMatrix type\n",varargin(1));
      printf('Allowed fields are: \n');
      fields = getfield(1,varargin($));
      disp([fields(2:$),"idxMax","dim"]);
      printf("\n");
      result=%nan;
			error(144);
	end
	// Nothing more to do here...
	return;
else
	// No, this is an index
	//
	// First, we will check the number of indices. 
	//    Caution: The last varargin (i.e varargin($)) is a mtx variable, 
	//             NOT the index 
	
	// Make the indices first
	idxMin = varargin($).idxMin;
// 	if length(varargin)>2 then
// 		if length(varargin)-1~=length(idxMin) then
// 			error(sprintf('Too many indices. Got: %d, Need 1 or %d.', ...
// 				length(varargin)-1,length(idxMin) ...
// 			));
// 		end
// 	else
// 		// We allow one index for vectors only. Vector is a 
// 		//	2D matrix that has size equal to [1,x] or [x,1] 
// 		if ((idxMin(1)~=1 & idxMin(2)~=1) | length(idxMin)~=2) then
// 			error(sprintf('One index is allowed only for 2D vector. Call ans(*) with %d indices.', ...
// 				length(idxMin) ...
// 			));
// 		end
// 	end
// 	idxMax = varargin($).idxMax;
// 	for idx=1:length(varargin)-1
//     currentIndex = varargin(idx);
// 		// Check if this is x(sth, :, sth) 
// 		if size(currentIndex,1)~=-1 then
// 			// Is the index valid?
// 			if currentIndex>=idxMin(idx) & currentIndex<=idxMax(idx) then
//         // yes - calculate the new index.
//   			idxMin(idx)=min(varargin(idx));
//   			// modify the indices to the actual - from 1 to length(idxMin)
//   			varargin(idx)=varargin(idx)-idxMin(idx)+1;
// 			else
// 			    printf('\n****************\n');
//           suffix = ["th","st","nd","rd","th","th","th","th","th","th"];
// 			    printf('Error: %d%s index is invalid. Got: %d, allowed range: <%d..%d>, inclusive.',idx, suffix(modulo(idx,10)+1),currentIndex, idxMin(idx), idxMax(idx));
//           printf('\n****************\n');
//           result=%nan;
// 			    error(21);
//       end
// 		end
// 	end
// 	// Check if the user supplied us with an allowed index...
//   siz = size(varargin($).mat);
// 	for dim=1:length(varargin)-1
// 		// As I do not know of any way of checking for the "eye *" directly, 
// 		//  therefore I have to use the "size" trick - this stuff has always 
// 		//  size of MINUS ONE (-1) 
// 		vararginDim = varargin(dim);
//     if and(size(vararginDim)~=[-1,-1]) then  
// 			if vararginDim<1 | max(vararginDim)>siz(dim) then
// 				error(sprintf('Wrong index of dimension %d. Is: %s, Allowed range: <%d, %d>, inclusive.', ... 
// 											dim,sci2exp(vararginDim+idxMin(dim)-1), ...
// 											idxMin(dim),idxMin(dim)+siz(dim)-1 ...
// 										));
// 				result=%nan;
// 				return;
// 			end
// 		end
// 	end

  //resultMat=result.mat(varargin(1:$-1));
//	result=varargin($);
//  result.mat=varargin($).mat(varargin(1:$-1));
//  mat = varargin($).mat(varargin(1:$-1));
//  disp(result.mat);
//  disp(mat);  
  //result.mat=resultMat(varargin(1:$-1));
//	result.idxMin=idxMin;
//   result = new_mtx(idxMin,varargin($).mat(varargin(1:$-1)));

//result=mlist(["mtx","mat","idxMin"],varargin($).mat(varargin(1:$-1)), idxMin);
//result=mlist(["mtx","mat","idxMin"],varargin($).mat(varargin(1:$-1)), idxMin);
result=mlist(["mtx","mat","idxMin"],varargin($).mat(varargin(1:$-1)), idxMin);
end
endfunction
