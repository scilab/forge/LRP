function result=%mtx_c_s(leftMtx, a_matrix)
// [m m]  

// size(leftMtx,1) must be equal to size(a_matrix,1)
//  Furthermore the size(leftMtx,3:$) must be equal 
//  to size(a_matrix,3:$)  
	leftSize=size(leftMtx);
	rightSize=size(a_matrix);
	if leftSize(1)~=rightSize(1) | or(leftSize(3:$)~=rightSize(3:$)) then
		if length(leftSize)>2 then
			strLeftMtxSize = sci2exp(leftSize(3:$));
			if length(strLeftMtxSize)>1 then
				strLeftMtxSize = part(strLeftMtxSize,[2:length(strLeftMtxSize)-1]);
			end 
			error(sprintf('Wrong right-hand size. Need: [%d,#,%s], got: %s.', ...
				leftSize(1),strLeftMtxSize, sci2exp(rightSize) ... 	
			));
		else
			error(sprintf('Wrong right-hand size. Need: [%d,#], got: %s.', ...
				leftSize(1), sci2exp(rightSize) ... 	
			));
		end			
	end		
	result=leftMtx;
	result.mat=[leftMtx.mat a_matrix];

endfunction
