function result=%mtx_zeros(a_mtx)
 // zeros(m)
 result=a_mtx;
 result.mat=zeros(a_mtx.mat);
endfunction
