//global LRP_OPTIONS

//OS
if (MSDOS) then
    lrp_OS='w';
else
    lrp_OS='u';
end

//load pathes to folders which contains functions in the library
try
    exec(fullfile(LRP_OPTIONS.path, "/macros/library/paths.sce"));
catch
    printf("\n**********************************************************************\n");
    printf("lrp/macros/library/loadLibrary.sci:\n");
    printf("WARNING: Error executing\n   %s\n",fullfile(LRP_OPTIONS.path, "/macros/library/paths.sce"));
    printf("Error:\n   %s\n",lasterror());    
    printf("Consider reinstalling the Toolkit\n");
    printf("**********************************************************************\n");
    error(lasterror());
    return;    
end
lrp_howManyLibraries=size(lrpLibrary.folder,1);

//create full path to library
for lrp_i=1:lrp_howManyLibraries
  lrpLibrary.folder(lrp_i)=pathconvert(fullfile(LRP_OPTIONS.path, lrpLibrary.folder(lrp_i)),%t,%f,lrp_OS);
end

//FIRST CLEAR
//clear if there are old copies of functions in RAM of these library
try
    exec(fullfile(LRP_OPTIONS.path, "/macros/library/cleanFunFromRam.sci"));
catch
    printf("\n**********************************************************************\n");
    printf("lrp/macros/library/loadLibrary.sci:\n");
    printf("WARNING: Error executing\n   %s\n",fullfile(LRP_OPTIONS.path, "/macros/library/cleanFunFromRam.sci"));
    printf("Error:\n   %s\n",lasterror());    
    printf("Consider reinstalling the Toolkit\n");
    printf("**********************************************************************\n");
    error(lasterror());
    return;    
end
//SECOND LOAD
//load new library
lrp_i=1;
while (lrp_i<=lrp_howManyLibraries)

	  //check that all libraries exists - files 'lib' and 'names' must exists !!!
   [lrp_fd, lrp_err]=mopen(fullfile(lrpLibrary.folder(lrp_i), "lib"), "r");
   if (lrp_err~=0) then
      printf("\n**************************************************\n");
      printf("lrp/macros/library/loadLibrary.sci:\n");
      printf("WARNING: Library\n   %s\ndoes not exist!\n\n",fullfile(lrpLibrary.folder(lrp_i), "lib"));
		printf("Attempting to recreate library...\n");
      printf("**************************************************\n");

// Create the library by calling an appropriate script
      try
          exec(fullfile(LRP_OPTIONS.path, "/macros/library/createLibrary.sci"),-1);
      catch
          printf("\n**********************************************************************\n");
          printf("lrp/macros/library/loadLibrary.sci:\n");
          printf("WARNING: Error executing\n   %s\n",fullfile(LRP_OPTIONS.path, "/macros/library/createLibrary.sci"));
          printf("Error:\n   %s\n",lasterror());    
          printf("Consider reinstalling the Toolkit\n");
          printf("**********************************************************************\n");
          error(lasterror());
          return;    
      end
      // Library should be created at this point       
      break;
   end
   mclose(lrp_fd);

   [lrp_fd1, lrp_err]=mopen(fullfile(lrpLibrary.folder(lrp_i), "names"), "r");
   if (lrp_err~=0) then
      printf("\n**************************************************\n");
      printf("lrp/macros/library/loadLibrary.sci:\n");
      printf("WARNING: Library\n   %s\ndoes not exist!\n\n",fullfile(lrpLibrary.folder(lrp_i), "names"));
		printf("Attempting to recreate library...\n");
      printf("**************************************************\n");
		exec(fullfile(LRP_OPTIONS.path, "/library/createLibrary.sci"),-1);
      break;
   end
   mclose(lrp_fd1);

   //load library
   load(fullfile(lrpLibrary.folder(lrp_i), "lib"));
	 lrp_i=lrp_i+1;
end
clear lrp_i lrp_OS lrp_howManyLibraries lrpLibrary lrp_fd lrp_fd1 lrp_err fileNameDirectory
