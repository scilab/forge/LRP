function AddHelpChapter(strDirName)
//  This function adds a new help chapter, it effectively is the improved 'add_help_chapter'
//     Scilab function
// *** UNSAFE *** - no parameter checks are defined
//
// Parameter
//strDirName - name of LRP subdirectory ('misc', 'control', etc.) The help files
//  are added from the 'help'+strDirName directory.
//This function is needed as add_help_chapter() issues a warning about the 'title' function.
//  As this warning is useless and can scare the user, it is silently turned off FOR THIS FUNCTION ONLY
//  After this function ends all the protections are exactly as they were - no changes are done.

	printf('  '+strDirName+'...');

//First, make sure we have a valid HTML files
[x,isErr]=fileinfo(fullfile(LRP_OPTIONS.path, "/help/"+strDirName+"/whatis.htm"));
if (isempty(x)) then
	prot=funcprot(); //Get the default protection
	funcprot(0);     // Do not issue warnings about redefining function names
	try
	   add_help_chapter("LRP "+strDirName,fullfile(LRP_OPTIONS.path, "/help/"+strDirName));
	catch
	  // Ooops - something went wrong!
		funcprot(prot);                // Set the old protection back
	  printf('*** ERROR ***\n');     // Inform the user
	  error(lasterror());            //   and show the error message
		return;												 // Do not go any further
	end;
	//Everything went well
	funcprot(prot);                 //Get the old protection back
	//file 'whatis.htm' does not exist. It means that we either have no help at all
	//  (should not ever happen) or we have the XML files only - we will try to compile them
	printf(' no compiled help files found - compiling...\n')
	prot=funcprot(); //Get the default protection
	funcprot(0);     // Do not issue warnings about redefining function names
  currentDir=pwd();
  cd(LRP_OPTIONS.path+'/help/'+strDirName);
  cd(currentDir);
	try
	   	xmltohtml(fullfile(LRP_OPTIONS.path, "/help/"+strDirName),"LRP Toolkit: lrp/"+strDirName);
	catch
	  // Ooops - something went wrong!
		funcprot(prot);                // Set the old protection back
	  printf('*** ERROR COMPILING MASTER HELP FILE ***\n');     // Inform the user
	  error(lasterror());            //   and show the error message
		return;												 // Do not go any further
	end;
	printf('  COMPILATION SUCCESSFULL. Adding help files...')
end
	printf('OK\n');								  // and inform the user
endfunction

// Initialise the help files
disp("Initialising the help files")
// First, we add all the pathes - IMPORTANT - do it BEFORE any AddHelpChapter!
%helps=[%helps; [LRP_OPTIONS.path+'\help\misc','LRP misc']];
%helps=[%helps; [LRP_OPTIONS.path+'\help\classic','LRP classic']];
%helps=[%helps; [LRP_OPTIONS.path+'\help\mtx','CoolMatrix']];
AddHelpChapter("misc");
AddHelpChapter("classic");
AddHelpChapter("mtx");
