function [lrp]=boundCond_y1(lrp,T)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  if ~isdef('T') then
     T=1;
  end
  requireType("T",T,"real");
  
  lrp.ini.y0=ones(lrp.dim.m, lrp.dim.alpha);
endfunction
