function [lrp]=boundCond_ysin(lrp,T,k)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  if ~isdef('T') then
     T=1;
  end
  requireType("T",T,"real");
  if ~isdef('k') then
     k=1;
  end
  requireType("k",k,"real");
  
  s=k.*sin(T*((0:lrp.dim.alpha-1).*2*%pi)./((lrp.dim.alpha-1)));
  lrp.ini.y0=s(ones(1,lrp.dim.m),1:$);
endfunction
