// This script creates a Tk window used to select a controller. It is called by 
//   TCL/Tk's SelectControllerOption function.
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2009-12-02 10:27:00

function LRP_Gui_CreateSelectCont(lrp) // Name shortened as Scilab does not like them long

//First, prepare a list of all .sci files in the control\ subdirectory. This list
//  must NOT contain the special 'none.sci' file that needs special handling. The finished
//  list must be sorted.
 
fileList = findfiles(fullfile(LRP_OPTIONS.path,'\macros\control'),'*.sci')
// Remove all auxillary files, beginning with '%'
fileList = fileList(part(fileList,1)~='%');

if (size(fileList,1)==0) then
   error('CRITICAL ERROR: No *.sci files in \macros\control! Reinstall the LRP Toolkit!');
   return;
end

// Make sure 'none.sci' exists and remove it from the list. This one needs special processing and 
//   will be added later on 
noneSciPosition = strcmp(fileList,'none.sci');
if (or(noneSciPosition==0)==%F) then
   error('CRITICAL ERROR: File ''none.sci'' missing in \macros\control! Reinstall the LRP Toolkit!');
   return;
end
   
fileList = fileList(noneSciPosition~=0);
// Finally, sort it
fileList = gsort(fileList,'c','d');

// Set some Tcl variables
TCL_SetVar('LRP_ControllerName',lrp.controller(lrp.indController).functionName);
TCL_SetVar('LRP_CurrentControllerIndex',lrp.indController);
TCL_SetVar('optionValue','');

// and create the window
//   First, create the header
TCL_EvalFile(fullfile(LRP_OPTIONS.path,'\macros\gui\SelectController_Header.tcl'))

// none.sci is by assumption number 1.
if (lrp.indController == 1) then
  TCL_EvalStr('$main.fr.frame1.radio1 select');
end

//   then fill the rest by hand
for fileListId = 1:size(fileList,1)
    controllerId = fileListId+1; // first controllerId is occupied by 'none.sci'

    controllerName = fileList(fileListId);
    controllerName = part(controllerName,1:length(controllerName)-3-1); // remove the .sci extension    

    frameName = sprintf('$main.fr.frame%d',controllerId);
    
    TCL_EvalStr(sprintf('labelframe %s -relief groove -borderwidth 2 -text {%s}',frameName, controllerName));
    TCL_EvalStr(sprintf('radiobutton %s.radioCalculate -text {Calculate} -variable {optionValue} -value ''%s''',frameName, controllerName));
    TCL_EvalStr(sprintf('pack %s.radioCalculate -side top -padx 10 -pady 10 -fill both -expand no', frameName));
disp(controllerName);
    lrpIndex = findLRPIndex(lrp,'controller', controllerName)
    if (lrpIndex<=0) then
       TCL_EvalStr(sprintf('radiobutton %s.radioSet -text {Calc. first} -variable {optionValue} -value ''set%s'' -state disabled',frameName, controllerName));
    else 
       if (lrp.controller(lrpIndex).solutionExists) then 
          TCL_EvalStr(sprintf('radiobutton %s.radioSet -text {Select} -variable {optionValue} -value ''set%s''',frameName, controllerName));
       else
          TCL_EvalStr(sprintf('radiobutton %s.radioSet -text {NO SOL.} -variable {optionValue} -value ''set%s'' -state disabled',frameName, controllerName));
       end
    end

    TCL_EvalStr(sprintf('pack %s.radioSet -side left -padx 15 -pady 10 -fill both -expand no',frameName));
    TCL_EvalStr(sprintf('pack %s -side left -padx 10 -pady 10 -fill both -expand yes', frameName));
    // If we have a currently selected index, select it here as well
    if (lrp.indController == lrpIndex) then
      TCL_EvalFile(sprintf('%s.radioSet select', frameName));
    end
    
    disp(sprintf('radiobutton %s.radioCalculate -text ''Calculate'' -variable {optionValue} -value ''%d''',frameName, controllerId));    
end
TCL_EvalStr('pack $main.fr');
TCL_EvalStr('button $main.btt -text {OK} -command "" set returnValue \$optionValue; bind $main <Destroy> {}; destroy $main; "" -underline 0');
TCL_EvalStr('button $main.bttCancel -text {Cancel} -command ""set returnValue {EMPTY}; bind $main <Destroy> {}; destroy $main;"" -underline 0');
TCL_EvalStr('pack $main.bttCancel -padx 3 -pady 3 -side right');
TCL_EvalStr('pack $main.btt -padx 3 -pady 3 -side right');
TCL_EvalStr('focus -force ""$main.fr.frame1.radio1"";');
TCL_EvalStr('vwait returnValue');
TCL_EvalStr('puts {END_OF_WAIT}');
// Active wait - I do not know any other way of forcing Scilab to wait for TCL script to finish.
disp('begin waiting!');
disp(TCL_GetVar('returnValue'))
while(strcmp(TCL_GetVar('returnValue'),'')==0)
sleep(100);
TCL_GetVar('returnValue')
end
disp('end waiting!');
returnValue = TCL_GetVar('returnValue');
if strcmp(stripblanks(returnValue),'EMPTY') == 0
  return
end
// Remove '' if necessary
if part(returnValue,1)==''''
   returnValue = part(returnValue,2:length(returnValue)-1);
end 

disp(sprintf('lrp = %s(lrp);',returnValue));
// Save the result to file to avoid stack problems with Scilab. THIS IS A SCILAB BUG, not Toolkit's!
if MSDOS then 
   unix(sprintf('del ""%s""',TMPDIR+'\trash.sce'));
else 
   unix(sprintf('rm -f %s',TMPDIR+'/trash.sce')); 
end
fid=file('open',fullfile(TMPDIR,'/trash.sce'),'scratch','sequential','formatted');
write(fid,sprintf('lrp = %s(lrp);',returnValue));
file('close',fid);
//execstr(sprintf('lrp = %s(lrp);',returnValue));
endfunction
