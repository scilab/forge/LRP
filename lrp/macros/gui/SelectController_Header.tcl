global LRP_PATH
global LRP_ControllerIndexResult
global LRP_CurrentControllerIndex
global LRPModel_DISPLAY_NAME
catch {font delete infofont}
#and create a new one
font create infofont -family Helvetica -size 12 -weight bold
#The main '.rc' window should exist - if not, take its place. This could only happen during testing  
if {[winfo exists .rc]==0} then {
   set main .rc
} else {
   set main .rc.radiooption
}  
#Unbind the destruction handler, so the old form can be destroyed
catch "bind $main <Destroy> {set returnValue {EMPTY}}"
#If the main form exist - destroy it
catch {destroy $main}
# ... so we can build a new one

toplevel $main

set title {Select controller}

wm title $main {Select option}
#make the form not resizeable
wm resizable $main 0 0
 #The header
label $main.lbl -text $title -font infofont
pack $main.lbl
#The main part
frame $main.fr -relief groove -borderwidth 2
#setup the frame
frame $main.arraywindow -relief groove -borderwidth 2
global returnValue
global otherValue
set optionValue {XXX}

# setup the NONE frame
 labelframe $main.fr.frame1 -relief groove -borderwidth 2 -text "No controller"
    radiobutton $main.fr.frame1.radio1 -text "Select" -variable {optionValue} -value "setnone"
    pack $main.fr.frame1.radio1 -side top -padx 10 -pady 10 -fill both -expand yes
    pack $main.fr.frame1 -side left -padx 10 -pady 10 -fill both -expand yes
set widgetNumber {1}
set frameNumber {2}
 # frame #1 is reserved for NONE - no controller
global LRP_ControllerName;

# 
# foreach {x} $controlFileList {
#   labelframe $main.fr.frame$frameNumber -relief groove -borderwidth 2 -text "$x"
#   radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Calculate" -variable {optionValue} -value "$x"
#     pack $main.fr.frame$frameNumber.radio$widgetNumber -side top -padx 10 -pady 10 -fill both -expand no
#   incr widgetNumber
#   ExecInScilab "TCL_SetVar('LRP_ControllerIndexResult',findLRPIndex(lrp,'controller', '$x'));" "sync"
#     if $LRP_ControllerIndexResult<=-1 then {
#        radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Unk. val" -variable {optionValue} -value "set$x" -state disabled
#   } else {
#        puts "Setting controller $LRP_ControllerIndexResult";
# 			 set LRP_ControllerIndexResult [expr int($LRP_ControllerIndexResult)];
# 			 ExecInScilab "TCL_SetVar('LRP_ControllerIndexResult',bool2s(lrp.controller($LRP_ControllerIndexResult).solutionExists));" "sync"
# 			 set LRP_ControllerIndexResult [expr int($LRP_ControllerIndexResult)];
# 			 if $LRP_ControllerIndexResult==1 then {
#          radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Select" -variable {optionValue} -value "set$x"
#        } else {
# 				 # No solution has been found, do not let the user select this controller
# 				 radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "NO SOL." -variable {optionValue} -value "set$x" -state disabled
# 			 }
#     }
#     pack $main.fr.frame$frameNumber.radio$widgetNumber -side left -padx 15 -pady 10 -fill both -expand no
#   pack $main.fr.frame$frameNumber -side left -padx 10 -pady 10 -fill both -expand yes
#   if {$x eq $LRP_ControllerName} {
#      $main.fr.frame$frameNumber.radio2 select;
#   }
#     set widgetNumber {1}
#   incr frameNumber
# }
# if {$LRP_CurrentControllerIndex==1} {
#   $main.fr.frame1.radio1 select;
# }
# pack $main.fr
# #and the button
# set returnValue {}
# button $main.btt -text {OK} -command " set returnValue \$optionValue; bind $main <Destroy> {}; destroy $main; ExecInScilab \"exec(fullfile(LRP_OPTIONS.path,'macros','gui','LRP_Gui_SetController.sce'));\";" -underline 0
# button $main.bttCancel -text {Cancel} -command "set returnValue {}; bind $main <Destroy> {}; destroy $main;" -underline 0
# focus -force "$main.fr.frame1.radio1";
# pack $main.bttCancel  -padx 3 -pady 3 -side right
# pack $main.btt  -padx 3 -pady 3 -side right
# bind $main <Alt-o> "$main.btt invoke"
# bind $main <Alt-c> "$main.bttCancel invoke"
# # Bind the handler again. If the user closes the window without the handler set - scilab will wait forever
# bind $main <Destroy> "set returnValue {}"
# grab set $main
# #  tkwait variable returnValue;
# #unbind the handler, so we can destroy the window
# #  bind $main <Destroy> {}
# #  destroy $main
#  return $returnValue
