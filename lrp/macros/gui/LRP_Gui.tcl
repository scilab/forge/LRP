##!/usr/bin/wish -f
# Author Lukasz Hladowski
# e-mail: L.Hladowski@issi.uz.zgora.pl,
#
# Institute of Control & Computation Engineering,
# Faculty of Electrical Engineering, Computer Science and Telecommunications,
# University of Zielona Gora,
# POLAND
#
# Last revised: 2008-10-06 15:01:00
#Initialize
# ================================== BEGIN ===================================
puts {------------------ TOOLKIT INITIALISED ------------------}
# ------ Great for debugging ---------
#
#  catch {rename proc _proc
#  _proc proc {name arglist body} {
#      uplevel 1 [list _proc $name $arglist $body]
#      uplevel 1 [list trace add execution $name enterstep [list ::proc_start $name]]
#  }
#  _proc proc_start {name command op} {
#      puts "$name >> $command"
#  }
# }
#
#--------------------------------------

global RUN_IN_SCILAB
set RUN_IN_SCILAB 1; # Activate, Deactivate and ExecInScilab work when =1, otherwise - they do not!
global LRP_Help_SetterMaxX;
global LRP_Help_Status;

global LRP_Help_SetterMaxY;
set LRP_Help_SetterMaxX {}
set LRP_Help_SetterMaxY {}

global LRP_Help_SetterResult
set LRP_Help_SetterResult 0
global LRP_Help_ProvideParametersResult;
set LRP_Help_ProvideParametersResult {}
global LRP_Help_ProvideMatricesResult;
set LRP_Help_ProvideMatricesResult {}

global LRP_ProvideStateNumberToPlot_Result;
set LRP_ProvideStateNumberToPlot_Result {1};
global LRP_ProvideOutputNumberToPlot_Result;
set LRP_ProvideOutputNumberToPlot_Result {1};

global LRP_ProvideController_Result;
set LRP_ProvideController_Result {};
global LRP_ProvideStability_Result;
set LRP_ProvideStability_Result {};

global LRP_ControllerName
set LRP_ControllerName {none};

#SystemCreator is step number for creation of the system
global LRP_Help_SystemCreatorState
set LRP_Help_SystemCreatorState 1

# LRPModel_n - number of states
# LRPModel_m - number of outputs
# LRPModel_r - number of inputs
global LRP_PATH;
global TMP_DIR;
#LRP_PATH MUST **NOT** BE SET TO ANY 'default' VALUE. IT IS SET OUTSIDE OF TCL AND IS
#  GUARANTEED TO BE VALID IF THIS FILE IS RUN!!!
global LRPModel_n LRPModel_m LRPModel_r;
global LRPModel_A LRPModel_B LRPModel_B0;
global LRPModel_C LRPModel_D LRPModel_D0;
global LRPModel_Alpha LRPModel_Beta
global LRPModel_X0 LRPModel_Y0
global LRPModel_DISPLAY_NAME;
set LRPModel_DISPLAY_NAME {** UNSET **};
global LRP_ControllerIndexResult
set LRP_ControllerIndexResult {-2}
global LRP_CurrentControllerIndex
set LRP_CurrentControllerIndex {1}

global LRP_StabilityIndexResult
set LRP_StabilityIndexResult {-2}
global LRP_CurrentStabilityIndex
set LRP_CurrentStabilityIndex {1}

proc ExecInScilab {cmd {mode {}}} {
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_Alpha LRPModel_Beta
  global LRPModel_X0 LRPModel_Y0
  global LRP_PATH
  global TMP_DIR
  global LRP_Help_Status
  #  set ErrorCode {};
    #ScilabEval "TCL_SetVar('ErrorCode',execstr($cmd,'errcatch'))";
#    if "\$mode==\{\}" then " \
      #puts \"ScilabEval >>\$cmd<<\"; \
    #" else " \
      #puts \"ScilabEval >>\$cmd<<, mode=>>\$mode<<)\";  \
    #"
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}
    puts "$cmd";
    #ActivateMainWindow
    ScilabEval $cmd "sync" "seq"
    ScilabEval "flush"
    update;
  #  return $ErrorCode
}

if { [catch {image create photo LRP_Image -file "$LRP_PATH/gui/LRP.gif"}] } {
image create photo LRP_Image -file "$LRP_PATH/gui/LRP.gif"
}

proc MatrixDimensionsChanged {} {
  # is called if the matrix dimensions have changed
  global LRPModel_n LRPModel_m;
  global LRP_ProvideStateNumberToPlot_Result;
  if "$LRP_ProvideStateNumberToPlot_Result>$LRPModel_n" then {
    set LRP_ProvideStateNumberToPlot_Result {1};
  }
  global LRP_ProvideOutputNumberToPlot_Result;
  if "$LRP_ProvideOutputNumberToPlot_Result>$LRPModel_m" then {
    set LRP_ProvideOutputNumberToPlot_Result {1};
  }
}
#END Starting screen - to be deleted in the final version
# ====================================================== RandomMatrix ================================
proc RandomMatrix {Matrix} {
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_Alpha LRPModel_Beta;
  if {$Matrix == "A"} {
    ExecInScilab "TCL_SetVar('LRPModel_A',string(sci2exp(rand($LRPModel_n, $LRPModel_n),0)));"
  }
  if {$Matrix == "B"} {
    ExecInScilab "TCL_SetVar('LRPModel_B',string(sci2exp(rand($LRPModel_n, $LRPModel_r),0)));"
  }
  if {$Matrix == "B0"} {
    ExecInScilab "TCL_SetVar('LRPModel_B0',string(sci2exp(rand($LRPModel_n, $LRPModel_m),0)));"
  }
  if {$Matrix == "C"} {
    ExecInScilab "TCL_SetVar('LRPModel_C',string(sci2exp(rand($LRPModel_m, $LRPModel_n),0)));"
  }
  if {$Matrix == "D"} {
    ExecInScilab "TCL_SetVar('LRPModel_D',string(sci2exp(rand($LRPModel_m, $LRPModel_r),0)));"
  }
  if {$Matrix == "D0"} {
    ExecInScilab "TCL_SetVar('LRPModel_D0',string(sci2exp(rand($LRPModel_m, $LRPModel_m),0)));"
  }
  if {$Matrix == "X0"} {
    ExecInScilab "TCL_SetVar('LRPModel_X0',string(sci2exp(rand($LRPModel_n, [expr \"$LRPModel_Beta\"] ),0)));"
  }
  if {$Matrix == "Y0"} {
    ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(rand($LRPModel_m, $LRPModel_Alpha),0)));"
  }
}
# ====================================================== SetToZero ================================
proc SetToZero {matrix m n} {
  #sets the "mat$x$y" variables for use with the Creator
  global $matrix;
  set res {[}
  for {set y 0} {$y<$m - 1} {incr y} {
    for {set x 0} {$x<$n} {incr x} {
      append res 0
      append res { }; #append space at the end
    }
    append res "; "
  }
  for {set x 0} {$x<$n} {incr x} {
    append res 0
    append res { }
  }
  append res {]}
  set $matrix $res
}
proc MatrixToLatex {matrix n m} {
  #sets the "mat$x$y" variables for use with the Creator
  global $matrix;
  #puts "string range $matrix 1 [expr [string length $matrix]-2]"
  set temporary "[string range $matrix 1 [expr [string length $matrix]-2]]"
  set temporary "$temporary"
  regsub -all {,} $temporary { \\\\\& } temporary
  regsub -all {;} $temporary {\\\\\\\\\\\\\\\\ \n } temporary
  set temporary "\\\\left\[\n\\\\begin\{BMAT\}\{[string repeat {c} $m]\}\{[string repeat {c} $n]\}\n$temporary\n\\\\end\{BMAT\}\n\\\\right\]"
  #puts "[subst -nocommands $temporary]"
  #TCL_messageBox -message "$temporary"
  return $temporary
}
# ====================================================== DeactivateAll ================================
proc ActivateAll {parent} {
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}
  # no $parent, so do nothing!
  if {[catch {winfo class $parent}] == 1} {return; }  
  foreach window [winfo children $parent] {
    if { [llength $window] > 0 } then {
      ActivateAll $window;
      if {[string compare [winfo class $window] "Labelframe"] == 0} then {
         continue;
      }
      if {[string compare [winfo class $window] "Frame"] == 0} then {
         continue;
      }
      if {[string compare [winfo class $window] "Toplevel"] == 0} then {
         continue;
      }                  
      if {[string compare [winfo class $window] {Menu}] == 0} then {
        for {set menuEntry 1} {$menuEntry <= [llength [winfo children $window]]} {incr menuEntry} {
            $window entryconfigure $menuEntry -state active
        }
        continue;
      }
      if {[string compare [winfo class $window] "Spinbox"] == 0} then {
         $window configure -state normal;
         continue;
      }
      if {[string compare [winfo class $window] "Entry"] == 0} then {
         $window configure -state normal;
         continue;
      }
      if {[string compare [winfo class $window] "Button"] == 0} then {
         $window configure -state active;
         continue;
      }
      if {[catch {$window configure -state active;} ]==1} {
         puts {***** LRP_Gui: ActivateAll: Unable to activate the following control *****};
        puts $window;
        puts [winfo class $window];
         puts {**********};
      }
      $window configure -state active;
    }
  }
}
# ====================================================== DeactivateAll ================================
proc DeactivateAll {parent} {
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}
  # no $parent, so do nothing!
  if {[catch {winfo class $parent}] == 1} {return; }  
  foreach window [winfo children $parent] {
    if { [llength $window] > 0 } then {
      DeactivateAll $window;
      if {[string compare [winfo class $window] "Labelframe"] == 0} then {
         continue;
      }
      if {[string compare [winfo class $window] "Toplevel"] == 0} then {
         continue;
      }                  
      if {[string compare [winfo class $window] "Frame"] == 0} then {
         continue;
      }                  
      if {[string compare [winfo class $window] {Menu}] == 0} then {
        for {set menuEntry 1} {$menuEntry <= [llength [winfo children $window]]} {incr menuEntry} {
            $window entryconfigure $menuEntry -state disabled
        }
        continue;
      }
      if {[string compare [winfo class $window] "Spinbox"] == 0} then {
         $window configure -state disabled;
         continue;
      }
      if {[string compare [winfo class $window] "Entry"] == 0} then {
         $window configure -state disabled;
         continue;
      }
      if {[string compare [winfo class $window] "Button"] == 0} then {
         $window configure -state disabled;
         continue;
      }
      if {[catch {$window configure -state disabled;} ]==1} {
         puts {***** LRP_Gui: DeactivateAll: Unable to deactivate the following control *****};
         puts $window;
         puts [winfo class $window];
         puts {**********};
      }
      $window configure -state disabled;
    }
  }
}
# ====================================================== DeactivateMainWindow ================================
#deactivates all buttons of main window
proc DeactivateMainWindow {} {
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}
  foreach btt [pack slaves .rc] {
    foreach bttNext [pack slaves $btt] {
   foreach bttNext2 [pack slaves $bttNext] {
       foreach bttNext3 [pack slaves $bttNext2] {
      foreach bttNext4 [pack slaves $bttNext3] {
        catch {$bttNext4 configure -state disabled}
      }
      catch {$bttNext3 configure -state disabled}
       }
       catch {$bttNext2 configure -state disabled}
       }
    catch {$bttNext configure -state disabled}
      }
  }
  catch {.rc.mainmenu entryconfigure 1 -state disabled}
  catch {.rc.mainmenu entryconfigure 2 -state disabled}
  catch {.rc.mainmenu entryconfigure 3 -state disabled}
}
# ====================================================== ActivateMainWindow ================================
#Activates all buttons of main window
proc ActivateMainWindow {} {
  global LRP_Help_Status
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}
  foreach btt [pack slaves .rc] {
    foreach bttNext [pack slaves $btt] {
   foreach bttNext2 [pack slaves $bttNext] {
       foreach bttNext3 [pack slaves $bttNext2] {
      foreach bttNext4 [pack slaves $bttNext3] {
        catch {$bttNext4 configure -state active}
      }
      catch {$bttNext3 configure -state active}
       }
       catch {$bttNext2 configure -state active}
       }
    catch {$bttNext configure -state active}
      }
  }
  catch {.rc.mainmenu entryconfigure 1 -state active}
  catch {.rc.mainmenu entryconfigure 2 -state active}
  catch {.rc.mainmenu entryconfigure 3 -state active}
  set LRP_Help_Status {Ready}
}
#============================ PickRadioOption ============================
proc PickRadioOption {numberOfOptions title oldValue} {
#Allows user to select only one option from the possible "numberOfOptions" values.
#  The first value has number one etc. Initially the "oldValue" option is selected
#  By assumption 1<=oldValue<=numberOfOptions. oldValue={} is the same as oldValue=1
#  RETURNS: Selected value. If the user selects "Cancel" then the oldValue is returned.
#delete old font
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.radiooption
  #Unbind the destruction handler, so the old form can be destroyed
  catch "bind $main <Destroy> {}"
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main

  wm title $main {Select option}
  #make the form not resizeable
  wm resizable $main 0 0
   #The header
  label $main.lbl -text $title -font infofont
  pack $main.lbl
  #The main part
  frame $main.fr -relief groove -borderwidth 2
  #setup the frame
  frame $main.arraywindow -relief groove -borderwidth 2
  global returnValue
  global otherValue
  for {set x 1} {$x<=[expr $numberOfOptions] } {incr x} {
    # radiobutton $main.fr.radio$x -text "[expr $x]" -variable "optionValue" -value "[expr $x]"
    radiobutton $main.fr.radio$x -text "[expr $x]" -variable {optionValue} -value "setnone"
    pack $main.fr.radio$x -side left -padx 10 -pady 10 -fill both -expand yes
  }
  set optionValue $oldValue
  $main.fr.radio$optionValue select
  pack $main.fr

  #and the button
  set returnValue {0}
  button $main.btt -text {OK} -command "set returnValue \[expr \$optionValue\];" -underline 0
  button $main.bttCancel -text {Cancel} -command "set returnValue {EMPTY};" -underline 0
  focus -force "$main.fr.radio$oldValue";
  pack $main.bttCancel  -padx 3 -pady 3 -side right
  pack $main.btt  -padx 3 -pady 3 -side right
  bind $main <Alt-o> "$main.btt invoke"
  bind $main <Alt-c> "$main.bttCancel invoke"
  # Bind the handler again. If the user closes the window without the handler set - scilab will wait forever
  bind $main <Destroy> "set returnValue $oldValue;"
  grab set $main
  tkwait variable returnValue;
  #unbind the handler, so we can destroy the window
  bind $main <Destroy> {}
  destroy $main
  return $returnValue
}
#============================ PickRadioOption ============================
proc SelectControllerOption {title} {
#Allows user to select the controller file in $LRP_PATH\control dir.
#  RETURNS: Selected file, WITHOUT extension (.sci is assumed).
#  If the user selects "Cancel" then the oldValue is returned.
#delete old font

 ExecInScilab {exec(fullfile(LRP_OPTIONS.path,'macros','gui','LRP_Gui_CreateSelectCont.sci'));};
 ExecInScilab {LRP_Gui_CreateSelectCont(lrp);};
 ExecInScilab {exec(fullfile(TMPDIR,'/trash.sce'));}
return;


   global LRP_PATH
   global LRP_ControllerIndexResult
   global LRP_CurrentControllerIndex
   global LRPModel_DISPLAY_NAME
   set controlFileList "[regsub -all {\.sci([^.]|$)} [glob -type {f} -tails -nocomplain -directory [file normalize $LRP_PATH/control] -- *.sci] {\1}]"
	 set controlFileList "[regsub -all {[ ]{0,}[%][^ ]{0,}([ ]|$)} $controlFileList {\1}]"
	 set controlFileList "[regsub -all {[ ]{2,}} $controlFileList { }]"
#puts "controlFileList=$controlFileList"
	 if [llength $controlFileList]==0 then {
      tk_messageBox -message "No files in [file normalize $LRP_PATH/control] with extension .sci!" -type ok -title {LRP Toolkit ERROR} -icon error;
     return
   }
   set controlFileList [lsort -ascii $controlFileList]
   set noneElementIndex [lsearch -ascii -sorted $controlFileList {none}]
   if $noneElementIndex==-1 then {
      tk_messageBox -message "No file named 'none.sci' in [file normalize $LRP_PATH/control]. Please reinstall the toolkit!" -type ok -title {LRP Toolkit ERROR} -icon error;
     return
   }
    # delete the "none" element
   set controlFileList [lreplace $controlFileList $noneElementIndex $noneElementIndex]
	set SciList {}
  foreach {x} $controlFileList {
      lappend SciList \"$x\"
  }
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.radiooption
  #Unbind the destruction handler, so the old form can be destroyed
  catch "bind $main <Destroy> {}"
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main

  wm title $main {Select option}
  #make the form not resizeable
  wm resizable $main 0 0
   #The header
  label $main.lbl -text $title -font infofont
  pack $main.lbl
  #The main part
  frame $main.fr -relief groove -borderwidth 2
  #setup the frame
  frame $main.arraywindow -relief groove -borderwidth 2
  global returnValue
  global otherValue
  set optionValue {XXX}
  ExecInScilab "SciList=\[[join $SciList ", "]\]";
 ExecInScilab {exec(fullfile(LRP_OPTIONS.path,'macros','gui','LRP_Gui_CreateSelectCont.sci'));};
 ExecInScilab {lrp=LRP_Gui_CreateSelectCont(lrp);};
#  ExecInScilab {xxx.sce;};
return;
  ExecInScilab "TCL_SetVar('LRP_CurrentControllerIndex',lrp.indController);" "sync"
  # setup the NONE frame
   labelframe $main.fr.frame1 -relief groove -borderwidth 2 -text "No controller"
      radiobutton $main.fr.frame1.radio1 -text "Select" -variable {optionValue} -value "setnone"
      pack $main.fr.frame1.radio1 -side top -padx 10 -pady 10 -fill both -expand yes
      pack $main.fr.frame1 -side left -padx 10 -pady 10 -fill both -expand yes
  set widgetNumber {1}
  set frameNumber {2}
   # frame #1 is reserved for NONE - no controller
  set LRP_CurrentControllerIndex [expr int($LRP_CurrentControllerIndex)];
  global LRP_ControllerName;
  ExecInScilab "TCL_SetVar('LRP_ControllerName',lrp.controller(lrp.indController).functionName);" "sync"
	foreach {x} $controlFileList {
    labelframe $main.fr.frame$frameNumber -relief groove -borderwidth 2 -text "$x"
    radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Calculate" -variable {optionValue} -value "$x"
      pack $main.fr.frame$frameNumber.radio$widgetNumber -side top -padx 10 -pady 10 -fill both -expand no
    incr widgetNumber
    ExecInScilab "TCL_SetVar('LRP_ControllerIndexResult',findLRPIndex(lrp,'controller', '$x'));" "sync"
      if $LRP_ControllerIndexResult<=-1 then {
         radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Unk. val" -variable {optionValue} -value "set$x" -state disabled
    } else {
         # puts "Setting controller $LRP_ControllerIndexResult";
				 set LRP_ControllerIndexResult [expr int($LRP_ControllerIndexResult)];
				 ExecInScilab "TCL_SetVar('LRP_ControllerIndexResult',bool2s(lrp.controller($LRP_ControllerIndexResult).solutionExists));" "sync"
				 set LRP_ControllerIndexResult [expr int($LRP_ControllerIndexResult)];
				 if $LRP_ControllerIndexResult==1 then {
	         radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Select" -variable {optionValue} -value "set$x"
         } else {
					 # No solution has been found, do not let the user select this controller
					 radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "NO SOL." -variable {optionValue} -value "set$x" -state disabled
				 }
      }
      pack $main.fr.frame$frameNumber.radio$widgetNumber -side left -padx 15 -pady 10 -fill both -expand no
    pack $main.fr.frame$frameNumber -side left -padx 10 -pady 10 -fill both -expand yes
    if {$x eq $LRP_ControllerName} {
       $main.fr.frame$frameNumber.radio2 select;
    }
      set widgetNumber {1}
    incr frameNumber
  }
  if {$LRP_CurrentControllerIndex==1} {
    $main.fr.frame1.radio1 select;
  }
  pack $main.fr
  #and the button
  set returnValue {}
  button $main.btt -text {OK} -command " set returnValue \$optionValue; bind $main <Destroy> {}; destroy $main; ExecInScilab \"exec(fullfile(LRP_OPTIONS.path,'macros','gui','LRP_Gui_SetController.sce'));\";" -underline 0
  button $main.bttCancel -text {Cancel} -command "set returnValue {}; bind $main <Destroy> {}; destroy $main;" -underline 0
  focus -force "$main.fr.frame1.radio1";
  pack $main.bttCancel  -padx 3 -pady 3 -side right
  pack $main.btt  -padx 3 -pady 3 -side right
  bind $main <Alt-o> "$main.btt invoke"
  bind $main <Alt-c> "$main.bttCancel invoke"
  # Bind the handler again. If the user closes the window without the handler set - scilab will wait forever
  bind $main <Destroy> "set returnValue {}"
  grab set $main
#  tkwait variable returnValue;
  #unbind the handler, so we can destroy the window
#  bind $main <Destroy> {}
#  destroy $main
#  return $returnValue
}
proc SelectStabilityOption {title} {
#Allows user to select the stability file in $LRP_PATH\stability dir.
#  RETURNS: Selected file, WITHOUT extension (.sci is assumed).
#  If the user selects "Cancel" then the oldValue is returned.
#delete old font
   global LRP_PATH
   global LRP_StabilityIndexResult
   global LRP_CurrentStabilityIndex
   set controlFileList "[regsub -all {\.sci([^.]|$)} [glob -type {f} -tails -nocomplain -directory [file normalize $LRP_PATH/stability] -- st*.sci] {\1}]"
	 puts $controlFileList;
   if [llength $controlFileList]==0 then {
      tk_messageBox -message "No files in [file normalize $LRP_PATH/stability] matching pattern st*.sci!" -type ok -title {LRP Toolkit ERROR} -icon error;
     return
   }
   set controlFileList [lsort -ascii $controlFileList]
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.radiooption
  #Unbind the destruction handler, so the old form can be destroyed
  catch "bind $main <Destroy> {}"
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main

  wm title $main {Select option}
  #make the form not resizeable
  wm resizable $main 0 0
   #The header
  label $main.lbl -text $title -font infofont
  pack $main.lbl
  #The main part
  frame $main.fr -relief groove -borderwidth 2
  #setup the frame
  frame $main.arraywindow -relief groove -borderwidth 2
  global returnValue
  global otherValue
  # setup the NONE frame
  set widgetNumber {1}
  set frameNumber {1}
  set optionValue {XXX}

  global LRP_StabilityName;
  #ExecInScilab "TCL_SetVar('LRP_StabilityName',lrp.stability(lrp.indstability)(1)(1));" "sync"
  set RESULT {QQQ};
  foreach {x} $controlFileList {
    labelframe $main.fr.frame$frameNumber -relief groove -borderwidth 2 -text "$x"
    radiobutton $main.fr.frame$frameNumber.radio$widgetNumber -text "Calculate" -variable {optionValue} -value "$x" -command {puts $optionValue};
      pack $main.fr.frame$frameNumber.radio$widgetNumber -side top -padx 10 -pady 10 -fill both -expand no
    incr widgetNumber
#     if {$x eq $LRP_StabilityName} {
#        $main.fr.frame$frameNumber.radio1 select;
#     }
    pack $main.fr.frame$frameNumber -side left -padx 10 -pady 10 -fill both -expand yes
      set widgetNumber {1}
    incr frameNumber
    puts $optionValue
  }
  if {$LRP_CurrentStabilityIndex==1} {
    $main.fr.frame1.radio1 select;
  }
  pack $main.fr
  #and the button
  set returnValue {}
  button $main.btt -text {Calculate} -command "ExecInScilab \"lrp=\$optionValue (lrp);\"; ExecInScilab \"TCL_EvalStr('tk_messageBox -message '+strIsStable(lrp,'stability','\$optionValue')+' -title \$optionValue -parent $main');\"; return $returnValue; " -underline 0
  button $main.bttCancel -text {Close} -command "set returnValue {};   bind $main <Destroy> \"set returnValue {}\"; grab set $main; destroy $main;" -underline 1
  focus -force "$main.fr.frame1.radio1";
  pack $main.bttCancel  -padx 3 -pady 3 -side right
  pack $main.btt  -padx 3 -pady 3 -side right
  bind $main <Alt-c> "$main.btt invoke"
  bind $main <Alt-l> "$main.bttCancel invoke"
  # Bind the handler again. If the user closes the window without the handler set - scilab will wait forever
  bind $main <Destroy> "set returnValue {}"
  grab set $main
###  tkwait variable returnValue;
  #unbind the handler, so we can destroy the window
###  bind $main <Destroy> {}
###  catch "destroy $main"
###  return $returnValue
}

# ====================================================== LRPToolkit ================================
proc LRPToolkit {} {
  # Main procedure
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_Alpha LRPModel_Beta;

#   set LRPModel_A {}
#   set LRPModel_B {}
#   set LRPModel_B0 {}
#   set LRPModel_C {}
#   set LRPModel_D {}
#   set LRPModel_D0 {}
#   set LRPModel_Alpha 10
#   set LRPModel_Beta 20
  RunMatrixCreator;
}
#===================================================================================================
#
#                                             RunMatrixCreator
#                              New Process Creator - setup the model dimensions
#
# ==================================================================================================
proc RunMatrixCreator {} {
  global LRP_Help_ProvideParametersResult;
  global LRP_Help_ProvideMatricesResult;
  global LRP_Help_SystemCreatorState;
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  catch { destroy .rc.plotoption};
  set $LRP_Help_ProvideParametersResult {}
  set $LRP_Help_ProvideMatricesResult {}
  if {$LRP_Help_SystemCreatorState == 1} {
#     set LRPModel_A {}
#     set LRPModel_B {}
#     set LRPModel_B0 {}
#     set LRPModel_C {}
#     set LRPModel_D {}
#     set LRPModel_D0 {}
#     set LRPModel_n {1}
#     set LRPModel_m {1}
#     set LRPModel_r {1}
    global LRPModel_Alpha LRPModel_Beta
#     set LRPModel_Alpha 10
#     set LRPModel_Beta 20
    ProvideParameters;
#     tkwait variable LRP_Help_ProvideParametersResult;
#     if {$LRP_Help_ProvideParametersResult == 10} { set LRP_Help_SystemCreatorState 1; return;}
#     if {$LRP_Help_ProvideParametersResult == 1} {
#       set LRP_Help_SystemCreatorState 2
# #      ProvideMatrices
# #      tkwait window .rc.providematrices
# #      #tkwait variable LRP_Help_ProvideMatricesResult;
# #      if {$LRP_Help_ProvideMatricesResult == 10} { set LRP_Help_SystemCreatorState 1; return;}
# #      if {$LRP_Help_ProvideMatricesResult == -1} {
# #        set LRP_Help_SystemCreatorState 1
# #        RunMatrixCreator;
# #        return;
# #      }
#     }
#   }
#     if {$LRP_Help_SystemCreatorState == 2 } {
#       set LRP_Help_SystemCreatorState 3
#       ProvideMatrices
#       tkwait variable LRP_Help_ProvideMatricesResult;
#       if {$LRP_Help_ProvideMatricesResult == 10} { set LRP_Help_SystemCreatorState 1; return;}
#       if {$LRP_Help_ProvideMatricesResult == -1} {
#         set LRP_Help_SystemCreatorState 1
#         RunMatrixCreator;
#         return;
#       }
#    } elseif {$LRP_Help_SystemCreatorState == 3 } {
#       ProvideSimulationParameter
#       tkwait variable LRP_Help_ProvideSimulationParametersResult;
#       if {$LRP_Help_ProvideMatricesResult == 10} { set LRP_Help_SystemCreatorState 1; return;}
#       if {$LRP_Help_ProvideMatricesResult == -1} {
#         set LRP_Help_SystemCreatorState 2
#         RunMatrixCreator;
#         return;
#       }
#   }
}
# ====================================================== ParseMatrix ================================
proc ParseMatrix {data} {
  if {$data == ""} {
      catch {unset lst}
      list lst {-1 -1}
      return lst
  }
  set lst [split $data {[;]}]
  list tmp {}
  foreach {x} $lst {
    set x [string trim $x]
    set x [string map {, { }} $x]
    if {$x != ""} {
      lappend tmp $x
    }
  }
  set rowDim 0
  set colDim 0
  set colDimTmp 0
#  set res {[}
  foreach {x} $tmp {
    foreach {y} $x {
#      append res "$y "
      incr colDimTmp
    }
    if {$colDim == 0} {
      set colDim $colDimTmp
    } elseif {$colDim != $colDimTmp} {
      error "Wrong num. of columns! Was: $colDim, is: $colDimTmp";
      unset lst
      list lst {-1 -1}
      return $lst
    }
    set colDimTmp 0
    incr rowDim
#    append res {; }
  }
#  set res [string trimright $res {; }]
#  append res {]}
#  puts $res

  set a 0
  set b 0
  foreach {x} $tmp {
    foreach {y} $x {
      global mat$a$b
      set mat$a$b $y
#puts "Parse: mat$a$b = $y"
      incr a
    }
    incr b
    set a 0
  }
  unset lst
  list lst
  lappend lst $rowDim
  lappend lst $colDim
  return $lst
}
# ====================================================== CREATE ================================
proc Create {entryText matrix maxY maxX} {
  #creates a new Matrix Entry creator
  global LRP_Help_CreateResult;
  global LRP_Help_SetterResult;
  global LRP_Help_SetterMaxX;
  global LRP_Help_SetterMaxY;
  set LRP_Help_SetterMaxX $maxX
  set LRP_Help_SetterMaxY $maxY

  catch {bind .rc.array <Destroy> {} }
  catch { destroy .rc.array }
  if {$maxX == ""} {
    return;
  }
  if {$maxY == ""} {
    return;
  }
  list lst
  if {$matrix != "" } {
    set lst [ParseMatrix $matrix];
    if {[lindex $lst 0] == -1 } {
      return;
    }
    if {[lindex $lst 0] > 0} {
#        set maxY [lindex $lst 0]
    }
    if {[lindex $lst 1] > 0} {
   set maxX [lindex $lst 1]
    }
    set needsNewSetting 0
  } else {
    set needsNewSetting 1
  }
  catch {font delete smallfont}
  font create smallfont -family Courier -size 8
  toplevel .rc.array
  #do not allow for closing
  wm protocol .rc.array WM_DELETE_WINDOW {#}
  set x {0};
  set y {0};
  button .rc.array.btt -text {OK} -command { \
         if 1==[regexp {[.]rc[.]array[.]arraywindow[.]canvas[.]ent} [focus]] { \
            if [[focus] validate]==0 { \
               return; \
            }; \
         }; \
         Setter ; \
         set LRP_Help_CreateResult $LRP_Help_SetterResult; \
         destroy .rc.array; \
         } -underline 0;
  bind .rc.array <Destroy> {Setter ; set LRP_Help_CreateResult $LRP_Help_SetterResult; destroy .rc.array}
  wm title .rc.array $entryText
  label .rc.array.lbl -text "Enter values for $entryText"
  pack .rc.array.lbl
  frame .rc.array.arraywindow -relief groove -borderwidth 2
  set maxx $maxX
  set maxy $maxY

  set maxx [expr $maxx * 75 + 10]
  if "[expr $maxX * 75 + 10] > 400" {
    set showHScroll 1
    set maxwidth 400
  } else {
    set showHScroll 0
    set maxwidth [expr $maxX * 75 + 10]
  }
  set maxy [expr $maxy * 30 + 10]
  if "[expr $maxY * 30 + 10] > 200" {
    set showVScroll 1
    set maxheight 200
  } else {
    set showVScroll 0
    set maxheight [expr $maxY * 30 + 10]
  }

  frame .rc.array.arraywindow.hscrollframe -width $maxwidth
  frame .rc.array.arraywindow.vscrollframe -height $maxheight
  scrollbar .rc.array.arraywindow.hscrollframe.hscroll -orient horiz -command ".rc.array.arraywindow.canvas xview"
  scrollbar .rc.array.arraywindow.vscrollframe.vscroll -command ".rc.array.arraywindow.canvas yview"
  #width, height is a size of a visible part of the canvas
  #scrollregion is a size of the whole canvas
  canvas .rc.array.arraywindow.canvas -relief flat -borderwidth 2 \
     -xscrollcommand ".rc.array.arraywindow.hscrollframe.hscroll set" \
     -yscrollcommand ".rc.array.arraywindow.vscrollframe.vscroll set" \
    -scrollregion "-5 -5 [expr $maxx - 5] [expr $maxy - 5]" \
    -highlightthickness 0 \
      -width $maxwidth \
      -height $maxheight

#    -width "[.rc.array.arraywindow cget -width ]"

  .rc.array.arraywindow.canvas xview moveto 0
  .rc.array.arraywindow.canvas yview moveto 0
  for {set y 0} {$y<$maxY} {incr y} {
     for {set x 0} {$x<$maxX} {incr x} {
      global "mat$x$y"
      if {$needsNewSetting == 1} {set mat$x$y 0}
#puts "Create: mat$x$y = [set mat$x$y]"
      entry .rc.array.arraywindow.canvas.ent{$x}{$y} -width 10 -textvariable "mat$x$y" -validatecommand { \
      if [string compare %V key]==0 { \
        return 1; \
      }; \
       if {1 == [regexp {(^[+-]{0,1}[0-9]{1,}[.]{0,1}[0-9]*$)} %P]} { \
          return 1; \
       } else { \
         if {1 == [regexp {(^[+-]{0,1}[0-9]{1,}[,]{1}[0-9]*$)} %P]} { \
            regsub {[,]} %P {.} [%W cget -textvariable]; \
            after idle {%W config -validate %v}; \
            return 1;\
         } else { \
         if [string length {%P}]==0 { \
            tk_messageBox -message "Wrong matrix entry - must be a number.\n  Zero inserted." -type ok -title {Wrong matrix entry - must be a number.} -icon warning -parent .rc.array; \
            set [%W cget -textvariable] {0}; \
         } else { \
            if [regexp {[ ]{0,}[^-+a-zA-Z0-9eE*.,()/][ ]{0,}} {%P}]==0 { \
               if { [catch {expr [regsub -all {,} %P {.}]}] } { \
                  tk_messageBox -message "Error evaluating expression.\n  Zero inserted." -type ok -title {Wrong matrix entry - must be a number.} -icon warning -parent .rc.array; \
                  set [%W cget -textvariable] {0}; \
               } else { \
                  catch {set [%W cget -textvariable] [expr [regsub -all {,} %P {.}]];}; \
               }; \
            } else { \
              tk_messageBox -message "Wrong matrix entry - must be a number.\n  Zero inserted." -type ok -title {Wrong matrix entry - must be a number.} -icon warning -parent .rc.array; \
              set [%W cget -textvariable] {0}; \
            }; \
         }; \
         after idle {%W config -validate %v}; \
         return 0; \
         }; \
       }; \
      } -validate all


      set prevX [expr $x - 1]
      if {$prevX < 0} {set prevX [expr $maxX - 1]}
      set prevY [expr $y - 1]
      if {$prevY < 0} {set prevY [expr $maxY - 1]}
      set nextX [expr $x + 1]
      if {$nextX >= $maxX} {set nextX 0}
      set nextY [expr $y + 1]
      if {$nextY >= $maxY} {set nextY 0}

      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Control-Shift-Up> "focus .rc.array.arraywindow.canvas.ent{$x}{$prevY}"
      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Control-Shift-Down> "focus .rc.array.arraywindow.canvas.ent{$x}{$nextY}"
      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Control-Shift-Left> "focus .rc.array.arraywindow.canvas.ent{$prevX}{$y}"
      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Control-Shift-Right> "focus .rc.array.arraywindow.canvas.ent{$nextX}{$y}"
      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Alt-Right> "focus .rc.array.arraywindow.canvas.ent{$nextX}{$nextY}"
      bind .rc.array.arraywindow.canvas.ent{$x}{$y} <Alt-Left> "focus .rc.array.arraywindow.canvas.ent{$prevX}{$prevY}"
      # create the window
      .rc.array.arraywindow.canvas create window [expr $x * 75] [expr $y * 30] \
   -window .rc.array.arraywindow.canvas.ent{$x}{$y} \
   -anchor nw -tag ent{$x}{$y}
      set xt [expr $x + 1]
      set yt [expr $y + 1]
      .rc.array.arraywindow.canvas create text [expr $x * 75] [expr $y * 30 + 15] -font smallfont -text "$yt, $xt" -anchor nw
     }
  }
  if "$showHScroll == 1" {
    pack .rc.array.arraywindow.hscrollframe -side bottom -fill x -pady 2 -padx 2 -expand 1
    pack .rc.array.arraywindow.hscrollframe.hscroll -fill x -expand 1
 }
  pack .rc.array.arraywindow.canvas -side left -expand 1 -fill both
  pack .rc.array.arraywindow -fill both -expand 1
  if "$showVScroll == 1" {
    pack .rc.array.arraywindow.vscrollframe -fill y -padx 4 -pady 2 -expand 1
    pack .rc.array.arraywindow.vscrollframe.vscroll -fill y -expand 1
  }
  pack .rc.array.btt -pady 2
  focus .rc.array.arraywindow.canvas.ent{0}{0}
  .rc.array.arraywindow.canvas.ent{0}{0} selection range 0 end
  bind .rc.array <Alt-o> ".rc.array.btt invoke"
  grab set .rc.array
}
# ====================================================== SETTER ================================
proc Setter {} {
  #sets the "mat$x$y" variables for use with the Creator
  global LRP_Help_SetterResult;
  global LRP_Help_SetterMaxX;
  global LRP_Help_SetterMaxY;
  set res {[}
  for {set y 0} {$y<$LRP_Help_SetterMaxY - 1} {incr y} {
    for {set x 0} {$x<$LRP_Help_SetterMaxX} {incr x} {
      global "mat$x$y"
      append res [set "mat$x$y"]
      append res { }; #append space at the end
    }
    append res "; "
  }
  for {set x 0} {$x<$LRP_Help_SetterMaxX} {incr x} {
    global "mat$x$y"
    append res [set "mat$x$y"]
    append res { }
  }
  append res {]}
  set LRP_Help_SetterResult $res
  return $LRP_Help_SetterResult
}
# ====================================================== ProvideParameters ================================
proc ProvideParameters {} {
  # provides the parameters of LRPModel_n LRPModel_m LRPModel_r.
  #RETURN VALUES:
  # 1 - user clicked "Next"
  # -1 - user clicked "Previous"
  # 10 - user closed window. WARNING: NO RETURN VALUE. CHECK THE LRP_Help_ProvideParametersResult FOR RESULT!!!
  # create the fonts
  global LRP_ProvideParameters_NeedZeroing; #1==all the matrices will be zeroed in ProvideMatrices
  global LRP_Help_ProvideParametersResult;
  global LRPModel_n LRPModel_m LRPModel_r
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #unbind the handler
  catch {bind .rc.provideparameters <Destroy> {} }

  #make sure, there are no duplicates of this window
  catch {destroy .rc.provideparameters}
  # setup main window
  toplevel .rc.provideparameters
  #do not allow for closing
  wm protocol .rc.provideparameters WM_DELETE_WINDOW {#}

  #and bind the handler again
  bind .rc.provideparameters <Destroy> { \
    catch {bind .rc.provideparameters <Destroy> {} }; \
    catch {set LRP_Help_ProvideParametersResult 10 }; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title .rc.provideparameters {Provide system parameters}
  wm resizable .rc.provideparameters 0 0
  #prepare the main frame
  set main .rc.provideparameters.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Provide the system parameters:}
  pack $main.info
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2

  set frcontrol $main.frcontrol
  frame $frcontrol
  # Next and Prev buttons
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideparameters; set LRP_Help_ProvideParametersResult -1;} -underline 0
  button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideparameters; MatrixDimensionsChanged;} -underline 0

  #button $frcontrol.btnext -text {Next} -command { destroy .rc.provideparameters; set LRP_Help_ProvideParametersResult 1; } -underline 0
  button $frcontrol.btnext -text {Next} -command { \
            destroy .rc.provideparameters; \
            set LRP_ProvideParameters_NeedZeroing 1; \
            MatrixDimensionsChanged; ProvideMatrices; \
  } -underline 0

  #states
  set frstate $ent.frstate
  frame $frstate
  label $frstate.lblstates -text {Number of states x (denoted n):} -underline 17
  #validate pn
  entry $frstate.entstates -textvariable {LRPModel_n} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_m] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_r]} { \
          .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong number of states. It must be greater or equal to 1." -type ok -title {Wrong number of states} -icon warning -parent .rc.provideparameters; \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_m] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_r]} { \
             .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
       } else { \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all
  pack $frstate -expand 1 -fill both -pady 2 -padx 2
  pack $frstate.lblstates -side left
  pack $frstate.entstates -side right
  #inputs
  set frinput $ent.frinput
  frame $frinput
  label $frinput.lblinputs -text {Number of inputs u (denoted r):} -underline 17
  entry $frinput.entinputs -textvariable {LRPModel_r} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_n] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_m]} { \
          .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong number of inputs. It must be greater or equal to 1." -type ok -title {Wrong number of inputs} -icon warning -parent .rc.provideparameters; \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_n] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_m]} { \
             .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
       } else { \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all
  pack $frinput -expand 1 -fill both -pady 2 -padx 2
  pack $frinput.lblinputs -side left
  pack $frinput.entinputs -side right
  #outputs
  set froutput $ent.froutput
  frame $froutput
  label $froutput.lbloutputs -text {Number of outputs y (denoted m):} -underline 18
  entry $froutput.entoutputs -textvariable {LRPModel_m} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_n] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_r]} { \
          .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong number of outputs. It must be greater or equal to 1." -type ok -title {Wrong number of outputs} -icon warning -parent .rc.provideparameters; \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_n] && 1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_r]} { \
             .rc.provideparameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
       } else { \
         .rc.provideparameters.mainframe.frcontrol.btnext configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all
  pack $froutput -expand 1 -fill both -pady 2 -padx 2
  pack $froutput.lbloutputs -side left
  pack $froutput.entoutputs -side right

  # set focus and select the whole content of 1st entry
  focus $frstate.entstates
  $frstate.entstates selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext -side right -padx 2 -pady 2
  #bind keyboard
  bind .rc.provideparameters <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.provideparameters <Alt-n> "$frcontrol.btnext invoke"
  bind .rc.provideparameters <Alt-x> "focus $frstate.entstates;"
  bind .rc.provideparameters <Alt-u> "focus $frinput.entinputs;"
  bind .rc.provideparameters <Alt-y> "focus $froutput.entoutputs;"
  grab set .rc.provideparameters
}
# ====================================================== RunCreate ================================
proc RunCreate {name matrix n m} {
  global LRP_Help_SetterResult;
#  global LRPModel_A;
  Create "$name" "$matrix" "$n" "$m";
  tkwait variable LRP_Help_CreateResult;
#  catch {destroy .rc.array};
}

# ====================================================== ProvideMatrices ================================
proc ProvideMatrices {} {
  # provides the matrices A, B, B0, C, D, D0.
  global LRP_Help_ProvideMatricesResult
  global LRP_Help_CreateResult
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_Alpha LRPModel_Beta;
  # create the fonts
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #make sure, there are no duplicates of this window
  catch {bind .rc.providematrices <Destroy> {} }
  catch {destroy .rc.providematrices}
  # setup main window
  toplevel .rc.providematrices
  #do not allow for closing
  wm protocol .rc.providematrices WM_DELETE_WINDOW {#}
  bind .rc.providematrices <Destroy> { \
    set LRP_Help_ProvideMatricesResult 10; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title .rc.providematrices {Provide system matrices}
  wm resizable .rc.providematrices 0 0
  #prepare the main frame
  set main .rc.providematrices.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Enter the system matrices:}
  pack $main.info
  set frcontrol $main.frcontrol
  #pack two buttons
  frame $frcontrol
  #Pack the picture of LRP
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  #Create some free space
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2
  #Create matrices A, B, B0
  frame $ent.state -relief groove -borderwidth 2
  pack $ent.state -padx 2 -pady 2 -expand 1 -fill both

  #Matrix A
  set frmatrixa $ent.state.frmatrixa
  frame $frmatrixa
  label $frmatrixa.lblmatrixa -text {Matrix A:}

#
#
#
global LRP_ProvideParameters_NeedZeroing;
if {$LRP_ProvideParameters_NeedZeroing==1} then { \
  SetToZero LRPModel_A $LRPModel_n $LRPModel_n; \
  SetToZero LRPModel_B $LRPModel_n $LRPModel_r; \
  SetToZero LRPModel_B0 $LRPModel_n $LRPModel_m; \
  SetToZero LRPModel_C $LRPModel_m $LRPModel_n; \
  SetToZero LRPModel_D $LRPModel_m $LRPModel_r; \
  SetToZero LRPModel_D0 $LRPModel_m $LRPModel_m; \
#  SetToZero LRPModel_X0 $LRPModel_n $LRPModel_Beta; \
#  ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));" ;
#  SetToZero LRPModel_Y0 $LRPModel_m $LRPModel_Alpha; \
  set LRP_ProvideParameters_NeedZeroing 0 \
}
#
#
#
# set matrix to zero
#SetToZero LRPModel_A $LRPModel_n $LRPModel_n;
  entry $frmatrixa.entmatrixa -textvariable {LRPModel_A}
  #-validate all -validatecommand {regsub -all {^[0-9]} $LRPModel_A {} LRPModel_A}
# if {[regexp {[^[:digit:]]} $LRPModel_n] == 0 } {return 1} else {return 0;}
#  set LRPModel_A {[1 2 3]}
  button $frmatrixa.bttmatrixa -text {...} -command {\
         RunCreate {matrix A} $LRPModel_A $LRPModel_n $LRPModel_n;\
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixa.entmatrixa; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixa.entmatrixa  selection clear; \
         set LRPModel_A $LRP_Help_SetterResult;\
  } -takefocus 0
  button $frmatrixa.bttrandmatrixa -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixa.entmatrixa; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixa.entmatrixa  selection clear; \
         RandomMatrix {A}; \
  }
  bind $frmatrixa.entmatrixa <Control-Shift-space> "$frmatrixa.bttmatrixa invoke"
  pack $frmatrixa -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixa.lblmatrixa -side left
  pack $frmatrixa.bttmatrixa -side right
  pack $frmatrixa.bttrandmatrixa -side right
  pack $frmatrixa.entmatrixa -side right
  #matrix B
  set frmatrixb $ent.state.frmatrixb
  frame $frmatrixb
  label $frmatrixb.lblmatrixb -text {Matrix B:}
  # set matrix to zero
# SetToZero LRPModel_B $LRPModel_n $LRPModel_r;
  entry $frmatrixb.entmatrixb -textvariable {LRPModel_B}
  button $frmatrixb.bttmatrixb -text {...} -command {\
         RunCreate {matrix B} $LRPModel_B $LRPModel_n $LRPModel_r; \
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixb.entmatrixb; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixb.entmatrixb  selection clear; \
         set LRPModel_B $LRP_Help_SetterResult; \
  } -takefocus 0
  button $frmatrixb.bttrandmatrixb -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixb.entmatrixb; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixb.entmatrixb  selection clear; \
         RandomMatrix {B}; \
  }
  bind $frmatrixb.entmatrixb <Control-Shift-space> "$frmatrixb.bttmatrixb invoke"
  pack $frmatrixb -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixb.lblmatrixb -side left
  pack $frmatrixb.bttmatrixb -side right
  pack $frmatrixb.bttrandmatrixb -side right
  pack $frmatrixb.entmatrixb -side right
  #matrix B0
  set frmatrixb0 $ent.state.frmatrixb0
  frame $frmatrixb0
  label $frmatrixb0.lblmatrixb0 -text {Matrix B0:}
  # set matrix to zero
#  SetToZero LRPModel_B0 $LRPModel_n $LRPModel_m;
  entry $frmatrixb0.entmatrixb0 -textvariable {LRPModel_B0}
  button $frmatrixb0.bttmatrixb0 -text {...} -command { \
         RunCreate {matrix B0} $LRPModel_B0 $LRPModel_n $LRPModel_m; \
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixb0.entmatrixb0; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixb0.entmatrixb0  selection clear; \
         set LRPModel_B0 $LRP_Help_SetterResult;\
  } -takefocus 0
  button $frmatrixb0.bttrandmatrixb0 -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.state.frmatrixb0.entmatrixb0; \
         .rc.providematrices.mainframe.entryframe.state.frmatrixb0.entmatrixb0  selection clear; \
         RandomMatrix {B0}; \
  }
  bind $frmatrixb0.entmatrixb0 <Control-Shift-space> "$frmatrixb0.bttmatrixb0 invoke"
  pack $frmatrixb0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixb0.lblmatrixb0 -side left
  pack $frmatrixb0.bttmatrixb0 -side right
  pack $frmatrixb0.bttrandmatrixb0 -side right
  pack $frmatrixb0.entmatrixb0 -side right


  #Create matrices C, D, D0
  frame $ent.output -relief groove -borderwidth 2
  pack $ent.output -padx 2 -pady 2 -expand 1 -fill both
  set frmatrixc $ent.output.frmatrixc
  frame $frmatrixc
  label $frmatrixc.lblmatrixc -text {Matrix C:}
  # set matrix to zero
#  SetToZero LRPModel_C $LRPModel_m $LRPModel_n;
  entry $frmatrixc.entmatrixc -textvariable {LRPModel_C}
  #-validate all -validatecommand {regsub -all {^[0-9]} $LRPModel_A {} LRPModel_A}
#  set LRPModel_A {[1 2 3]}
  button $frmatrixc.bttmatrixc -text {...} -command {\
         RunCreate {matrix C} $LRPModel_C $LRPModel_m $LRPModel_n;\
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixc.entmatrixc; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixc.entmatrixc  selection clear; \
         set LRPModel_C $LRP_Help_SetterResult;\
  } -takefocus 0
  button $frmatrixc.bttrandmatrixc -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixc.entmatrixc; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixc.entmatrixc  selection clear; \
         RandomMatrix {C}; \
  }
  bind $frmatrixc.entmatrixc <Control-Shift-space> "$frmatrixc.bttmatrixc invoke"
  pack $frmatrixc -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixc.lblmatrixc -side left
  pack $frmatrixc.bttmatrixc -side right
  pack $frmatrixc.bttrandmatrixc -side right
  pack $frmatrixc.entmatrixc -side right
  #matrix D
  set frmatrixd $ent.output.frmatrixd
  frame $frmatrixd
  label $frmatrixd.lblmatrixd -text {Matrix D:}
  # set matrix to zero
#  SetToZero LRPModel_D $LRPModel_m $LRPModel_r;
  entry $frmatrixd.entmatrixd -textvariable {LRPModel_D}
  button $frmatrixd.bttmatrixd -text {...} -command { \
         RunCreate {matrix D} $LRPModel_D $LRPModel_m $LRPModel_r; \
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixd.entmatrixd; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixd.entmatrixd  selection clear; \
         set LRPModel_D $LRP_Help_SetterResult; \
  } -takefocus 0
  button $frmatrixd.bttrandmatrixd -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixd.entmatrixd; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixd.entmatrixd  selection clear; \
         RandomMatrix {D}; \
  }
  bind $frmatrixd.entmatrixd <Control-Shift-space> "$frmatrixd.bttmatrixd invoke"
  pack $frmatrixd -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixd.lblmatrixd -side left
  pack $frmatrixd.bttmatrixd -side right
  pack $frmatrixd.bttrandmatrixd -side right
  pack $frmatrixd.entmatrixd -side right
  #matrix D0
  set frmatrixd0 $ent.output.frmatrixd0
  frame $frmatrixd0
  label $frmatrixd0.lblmatrixd0 -text {Matrix D0:}
  # set matrix to zero
#  SetToZero LRPModel_D0 $LRPModel_m $LRPModel_m;
  entry $frmatrixd0.entmatrixd0 -textvariable {LRPModel_D0}
  button $frmatrixd0.bttmatrixd0 -text {...} -command {\
         RunCreate {matrix D0} $LRPModel_D0 $LRPModel_m $LRPModel_m; \
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixd0.entmatrixd0; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixd0.entmatrixd0  selection clear; \
         set LRPModel_D0 $LRP_Help_SetterResult;\
  } -takefocus 0
  button $frmatrixd0.bttrandmatrixd0 -text {rand} -command {\
         focus .rc.providematrices.mainframe.entryframe.output.frmatrixd0.entmatrixd0; \
         .rc.providematrices.mainframe.entryframe.output.frmatrixd0.entmatrixd0  selection clear; \
         RandomMatrix {D0}; \
  }
  bind $frmatrixd0.entmatrixd0 <Control-Shift-space> "$frmatrixd0.bttmatrixd0 invoke"
  pack $frmatrixd0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixd0.lblmatrixd0 -side left
  pack $frmatrixd0.bttmatrixd0 -side right
  pack $frmatrixd0.bttrandmatrixd0 -side right
  pack $frmatrixd0.entmatrixd0 -side right


  # set focus and select the whole content of 1st entry
  focus $frmatrixa.entmatrixa
  $frmatrixa.entmatrixa selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.providematrices; set LRP_Help_ProvideMatricesResult -1;} -underline 0;
  button $frcontrol.btprev -text {Previous} -command {destroy .rc.providematrices; ProvideParameters;} -underline 0;
  #button $frcontrol.btnext -text {Next} -command {destroy .rc.providematrices; set LRP_Help_ProvideMatricesResult 1; RunMatrixCreator; UpdateMatrices; destroy .rc.providematrices;} -underline 0;
  button $frcontrol.btrandall -text {Randomize all} -command {RandomMatrix {A}; RandomMatrix {B}; RandomMatrix {B0}; RandomMatrix {C}; RandomMatrix {D}; RandomMatrix {D0};} -underline 0
  button $frcontrol.btnext -text {Next} -command {destroy .rc.providematrices; ProvideSimulationParameter;} -underline 0;


  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext -side right -padx 2 -pady 2
  pack $frcontrol.btrandall -side right -padx 2 -pady 2
  #bind keyboard
  bind .rc.providematrices <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.providematrices <Alt-n> "$frcontrol.btnext invoke"
  bind .rc.providematrices <Alt-r> "$frcontrol.btrandall invoke"
  #  grab set .rc.providematrices
}
# ====================================================== END: ProvideMatrices ================================
# ====================================================== MainForm ================================
proc MainForm {} {
  # provides the parameters of LRPModel_n LRPModel_m LRPModel_r.
  #RETURN VALUES:
  # 1 - user clicked "Next"
  # -1 - user clicked "Previous"
  # 10 - user closed window. WARNING: NO RETURN VALUE. CHECK THE LRP_Help_ProvideParametersResult FOR RESULT!!!
  # create the fonts
  global LRP_Help_MainFormResult;
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #unbind the handler
  catch {bind .rc.mainform <Destroy> {} }
  #make sure, there are no duplicates of this window
  catch {destroy .rc.mainform}

  # setup main window
  toplevel .rc.mainform
  #and bind the handler again
  bind .rc.mainform <Destroy> { \
    catch {bind .rc.mainform <Destroy> {} }; \
    catch {set LRP_Help_MainFormResult 10 }; \
  }
  wm title .rc.mainform {Provide system parameters}
  wm resizable .rc.mainform 0 0
  #prepare the main frame
  set main .rc.mainform.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Provide the system parameters:}
  pack $main.info
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2

  #states
  set frstate $ent.frstate
  frame $frstate
  label $frstate.lblstates -text {Number of states x (denoted n):}
  #validate pn
  entry $frstate.entstates -textvariable {LRPModel_n} -validatecommand { \
    if {1 == [regexp {(^\d?\d*[1-9]\d*$)} "%P"] } { \
      return 1; \
    } else { \
   return 0; \
    } \
  } -validate all
  pack $frstate -expand 1 -fill both -pady 2 -padx 2
  pack $frstate.lblstates -side left
  pack $frstate.entstates -side right
  #inputs
  set frinput $ent.frinput
  frame $frinput
  label $frinput.lblinputs -text {Number of inputs u (denoted r):}
  entry $frinput.entinputs -textvariable {LRPModel_r} -validatecommand {
    if {1 == [regexp {(^\d?\d*[1-9]\d*$)} "%P"] } { \
      return 1; \
    } else { \
   return 0; \
    }\
  } -validate all
  pack $frinput -expand 1 -fill both -pady 2 -padx 2
  pack $frinput.lblinputs -side left
  pack $frinput.entinputs -side right
  #outputs7
  set froutput $ent.froutput
  frame $froutput
  label $froutput.lbloutputs -text {Number of outputs y (denoted m):}
  entry $froutput.entoutputs -textvariable {LRPModel_m} -validatecommand {
    if {1 == [regexp {(^\d?\d*[1-9]\d*$)} "%P"] } { \
      return 1; \
    } else { \
   return 0; \
    }\
  } -validate all
  pack $froutput -expand 1 -fill both -pady 2 -padx 2
  pack $froutput.lbloutputs -side left
  pack $froutput.entoutputs -side right

  # set focus and select the whole content of 1st entry
  focus $frstate.entstates
  $frstate.entstates selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  set frcontrol $main.frcontrol
  frame $frcontrol
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
  # Next and Prev buttons
  button $frcontrol.btprev -text {Previous} -command {destroy .rc.mainform; set LRP_Help_MainFormResult -1;}
  button $frcontrol.btnext -text {Next} -command {destroy .rc.mainform; set LRP_Help_MainFormResult 1; }
  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext -side right -padx 2 -pady 2
  #bind keyboard
  bind .rc.mainform <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.mainform <Alt-n> "$frcontrol.btnext invoke"
}

proc fileDialog {w ent operation} {
    #   Type names                Extension(s)        Mac File Type(s)
    #
    #---------------------------------------------------------
    set types {
      {"Matlab files" {.mat}}
#        {"Text files"                {.txt .doc}        }
#        {"Text files"                {}                TEXT}
#        {"Tcl Scripts"                {.tcl}                TEXT}
#        {"C Source Files"        {.c .h}                }
#        {"All Source Files"        {.tcl .c .h}        }
#        {"Image Files"                {.gif}                }
#        {"Image Files"                {.jpeg .jpg}        }
#        {"Image Files"                ""                {GIFF JPEG}}
#        {"All files"                *}
    }
    if {$operation == "open"} {
   set file [tk_getOpenFile -filetypes $types -parent $w]
    } else {
   set file [tk_getSaveFile -filetypes $types -parent $w \
       -initialfile Untitled -defaultextension .mat]
    }
    if {[string compare $file ""]} {
#        $ent delete 0 end
#        $ent insert 0 $file
#        $ent xview end
#puts "$file"
    }
}
proc CalculateInitialConditions {} {
  global LRPModel_X0 LRPModel_Beta LRPModel_n;
  global LRPModel_Y0 LRPModel_m LRPModel_Alpha;
  SetToZero LRPModel_X0 $LRPModel_n [expr "$LRPModel_Beta"];
#  ExecInScilab "temporary=string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0));" "seq";
#  set LRPModel_Y0 {}
  ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));"
#  ExecInScilab "flush"
  #ExecInScilab "temporary=TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));";
  # ExecInScilab "temporary=TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));";
  #ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(rand($LRPModel_m, $LRPModel_Alpha),0)));"
  #ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones(1,12),0)));"
  #ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));"
  #ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones(2, 4),0)));"
#  ExecInScilab "disp(TCL_GetVar('LRPModel_Y0'));";
}
#===================================================================================================
#
#                              The main form of the LRP toolkit
#
# ==================================================================================================
proc MainWindow {} {
#Displays the main window
global LRP_PATH;
global LRPModel_n LRPModel_m LRPModel_r;
global LRPModel_A LRPModel_B LRPModel_B0;
global LRPModel_C LRPModel_D LRPModel_D0;
global LRPModel_Alpha LRPModel_Beta
global LRPModel_X0 LRPModel_Y0
global LRPModel_DISPLAY_NAME;

catch { destroy .rc}
toplevel .rc
#make the size changes impossible
wm resizable .rc 0 0
#
#wm geometry .rc ""
wm geometry .rc 400x280+[expr ([lindex [wm maxsize .] 0]-280)/2]+[expr ([lindex [wm maxsize .] 1]-400)/2]
#wm geometry .rc =400x280
#puts [wm geometry .rc]
set frUpFrame .rc.upframe
set frMidFrame .rc.midframe
set frBottomFrame .rc.bottomframe
set frLeftFrame .rc.midframe.leftframe
set frRightFrame .rc.midframe.rightframe
set frMiddleFrame .rc.midframe.middleframe
frame $frUpFrame
frame $frMidFrame
  frame $frLeftFrame
  frame $frRightFrame
  frame $frMiddleFrame -width 30
frame $frBottomFrame -relief raised -borderwidth 2

pack $frUpFrame -expand 1 -fill both -side top
pack $frMidFrame -expand 1 -fill both
pack $frBottomFrame -padx 6 -pady 6 -expand 1 -fill both -side bottom
pack $frLeftFrame -expand 1 -fill both -side left
pack $frMiddleFrame -expand 0 -fill both -side left
pack $frRightFrame -expand 1 -fill both

frame $frUpFrame.frcraateanewsystem -relief groove -borderwidth 2
pack $frUpFrame.frcraateanewsystem -fill x -expand 1 -side left
button $frUpFrame.frcraateanewsystem.bttcreateANewSystem -text {Create a new system} -command {set LRP_Help_SystemCreatorState 1; RunMatrixCreator;} -underline 9;
pack $frUpFrame.frcraateanewsystem.bttcreateANewSystem -pady 5 -padx 5 -expand 1 -fill both

#Make Width = $frMiddleFrame -width
frame $frUpFrame.frSpacerFrame -width 30
pack $frUpFrame.frSpacerFrame -expand 0 -fill both -side left

frame $frUpFrame.frshowsystemmatrices -relief groove -borderwidth 2
pack $frUpFrame.frshowsystemmatrices -expand 1 -fill x -padx 6 -pady 6 -side left
button $frUpFrame.frshowsystemmatrices.bttshowSystemMatrices -text {Change system matrices} -command {ChangeProcess;} -underline 0;
pack $frUpFrame.frshowsystemmatrices.bttshowSystemMatrices -pady 5 -padx 5 -expand 1 -fill both

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! RIGHT FRAME !!!!!!!!!!!!!!!!!!!!!!!!!!!!
set frSimulationParametersOut $frRightFrame.simulationparametersout
frame $frSimulationParametersOut -relief groove -borderwidth 2
pack $frSimulationParametersOut -expand 1 -fill x -padx 3 -pady 3
set frSimulationParameters $frSimulationParametersOut.simulationparameters
frame $frSimulationParameters -relief groove -borderwidth 2
pack $frSimulationParameters -expand 1 -fill x -padx 3 -pady 3
# ============================== Pass len
set frPassLen $frSimulationParameters.passlen
frame $frPassLen

label $frPassLen.lbl -text {Pass length (alpha)} -width 25
entry $frPassLen.ent -state disabled -textvariable {LRPModel_Alpha} -width 6 -background {light gray}

pack $frPassLen.lbl -side left
pack $frPassLen.ent -expand 1 -fill x  -side right
pack $frPassLen -expand 1 -fill both -padx 6 -pady 6
# ============================== Points on pass
#set frPointsOnPass $frSimulationParameters.frpointsOnPass
#frame $frPointsOnPass

#label $frPointsOnPass.lbl -text {Points on pass (p)} -width 25
#entry $frPointsOnPass.ent -textvariable {LRPModel_P} -width 6 -state disabled -background {light gray}

#pack $frPointsOnPass.lbl -side left
#pack $frPointsOnPass.ent -expand 1 -fill x -side right
#pack $frPointsOnPass -expand 1 -fill both -padx 6 -pady 3

# ============================== Points on pass
set frNumberOfPasses $frSimulationParameters.frnumberOfPasses
frame $frNumberOfPasses

label $frNumberOfPasses.lbl -text {Number of passes (beta)} -width 25
entry $frNumberOfPasses.ent -textvariable {LRPModel_Beta} -width 6 -state disabled -background {light gray}

pack $frNumberOfPasses.lbl -side left
pack $frNumberOfPasses.ent -expand 1 -fill x -side right
pack $frNumberOfPasses -expand 1 -fill both -padx 6 -pady 6

#================================ Change parameters button
button $frSimulationParameters.bttChangeParameters -text {Change parameters} -command { \
        catch { destroy .rc.plotoption}; \
        set LRP_Help_ProvideSimulationParametersResult {}; \
        ChangeSimulationParameter; \
        tkwait variable LRP_Help_ProvideSimulationParametersResult; \
      } -underline 2;
pack $frSimulationParameters.bttChangeParameters -padx 5 -pady 5

#================================= Initial conditions and Inputs
set frInitial $frSimulationParametersOut.frinitial
frame $frInitial
pack $frInitial -fill both -expand 1
button $frInitial.bttInitialConditions -text {Initial conditions} -command {ProvideInitialConditions;} -underline 0;
button $frInitial.bttChangeParameters -text {Inputs}
pack $frInitial.bttInitialConditions -padx 5 -pady 5 -side left
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# TODO:  Not fully implemented yet
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#pack $frInitial.bttChangeParameters -padx 5 -pady 5 -side right
button $frInitial.bttLoadSystem -text {Load} -command {LoadSystem} -underline 0
pack $frInitial.bttLoadSystem -padx 5 -pady 5 -side right
button $frInitial.bttSaveSystem -text {Save Tex} -command {\
       ExecInScilab "TCL_EvalFile('$LRP_PATH/gui/select.tcl');"
       focus -force .win;
       grab .win;
  } -underline 0 -state disabled
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# TODO:  Not fully implemented yet
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#pack $frInitial.bttSaveSystem -padx 5 -pady 5 -side right

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! RIGHT FRAME END !!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! LEFT FRAME !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# ==================================== 3D Plot
set fr3DPlot $frLeftFrame.frthreedplot
frame $fr3DPlot -relief groove -borderwidth 2
pack $fr3DPlot -expand 1 -fill x
button $fr3DPlot.btt3DPlot -text {Plots} -command {ProvidePlotOptions;} -underline 0
pack $fr3DPlot.btt3DPlot -expand 1 -padx 5 -pady 5 -fill both
# ==================================== Stability
set frStability $frLeftFrame.frstability
frame $frStability -relief groove -borderwidth 2
pack $frStability -expand 1 -fill x
button $frStability.bttStability -text {Stability} -command "set LRP_ProvideStability_Result \[SelectStabilityOption {Select stability test}\];"
pack $frStability.bttStability -expand 1 -padx 5 -pady 5 -fill both
button $frStability.bttController -text {Controller design} -command "SelectControllerOption {Select controller};"
pack $frStability.bttController -expand 1 -padx 5 -pady 5 -fill both

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! LEFT FRAME END!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BOTTOM FRAME !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
pack $frBottomFrame -expand 1 -fill x -side left
frame $frBottomFrame.frOptions -relief groove -borderwidth 2
pack $frBottomFrame.frOptions -padx 6 -pady 6 -expand 1 -side left
button $frBottomFrame.frOptions.bttOptions -text {Update variables} -command {ImportFromScilab;} -underline 4
pack $frBottomFrame.frOptions.bttOptions -padx 5 -pady 5 -expand 1 -fill x

frame $frBottomFrame.frExit -relief groove -borderwidth 2
pack $frBottomFrame.frExit -padx 6 -pady 6 -expand 1 -side right
button $frBottomFrame.frExit.bttExit -text {Exit} -command {destroy .rc} -underline 1
pack $frBottomFrame.frExit.bttExit -padx 5 -pady 5 -expand 1 -fill x

focus -force $frUpFrame.frcraateanewsystem.bttcreateANewSystem

# ==================================== Status
global LRP_Help_Status
set frStatus $frBottomFrame.frstatus
frame $frStatus -relief sunken -borderwidth 1
pack $frStatus -expand 1 -fill x
label $frStatus.lblStatus -textvariable {LRP_Help_Status} -width 25
pack $frStatus.lblStatus -expand 0 -padx 0 -pady 0


# ================= Import the data
ImportFromScilab
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BOTTOM FRAME END !!!!!!!!!!!!!!!!!!!!!!!!!!!
#create the menu
set TheMenu ".rc.mainmenu"
set FileMenu "$TheMenu.file"
set EditMenu "$TheMenu.edit"
set DisplayMenu "$TheMenu.display"
#Create the menu
menu $TheMenu

#bind it
.rc config -menu $TheMenu
#Add the main menu
$TheMenu add cascade -label "File" -underline 0 \
      -menu [menu $FileMenu -tearoff 0]
$TheMenu add cascade -label "Edit" -underline 0 \
      -menu [menu $EditMenu -tearoff 0]
$TheMenu add cascade -label "Display" -underline 0 \
      -menu [menu $DisplayMenu -tearoff 0]
#File / New
$FileMenu add command \
    -label "New" \
    -underline 0 \
     -command "$frUpFrame.frcraateanewsystem.bttcreateANewSystem invoke" \
    -accelerator {Alt+N};
#File / Load model...
$FileMenu add command \
    -label "Load model ..." \
    -underline 0 \
     -command "$frInitial.bttLoadSystem invoke" \
    -accelerator {Alt+L};
#File / Save model...
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# TODO:  Not fully implemented yet
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#$FileMenu add command \
#    -label "Save Tex ..." \
#    -underline 0 \
#     -command "$frInitial.bttSaveSystem invoke" \
#    -accelerator {Alt+S};
$FileMenu add separator;
$FileMenu add command \
    -label "Exit" \
    -underline 0 \
     -command "$frBottomFrame.frExit.bttExit invoke" \
    -accelerator {Alt+X};

$EditMenu add command \
    -label "Change matrices..." \
    -underline 0 \
     -command "$frUpFrame.frshowsystemmatrices.bttshowSystemMatrices invoke" \
    -accelerator {Alt+C};

$EditMenu add command \
    -label "Change simulation parameters ..." \
    -underline 0 \
     -command "$frSimulationParameters.bttChangeParameters invoke" \
    -accelerator {Alt+A};
$EditMenu add command \
    -label "Change initial conditions ..." \
    -underline 0 \
     -command "$frInitial.bttInitialConditions invoke" \
    -accelerator {Alt+I};

$DisplayMenu add command \
    -label "Plots" \
    -underline 0 \
     -command "$fr3DPlot.btt3DPlot invoke" \
    -accelerator {Alt+P};

#END: create the menu

bind .rc <Alt-x> "$frBottomFrame.frExit.bttExit invoke"
bind .rc <Alt-p> "$fr3DPlot.btt3DPlot invoke"
bind .rc <Alt-s> "$frInitial.bttSaveSystem invoke"
bind .rc <Alt-l> "$frInitial.bttLoadSystem invoke"
bind .rc <Alt-n> "$frUpFrame.frcraateanewsystem.bttcreateANewSystem invoke"
bind .rc <Alt-c> "$frUpFrame.frshowsystemmatrices.bttshowSystemMatrices invoke"
bind .rc <Alt-a> "$frSimulationParameters.bttChangeParameters invoke"
bind .rc <Alt-i> "$frInitial.bttInitialConditions invoke"
focus -force $frSimulationParameters.bttChangeParameters

wm geometry .rc ""
wm deiconify .rc
regexp {([0-9]{1,})x([0-9]{1,})} [wm geometry .rc] dummy width height
if {$width<350} {set width 400;}
if {$height<200} {set height 280;}
#puts 400x280+[expr ([lindex [wm maxsize .] 0]-280)/2]+[expr ([lindex [wm maxsize .] 1]-400)/2];
set geom "$width\x$height+[expr ([lindex [wm maxsize .] 0]-$width)/2]+[expr ([lindex [wm maxsize .] 1]-$height)/2]"
#puts $geom
#tk_messageBox -parent . -message $geom
wm geometry .rc $geom
}

proc ProvideSimulationParameter {} {
  # provides the parameters of LRPModel_n LRPModel_m LRPModel_r.
  #RETURN VALUES:
  # 1 - user clicked "Next"
  # -1 - user clicked "Previous"
  # 10 - user closed window. WARNING: NO RETURN VALUE. CHECK THE LRP_Help_ProvideParametersResult FOR RESULT!!!
  # create the fonts
  global LRP_Help_ProvideSimulationParametersResult;
  global LRPModel_Alpha LRPModel_Beta;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_n LRPModel_m LRPModel_r;

  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #unbind the handler
  catch {bind .rc.provideSimulationParameters <Destroy> {} }
  #make sure, there are no duplicates of this window
  catch {destroy .rc.provideSimulationParameters}

  # setup main window
  toplevel .rc.provideSimulationParameters
# set x [expr {([winfo screenwidth .]-[winfo width .rc.provideSimulationParameters])/2}]
# set y [expr {([winfo screenheight .]-[winfo height .rc.provideSimulationParameters])/2}]
#wm geometry  .rc.provideSimulationParameters +$x+$y


  wm protocol .rc.provideSimulationParameters WM_DELETE_WINDOW {#}
  #and bind the handler again
  bind .rc.provideSimulationParameters <Destroy> { \
    if [regexp {(^[0-9]{0,}[1-9][0-9]{0,}$)} $LRPModel_Alpha]==0 {
       tk_messageBox -message "Wrong pass length. Assumed alpha=10!" -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
       set LRPModel_Alpha {10};
    };
    if [regexp {(^[0-9]{0,}[1-9][0-9]{0,}$)} $LRPModel_Beta]==0 {
       tk_messageBox -message "Wrong number of passes. Assumed beta=10!" -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
       set LRPModel_Beta {20};
    };
    catch {bind .rc.provideSimulationParameters <Destroy> {} }; \
    catch {set LRP_Help_ProvideSimulationParametersResult 10 }; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title .rc.provideSimulationParameters {Provide system simulation parameters}
  wm resizable .rc.provideSimulationParameters 0 0
  #Set the names
  set main .rc.provideSimulationParameters.mainframe
  set ent $main.entryframe
  set frstate $ent.frstate
  set frcontrol $main.frcontrol
  set froutput $ent.froutput
  frame $main -relief groove -borderwidth 2

  frame $frcontrol
  # Next and Prev buttons
#   button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideSimulationParameters; set LRP_Help_ProvideSimulationParametersResult -1;} -underline 0
#   button $frcontrol.btnext -text {Finish} -command {destroy .rc.provideSimulationParameters; set LRP_Help_ProvideSimulationParametersResult 1; } -underline 0
  button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideSimulationParameters; ProvideMatrices} -underline 0
  button $frcontrol.btnext -text {Finish} -command {destroy .rc.provideSimulationParameters; \
         SetToZero LRPModel_X0 $LRPModel_n [expr "$LRPModel_Beta"] ; \
         ExecInScilab "TCL_SetVar('LRPModel_Y0',string(sci2exp(ones($LRPModel_m, $LRPModel_Alpha),0)));"; \
         ActivateMainWindow; UpdateMatrices} -underline 0


  pack $main
  label $main.info -font {infofont} -text {Provide the system simulation parameters:}
  pack $main.info
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2

  #states
  frame $frstate
  label $frstate.lblstates -text {Pass length (alpha):} -underline 17
  #validate pn


entry $frstate.entstates -textvariable {LRPModel_Alpha} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Beta]} { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong pass length. It must be greater or equal to 1." -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Beta]} { \
             .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
             .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state normal; \
          }; \
       } else { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all

#   entry $frstate.entstates -textvariable {LRPModel_Alpha} -validatecommand { \
#     if {1 == [regexp {(^\d?\d*[1-9]\d*$)} "%P"] } { \
#       return 1; \
#     } else { \
#  return 0; \
#     } \
#   } -validate all
  pack $frstate -expand 1 -fill both -pady 2 -padx 2
  pack $frstate.lblstates -side left
  pack $frstate.entstates -side right
  #outputs
  frame $froutput
  label $froutput.lbloutputs -text {Number of passes to simulate (Beta):} -underline 18


  entry $froutput.entoutputs -textvariable {LRPModel_Beta} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Alpha]} { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong number of passes. It must be greater or equal to 1." -type ok -title {Wrong number of passes} -icon warning  -parent .rc.provideSimulationParameters; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Alpha]} { \
             .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
             .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state normal; \
          }; \
       } else { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btprev configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all

#   entry $froutput.entoutputs -textvariable {LRPModel_Beta} -validatecommand {
#     if {1 == [regexp {(^\d?\d*[1-9]\d*$)} "%P"] } { \
#       return 1; \
#     } else { \
#  return 0; \
#     }\
#   } -validate all
  pack $froutput -expand 1 -fill both -pady 2 -padx 2
  pack $froutput.lbloutputs -side left
  pack $froutput.entoutputs -side right

  # set focus and select the whole content of 1st entry
  focus $frstate.entstates
  $frstate.entstates selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext -side right -padx 2 -pady 2


  #bind keyboard
  bind .rc.provideSimulationParameters <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.provideSimulationParameters <Alt-f> "$frcontrol.btnext invoke"
  bind .rc.provideSimulationParameters <Alt-a> "focus $frstate.entstates;"
  bind .rc.provideSimulationParameters <Alt-b> "focus $froutput.entoutputs;"
  focus -force $frstate.entstates
  grab set .rc.provideSimulationParameters
}
# ====================================================== ImportFromScilab ================================
proc ImportFromScilab {} {
  global LRP_PATH
  global LRPModel_DISPLAY_NAME
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_Alpha LRPModel_Beta
  global LRPModel_X0 LRPModel_Y0
 global LRP_Help_Status
  set LRP_Help_Status {Importing data, please wait};
  DeactivateMainWindow
#  ExecInScilab "flush"
  #puts $LRP_PATH/gui/SetValues.sci
  set test 0;
  ExecInScilab "exec('$LRP_PATH/gui/SetValues.sci');"
  update;
#  ExecInScilab "flush"
#  ExecInScilab {          TCL_SetVar('LRPModel_Alpha',string(sci2exp(lrp.dim.alpha,0)));}
##  ExecInScilab "TCL_SetVar('LRPModel_A',string(sci2exp(lrp.mat.A,0)))"
#  ExecInScilab "          TCL_SetVar('LRPModel_A',string(sci2exp(lrp.mat.A,0)));"
#  ExecInScilab "          TCL_SetVar('LRPModel_B',string(sci2exp(lrp.mat.B,0)));"
#  ExecInScilab "          TCL_SetVar('LRPModel_B0',string(sci2exp(lrp.mat.B0,0)));"
#  ExecInScilab "          TCL_SetVar('LRPModel_C',string(sci2exp(lrp.mat.C,0)));"
#  ExecInScilab "          TCL_SetVar('LRPModel_D',string(sci2exp(lrp.mat.D,0)));"
#  ExecInScilab "          TCL_SetVar('LRPModel_D0',string(sci2exp(lrp.mat.D0,0)));"
  ExecInScilab "TCL_EvalStr('ActivateMainWindow');"
  ScilabEval "flush"
#  ExecInScilab "flush"
  #ExecInScilab {      TCL_SetVar('LRPModel_A','xxxx')}
  UpdateMainFormTitle;
}
# ====================================================== UpdateMainFormTitle ================================
proc UpdateMainFormTitle {} {
	global LRPModel_DISPLAY_NAME;
  # set the main form title
  puts "Updating title to $LRPModel_DISPLAY_NAME";
  wm title .rc "Main form - $LRPModel_DISPLAY_NAME";
}
#======================================================= SaveMatrices ==================================
proc SaveMatrices {} {
#Saves all the matrices and the model parameters to the file "system.sci"
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_Alpha LRPModel_Beta;
  global LRPModel_n LRPModel_r LRPModel_m;
  global LRP_Help_Status;
  global LRPModel_X0 LRPModel_Y0;
  global LRP_PATH;
  global TMP_DIR;

  set  LRP_Help_Status {Setting data. Please wait.}
#Prepare file - if it exist: delete it.
   catch "file delete \"$TMP_DIR/system.sci\";"
   set myfile [open "$TMP_DIR/system.sci" w]
#Create the model
   puts $myfile "// AUTOMATICALLY GENERATED FILE - DO NOT MODIFY, CHANGES WILL BE LOST!\n"
   puts $myfile "//   To change this file use ''Create a new system'' in "
   puts $myfile "//     the LRP Toolkit GUI, accessible from the LRP menu\n"
   puts $myfile "clear lrp; //Get rid of the old variable\n"
   puts $myfile {// First create an empty lrp structure that will be filled later}
   puts $myfile {lrp=createStubLRP(); }
#Insert the variables : ALPHA VALUE
   puts $myfile {}
   puts $myfile {lrp.type=0; //0 means "classic" system}
   puts $myfile {}
   puts -nonewline $myfile {lrp.dim.alpha=}
   puts $myfile "$LRPModel_Alpha; //number of points"
   puts $myfile "\n// The system will be simulated upto beta pass, from 0 to the ''beta''."
   puts -nonewline $myfile {lrp.dim.beta=}
   puts $myfile "$LRPModel_Beta;"
   puts $myfile {
// In the classic LRP setting, the points are numbered from pmin=0 to pmax=alpha-1}
   puts $myfile {lrp.dim.pmin=0;}
   puts $myfile {lrp.dim.pmax=lrp.dim.alpha-1;}
   puts $myfile {
// In the classic LRP setting, the passes are numbered from kmin=0 to kmax=beta-1
}
   puts $myfile {lrp.dim.kmin=0;}
   puts $myfile {lrp.dim.kmax=lrp.dim.beta - 1;}
   puts $myfile {}
   puts $myfile "lrp.dim.n=$LRPModel_n; //number of states x";
   puts $myfile "lrp.dim.r=$LRPModel_r; //number of inputs u";
   puts $myfile "lrp.dim.m=$LRPModel_m; //number of outputs y";
   puts $myfile {
// The system matrices}

#Insert the variables : A MATRIX
   puts $myfile {lrp.mat.A=[}
   set lst [split $LRPModel_A {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
#Insert the variables : B MATRIX
  puts $myfile {lrp.mat.B=[}
   set lst [split $LRPModel_B {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
#Insert the variables : B0 MATRIX
  puts $myfile {lrp.mat.B0=[}
   set lst [split $LRPModel_B0 {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
puts $myfile {}
#Insert the variables : C MATRIX
  puts $myfile {lrp.mat.C=[}
   set lst [split $LRPModel_C {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
#Insert the variables : D MATRIX
  puts $myfile {lrp.mat.D=[}
   set lst [split $LRPModel_D {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
#Insert the variables : D0 MATRIX
  puts $myfile {lrp.mat.D0=[}
   set lst [split $LRPModel_D0 {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
puts $myfile {
//Boundary conditions}
#Insert the variables : X0 MATRIX
  puts $myfile {lrp.ini.x0=[}
   set lst [split $LRPModel_X0 {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
# #Insert the variables : Y0 MATRIX
  puts $myfile {lrp.ini.y0=[}
   set lst [split $LRPModel_Y0 {[;]}]
     foreach {x} $lst {
     if {$x != ""} {
      puts $myfile $x;
    }
  }
  puts $myfile {];}
puts $myfile {
// Required for the toolkit. The lrp.controller(0) should contain
//   copies of all the model matrices
lrp.controller(1).A=lrp.mat.A;
lrp.controller(1).B=lrp.mat.B;
lrp.controller(1).B0=lrp.mat.B0;
lrp.controller(1).C=lrp.mat.C;
lrp.controller(1).D=lrp.mat.D;
lrp.controller(1).D0=lrp.mat.D0;
}
#
# close the file
#
  close $myfile
ActivateMainWindow;
}
# ====================================================== UpdateMatrices ================================
proc UpdateMatrices {} {
  global LRP_PATH;
  global TMP_DIR;
  
  DeactivateMainWindow;
  SaveMatrices;
  #Execute the file inside Scilab
  ExecInScilab "exec('$TMP_DIR/system.sci',-1);"
  ExecInScilab "TCL_EvalStr('ActivateMainWindow');"  
}

# ====================================================== UpdateMatrices ================================
proc ExportSimulationParameters {} {
  # CAUTION: the lrp variable must exist!
  global LRPModel_Alpha LRPModel_Beta;
  global LRP_Help_Status;
  DeactivateMainWindow;
  set  LRP_Help_Status {Setting data. Please wait.}
  #ExecInScilab {          lrp=tlist(['lrpmodel';'A';'B0';'B';'C';'D0';'D';'alpha';'x0';'Y0'],A,B0,B,C,D0,D,alpha,x0,Y0);}
#  ExecInScilab {          lrp=tlist(['lrpmodel';'A';'B0';'B';'C';'D0';'D';'alpha';'x0';'Y0']);}
  ExecInScilab "          lrp.dim.alpha = $LRPModel_Alpha;"
  ExecInScilab "          TCL_EvalStr('ActivateMainWindow');"
}

proc SaveSystem {} {
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_Beta LRPModel_Alpha;
  # display the save requester
      set types {
   {"Latex files" {.tex}}
   {"All files"                *}
      }
   set file_name [tk_getSaveFile -filetypes $types -parent .rc \
       -initialfile "system.tex" -defaultextension ".tex" -title {Save Latex}]
#
    if {[string compare $file_name ""] != 0} {
   catch {file delete $file_name; }
   set myfile [open $file_name w]
#Create the model
puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "A="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_A" "$LRPModel_n"  "$LRPModel_n"]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "B="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_B" "$LRPModel_n"  "$LRPModel_r"]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "B_0="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_B0" "$LRPModel_n"  "$LRPModel_m"]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "C="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_C" "$LRPModel_m"  "$LRPModel_n"]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "D="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_D" "$LRPModel_m"  "$LRPModel_r"]]
  puts $myfile "\\end\{displaymath\}"
    puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "D_0="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_D0" "$LRPModel_m"  "$LRPModel_m"]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "X_0="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_X0" "$LRPModel_n"  [expr "$LRPModel_Beta"] ]]
  puts $myfile "\\end\{displaymath\}"

puts $myfile ""
   puts $myfile "\\begin\{displaymath\}"
   puts $myfile "Y_0="
   #Insert the variables : ALPHA VALUE
  puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_Y0" "$LRPModel_m"  "$LRPModel_Alpha"]]
  puts $myfile "\\end\{displaymath\}"

close $myfile
}
#       set types {
#  {"Matlab files" {.mat}}
#  {"All files"                *}
#       }
#  set file_name [tk_getSaveFile -filetypes $types -parent .rc \
#      -initialfile "lrp_model.mat" -defaultextension ".mat" -title {Save model}]
# #
#     if {[string compare $file_name ""] != 0} {
# #      puts "'$file_name'"
#       UpdateMatrices;
#       DeactivateMainWindow;
#       set  LRP_Help_Status {Saving. Please wait}
#       ExecInScilab "          mtlb_save('$file_name',lrp);"
#       ExecInScilab "          TCL_EvalStr('ActivateMainWindow');"
#     }

}
proc LoadSystem {} {
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_n LRPModel_m LRPModel_r;
  # display the save requester
#  set file_name {}
      set types {
   {"Matlab files" {.mat}}
   {"All files"                *}
      }
   set file_name [tk_getOpenFile -filetypes $types -parent .rc \
       -initialfile "lrp_model.mat" -defaultextension ".mat" -title {Load model}]
#
    if {[string compare $file_name ""] != 0} {
#      puts "'$file_name'"
      DeactivateMainWindow;
      set  LRP_Help_Status {Loading. Please wait}
#      ExecInScilab "getf('LoadFromMatlabAndConvert.sci');"
#      ExecInScilab "flush"
      ExecInScilab "\[lrp\]=LoadFromMatlabAndConvert('$file_name');" "seq" "sync";
#      ExecInScilab "flush"
#      ExecInScilab "\[lrp\]=LoadFromMatlabAndConvert('$file_name');";
#      ExecInScilab "          mtlb_load('$file_name',lrp);"
      ImportFromScilab;
#      ExecInScilab "          TCL_EvalStr('ActivateMainWindow');"
    }

}

# ====================================================== ProvideInitialConditions ================================
proc ProvideInitialConditions {} {
  # provides the initial conditions: X0 and Y0.
#   global LRPModel_A LRPModel_B LRPModel_B0;
#   global LRPModel_C LRPModel_D LRPModel_D0;
global LRP_PATH
if { [catch {image create photo LRP_Image -file "$LRP_PATH/gui/LRP.gif"}] } {
image create photo LRP_Image -file "$LRP_PATH/gui/LRP.gif"
}

  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_Alpha LRPModel_Beta;
  # create the fonts
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #make sure, there are no duplicates of this window
  catch {bind .rc.provideinitialconditions <Destroy> {} }
  catch {destroy .rc.provideinitialconditions}
  # setup main window
  toplevel .rc.provideinitialconditions
  bind .rc.provideinitialconditions <Destroy> { \
    set LRP_Help_ProvideMatricesResult 10; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
#
#
# Zero the vectors X0 and Y0, if needed
# global LRP_ProvideInitialConditions_NeedZeroing;
# if {$LRP_ProvideInitialConditions_NeedZeroing==1} then { \
#    SetToZero LRPModel_X0 $LRPModel_n $LRPModel_Alpha; \
#    SetToZero LRPModel_Y0 $LRPModel_m LRPModel_Beta; \
#   set LRP_ProvideInitialConditions_NeedZeroing 0 \
# }
#
#
#

  wm title .rc.provideinitialconditions {Change the initial conditions}
  wm resizable .rc.provideinitialconditions 0 0
  #prepare the main frame
  set main .rc.provideinitialconditions.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Enter the initial conditions:}
  pack $main.info
  set frcontrol $main.frcontrol
  #pack two buttons
  frame $frcontrol
  #Pack the picture of LRP
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  #Create some free space
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2
  #Create matrices X0, Y0
  frame $ent.state -relief groove -borderwidth 2
  pack $ent.state -padx 2 -pady 2 -expand 1 -fill both

  #Matrix X0
  set frmatrixx0 $ent.state.frmatrixx0
  frame $frmatrixx0
  label $frmatrixx0.lblmatrixx0 -text {Matrix X0 (first NaN added automatically)}

# set matrix to zero
#SetToZero LRPModel_A $LRPModel_n $LRPModel_n;
  entry $frmatrixx0.entmatrixx0 -textvariable {LRPModel_X0}
  #-validate all -validatecommand {regsub -all {^[0-9]} $LRPModel_A {} LRPModel_A}
# if {[regexp {[^[:digit:]]} $LRPModel_n] == 0 } {return 1} else {return 0;}
#  set LRPModel_A {[1 2 3]}
  button $frmatrixx0.bttmatrixx0 -text {...} -command {RunCreate {matrix X0} $LRPModel_X0 $LRPModel_n [expr "$LRPModel_Beta"]; set LRPModel_X0 $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixx0.bttrandmatrixx0 -text {rand} -command {RandomMatrix {X0};}
  bind $frmatrixx0.entmatrixx0 <Control-Shift-space> "$frmatrixx0.bttmatrixx0 invoke"
  pack $frmatrixx0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixx0.lblmatrixx0 -side left
  pack $frmatrixx0.bttmatrixx0 -side right
  pack $frmatrixx0.bttrandmatrixx0 -side right
  pack $frmatrixx0.entmatrixx0 -side right
  #matrix Y0
  set frmatrixy0 $ent.state.frmatrixy0
  frame $frmatrixy0
  label $frmatrixy0.lblmatrixy0 -text {Matrix Y0}
  # set matrix to zero
# SetToZero LRPModel_B $LRPModel_n $LRPModel_r;
  entry $frmatrixy0.entmatrixy0 -textvariable {LRPModel_Y0}
  button $frmatrixy0.bttmatrixy0 -text {...} -command {RunCreate {matrix Y0} $LRPModel_Y0 $LRPModel_m $LRPModel_Alpha; set LRPModel_Y0 $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixy0.bttrandmatrixy0 -text {rand} -command {RandomMatrix {Y0}}
  bind $frmatrixy0.entmatrixy0 <Control-Shift-space> "$frmatrixy0.bttmatrixy0 invoke"
  pack $frmatrixy0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixy0.lblmatrixy0 -side left
  pack $frmatrixy0.bttmatrixy0 -side right
  pack $frmatrixy0.bttrandmatrixy0 -side right
  pack $frmatrixy0.entmatrixy0 -side right
#
#
#   # set focus and select the whole content of 1st entry
  focus $frmatrixx0.entmatrixx0
  $frmatrixx0.entmatrixx0 selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideinitialconditions; set LRP_Help_provideinitialconditionsResult -1;} -underline 0;
  button $frcontrol.btprev -text {Ok} -command {destroy .rc.provideinitialconditions;  UpdateMatrices;} -underline 0;
  #button $frcontrol.btnext -text {Next} -command {destroy .rc.provideinitialconditions; set LRP_Help_provideinitialconditionsResult 1; RunMatrixCreator; UpdateMatrices; destroy .rc.provideinitialconditions;} -underline 0;
#  button $frcontrol.btnext -text {Next} -command {destroy .rc.provideinitialconditions; ProvideSimulationParameter;} -underline 0;

  #bind keyboard
  bind .rc.provideinitialconditions <Alt-o> "$frcontrol.btprev invoke"
#  bind .rc.provideinitialconditions <Alt-n> "$frcontrol.btnext invoke"

  pack $frcontrol.btprev -padx 2 -pady 2
#  pack $frcontrol.btnext -side right -padx 2 -pady 2
  grab set $main
}
# ====================================================== END: ProvideInitialConditions ================================

proc RejectZeroValue {val1 val2 val3 val4 val5 val6} {
  set res $val1;
  if {$val2>0} then {
     lappend res $val2;
  }
  if {$val3>0} then {
     lappend res $val3;
  }
  if {$val4>0} then {
     lappend res $val4;
  }
  if {$val5>0} then {
     lappend res $val5;
  }
  if {$val6>0} then {
     lappend res $val6;
  }
  set res [join $res  ", "];
  return $res;
}
proc ProvidePlotOptions {} {
  #Setup the font
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.plotoption
  #If the main form exist - destroy it
  catch {bind .rc.plotoption <Destroy> {} }
   #make sure, there are no duplicates of this window
   catch {destroy .rc.plotoption;}
  # ... so we can build a new one
  toplevel $main
  # setup main window
  bind .rc.plotoption <Destroy> { \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title $main {Provide plot options}
  #make the form not resizeable
  wm resizable $main 0 0
  label $main.lbl -text {Select plot options} -font infofont
  pack $main.lbl

  labelframe $main.frame -relief groove -borderwidth 2 -text {2D pass profile plots}
  set fr $main.frame
  button $fr.state -text {Along the pass} -command {ProvideStatePlotOptions} -underline 0 -width 15
  pack $fr.state -padx 5 -pady 5
  button $fr.output -text {Pass to pass} -command {ProvideOutputPlotOptions} -underline 0 -width 15
  pack $fr.output -padx 5 -pady 5
  pack $fr -fill both  -padx 5 -pady 5
  labelframe $main.threedframe -relief groove -borderwidth 2 -text {3D plots}
  set frthreed $main.threedframe
  button $frthreed.btt -text {3D plot} -command {Provide3DPlotOptions} -underline 1  -width 15
  pack $frthreed.btt -padx 3 -pady 3
  pack $frthreed -fill both -padx 5 -pady 5
  button $main.bttclear -text {Remove all plots} -command {ExecInScilab "xdel(winsid());" ;} -underline 0 -width 15
  pack $main.bttclear -padx 3 -pady 10
  button $main.bttcancel -text {Close} -command "destroy $main; ActivateMainWindow;" -underline 0
  pack $main.bttcancel -side right -padx 4 -pady 4
  bind $main <Alt-c> "$main.bttcancel invoke"
  bind $main <Alt-d> "$frthreed.btt invoke"
  bind $main <Alt-r> "$main.bttclear invoke"
  bind $main <Alt-a> "$fr.state invoke"
  bind $main <Alt-p> "$fr.output invoke"
  focus -force $fr.state

}


proc ProvideStatePlotOptions {} {
  global LRPModel_Beta
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.option
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main
  wm title $main {Provide plot options}
  #make the form not resizeable
  wm resizable $main 0 0
  global LRP_ProvideStateNumberToPlot_Result;
  #The header
  label $main.lbl -text {Along the pass plot: options} -font infofont
  pack $main.lbl
  set fr $main.frame
  labelframe $main.frame -relief groove -borderwidth 2 -text {Which profile to plot. Note: 1st state profile is always NaN}
  set fr $main.frame.fr1
  frame $fr
  spinbox $fr.spinbox1 -from 1 -to $LRPModel_Beta -textvariable {statespinbox1}
  pack $fr.spinbox1 -side left -padx 3 -pady 3
  spinbox $fr.spinbox2 -from 0 -to $LRPModel_Beta -textvariable {statespinbox2}
  pack $fr.spinbox2 -side left -padx 3 -pady 3
  spinbox $fr.spinbox3 -from 0 -to $LRPModel_Beta -textvariable {statespinbox3}
  pack $fr.spinbox3 -padx 3 -pady 3
  pack $fr
  set fr $main.frame.fr2
  frame $fr
  spinbox $fr.spinbox4 -from 0 -to $LRPModel_Beta -textvariable {statespinbox4}
  pack $fr.spinbox4 -side left -padx 3 -pady 3
  spinbox $fr.spinbox5 -from 0 -to $LRPModel_Beta -textvariable {statespinbox5}
  pack $fr.spinbox5 -side left -padx 3 -pady 3
  spinbox $fr.spinbox6 -from 0 -to $LRPModel_Beta -textvariable {statespinbox6}
  pack $fr.spinbox6 -padx 3 -pady 3
  pack $fr
  pack $main.frame
  #STATE FRAME
  labelframe $main.frstate -relief groove -borderwidth 2 -text {State}
  set frstate $main.frstate
  label $frstate.entlabel -text {State to plot:  }
  pack $frstate.entlabel -side left
  label $frstate.ent -textvariable {LRP_ProvideStateNumberToPlot_Result} -font infofont
  button $frstate.btt -text {Change} -command "set LRP_ProvideStateNumberToPlot_Result \[PickRadioOption \$LRPModel_n {Provide state number to plot} \$LRP_ProvideStateNumberToPlot_Result\]" -underline 1
  pack $frstate.ent -side left -padx 10
  pack $frstate.btt -side left
  button $frstate.bttplotalong -text {Plot state} -command { \
               set result [RejectZeroValue $statespinbox1 $statespinbox2 $statespinbox3 $statespinbox4 $statespinbox5 $statespinbox6]; \
               ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_PlotAPState.sce\");"; \
#                ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();"; \
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"alongthepass\", \"state\", $LRP_ProvideStateNumberToPlot_Result, list(\[$result\]), list(\"all\"), lrpMergedDatToPlots);"; \
# ExecInScilab "showInfoAboutPlot=%F;"; \
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";"; \
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);"; \
#               ExecInScilab "plotHorProfile(gso.state, \[$result\], $LRP_ProvideStateNumberToPlot_Result, 3, 'State X_$LRP_ProvideStateNumberToPlot_Result');" ; \
  } -underline 5
  pack $frstate.bttplotalong -side right -padx 5
  #The main part
  pack $frstate -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: STATE FRAME


  #OUTPUT FRAME
  labelframe $main.froutput -relief groove -borderwidth 2 -text {Output}
  set froutput $main.froutput
  label $froutput.entlabel -text {Output to plot:  }
  pack $froutput.entlabel -side left
  label $froutput.ent -textvariable {LRP_ProvideOutputNumberToPlot_Result} -font infofont
  button $froutput.btt -text {Change} -command "set LRP_ProvideOutputNumberToPlot_Result \[PickRadioOption \$LRPModel_m {Provide output number to plot} \$LRP_ProvideOutputNumberToPlot_Result\]" -underline 1
  pack $froutput.ent -side left -padx 10
  pack $froutput.btt -side left
  button $froutput.bttplotalong -text {Plot output} -command { \
               set result [RejectZeroValue $statespinbox1 $statespinbox2 $statespinbox3 $statespinbox4 $statespinbox5 $statespinbox6]; \
               ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_PlotAPOutput.sce\");"; \
# ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();";
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"alongthepass\", \"output\", $LRP_ProvideOutputNumberToPlot_Result, list(\[$result\]), list(\"all\"), lrpMergedDatToPlots);";
# ExecInScilab "showInfoAboutPlot=%F;";
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";";
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);";
  } -underline 5

  pack $froutput.bttplotalong -side right -padx 5
#   button $froutput.bttplotacross -text {Plot ACROSS the pass}
#   pack $froutput.bttplotacross -side right -padx 5
  pack $main.froutput -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: OUTPUT FRAME


  button $main.bttclose -text {Close} -command "destroy $main" -underline 0
  pack $main.bttclose -padx 5 -pady 5 -side right
  bind $main <Alt-c> "$main.bttclose invoke"
  bind $main <Alt-h> "$frstate.btt invoke"
  bind $main <Alt-a> "$froutput.btt invoke"
  bind $main <Alt-o> "$froutput.bttplotalong invoke"
  bind $main <Alt-s> "$frstate.bttplotalong invoke"
  #setup the frame
  focus -force $main.frame.fr1.spinbox1
}
proc ProvideOutputPlotOptions {} {
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.option
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main
  wm title $main {Provide plot options}
  #make the form not resizeable
  wm resizable $main 0 0
  global LRP_ProvideOutputNumberToPlot_Result;
  global LRPModel_Alpha
  #The header
  label $main.lbl -text {Pass to pass plot: options} -font infofont
  pack $main.lbl
  set fr $main.frame
  labelframe $main.frame -relief groove -borderwidth 2 -text {Which points on each pass to plot}
  set fr $main.frame.fr1
  frame $fr
  spinbox $fr.spinbox1 -from 1 -to $LRPModel_Alpha -textvariable {outputspinbox1}
  pack $fr.spinbox1 -side left -padx 3 -pady 3
  spinbox $fr.spinbox2 -from 0 -to $LRPModel_Alpha -textvariable {outputspinbox2}
  pack $fr.spinbox2 -side left -padx 3 -pady 3
  spinbox $fr.spinbox3 -from 0 -to $LRPModel_Alpha -textvariable {outputspinbox3}
  pack $fr.spinbox3 -padx 3 -pady 3
  pack $fr
  set fr $main.frame.fr2
  frame $fr
  spinbox $fr.spinbox4 -from 0 -to $LRPModel_Alpha -textvariable {outputspinbox4}
  pack $fr.spinbox4 -side left -padx 3 -pady 3
  spinbox $fr.spinbox5 -from 0 -to $LRPModel_Alpha -textvariable {outputspinbox5}
  pack $fr.spinbox5 -side left -padx 3 -pady 3
  spinbox $fr.spinbox6 -from 0 -to $LRPModel_Alpha -textvariable {outputspinbox6}
  pack $fr.spinbox6 -padx 3 -pady 3
  pack $fr
  pack $main.frame
  #STATE FRAME
  labelframe $main.frstate -relief groove -borderwidth 2 -text {State}
  set frstate $main.frstate
  label $frstate.entlabel -text {State to plot:  }
  pack $frstate.entlabel -side left
  label $frstate.ent -textvariable {LRP_ProvideStateNumberToPlot_Result} -font infofont
  button $frstate.bttchange -text {Change} -command "set LRP_ProvideStateNumberToPlot_Result \[PickRadioOption \$LRPModel_n {Provide state number to plot} \$LRP_ProvideStateNumberToPlot_Result\]" -underline 1
  pack $frstate.ent -side left -padx 10
  pack $frstate.bttchange -side left
  button $frstate.bttplotalong -text {Plot state} -command { \
               set result [RejectZeroValue $outputspinbox1 $outputspinbox2 $outputspinbox3 $outputspinbox4 $outputspinbox5 $outputspinbox6]; \
ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_PlotPPState.sce\");"; \
# ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();";
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"passtopass\", \"state\", $LRP_ProvideStateNumberToPlot_Result, list(\[$result\]), list(\"all\"), lrpMergedDatToPlots);";
# ExecInScilab "showInfoAboutPlot=%F;";
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";";
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);";
  } -underline 5
  #TCL_messageBox -message "STATEXX ALONG:\[$result\]" -type ok; \  # ExecInScilab {temporaryowiec=23;}
  # ExecInScilab "plotHorProfile(X, \[$result\], $LRP_ProvideStateNumberToPlot_Result, 3);"
  pack $frstate.bttplotalong -side right -padx 5
#   button $frstate.bttplotacross -text {Plot ACROSS the pass} -command { \
#                set result [RejectZeroValue $statespinbox1 $statespinbox2 $statespinbox3 $statespinbox4 $statespinbox5 $statespinbox6]; \
#                ExecInScilab "plotVertProfile(X, \[$result\], $LRP_ProvideStateNumberToPlot_Result, 3);"; \
#   }
#   pack $frstate.bttplotacross -side right -padx 5
  #The main part
  pack $frstate -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: STATE FRAME


  #OUTPUT FRAME
  labelframe $main.froutput -relief groove -borderwidth 2 -text {Output}
  set froutput $main.froutput
  label $froutput.entlabel -text {Output to plot:  }
  pack $froutput.entlabel -side left
  label $froutput.ent -textvariable {LRP_ProvideOutputNumberToPlot_Result} -font infofont
  button $froutput.btt -text {Change} -command "set LRP_ProvideOutputNumberToPlot_Result \[PickRadioOption \$LRPModel_m {Provide output number to plot} \$LRP_ProvideOutputNumberToPlot_Result\]" -underline 1
  pack $froutput.ent -side left -padx 10
  pack $froutput.btt -side left
  button $froutput.bttplotalong -text {Plot output} -command { \
               set result [RejectZeroValue $outputspinbox1 $outputspinbox2 $outputspinbox3 $outputspinbox4 $outputspinbox5 $outputspinbox6]; \
ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_PlotPPOutput.sce\");"; \
# ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();";
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"passtopass\", \"output\", $LRP_ProvideOutputNumberToPlot_Result, list(\[$result\]), list(\"all\"), lrpMergedDatToPlots);";
# ExecInScilab "showInfoAboutPlot=%F;";
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";";
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);";
  } -underline 5

  pack $froutput.bttplotalong -side right -padx 5
#   button $froutput.bttplotacross -text {Plot ACROSS the pass}
#   pack $froutput.bttplotacross -side right -padx 5
  pack $main.froutput -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: OUTPUT FRAME

  button $main.bttclose -text {Close} -command "destroy $main" -underline 0
  pack $main.bttclose -padx 5 -pady 5 -side right
  bind $main <Alt-c> "$main.bttclose invoke"
  bind $main <Alt-h> "$frstate.btt invoke"
  bind $main <Alt-s> "$frstate.bttplotalong invoke"
  bind $main <Alt-o> "$froutput.bttplotalong invoke"
  bind $main <Alt-a> "$froutput.btt invoke"
  #setup the frame
  focus -force $main.frame.fr1.spinbox1
}

proc Provide3DPlotOptions {} {
  global LRPModel_Beta
  catch {font delete infofont}
  #and create a new one
  font create infofont -family Helvetica -size 12 -weight bold
  #Set the main form name
  set main .rc.option
  #If the main form exist - destroy it
  catch {destroy $main}
  # ... so we can build a new one
  toplevel $main
  wm title $main {Provide plot options}
  #make the form not resizeable
  wm resizable $main 0 0
  global LRP_ProvideStateNumberToPlot_Result;
  global LRPModel_m;
  global LRPModel_n;
  #The header
  label $main.lbl -text {3d plot options} -font infofont
  pack $main.lbl
  set fr $main.frame
  labelframe $main.frame -relief groove -borderwidth 2 -text {Which profile to plot}
  set fr $main.frame.fr1
  pack $main.frame
  #STATE FRAME
  labelframe $main.frstate -relief groove -borderwidth 2 -text {State}
  set frstate $main.frstate
  label $frstate.entlabel -text {State to plot:  }
  pack $frstate.entlabel -side left
  label $frstate.ent -textvariable {LRP_ProvideStateNumberToPlot_Result} -font infofont
  button $frstate.btt -text {Change} -command "set LRP_ProvideStateNumberToPlot_Result \[PickRadioOption \$LRPModel_n {Provide state number to plot} \$LRP_ProvideStateNumberToPlot_Result\]" -underline 1
  pack $frstate.ent -side left -padx 10
  pack $frstate.btt -side left
  button $frstate.bttplotalong -text {Plot state} -command { \
ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_Plot3DState.sce\");"; \
# ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();";
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"3D\", \"state\", $LRP_ProvideStateNumberToPlot_Result, list(\"all\"), list(\"all\"), lrpMergedDatToPlots);";
# ExecInScilab "showInfoAboutPlot=%F;";
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";";
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);";
  }
  #TCL_messageBox -message "STATEXX ALONG:\[$result\]" -type ok; \  # ExecInScilab {temporaryowiec=23;}
  # ExecInScilab "plotHorProfile(X, \[$result\], $LRP_ProvideStateNumberToPlot_Result, 3);"
  pack $frstate.bttplotalong -side right -padx 5
#   button $frstate.bttplotacross -text {Plot ACROSS the pass} -command { \
#                set result [RejectZeroValue $statespinbox1 $statespinbox2 $statespinbox3 $statespinbox4 $statespinbox5 $statespinbox6]; \
#                ExecInScilab "plotVertProfile(X, \[$result\], $LRP_ProvideStateNumberToPlot_Result, 3);"; \
#   }
#   pack $frstate.bttplotacross -side right -padx 5
  #The main part
  pack $frstate -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: STATE FRAME


  #OUTPUT FRAME
  labelframe $main.froutput -relief groove -borderwidth 2 -text {Output}
  set froutput $main.froutput
  label $froutput.entlabel -text {Output to plot:  }
  pack $froutput.entlabel -side left
  label $froutput.ent -textvariable {LRP_ProvideOutputNumberToPlot_Result} -font infofont
  button $froutput.btt -text {Change} -command "set LRP_ProvideOutputNumberToPlot_Result \[PickRadioOption \$LRPModel_m {Provide output number to plot} \$LRP_ProvideOutputNumberToPlot_Result\]" -underline 1
  pack $froutput.ent -side left -padx 10
  pack $froutput.btt -side left
  button $froutput.bttplotalong -text {Plot output} -command { \
ExecInScilab "exec(\"$LRP_PATH/gui/LRP_Gui_Plot3DOutput.sce\");"; \
# ExecInScilab "lrpMergedDatToPlots=createPlotDataStub();";
# ExecInScilab "lrpMergedDatToPlots=lrpAddPlot(lrp,\"\",\"3D\", \"output\", $LRP_ProvideOutputNumberToPlot_Result, list(\"all\"), list(\"all\"), lrpMergedDatToPlots);";
# ExecInScilab "showInfoAboutPlot=%F;";
# ExecInScilab "plotBlackOrColor3DPlot=\"color\";";
# ExecInScilab "lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);";
  }

  pack $froutput.bttplotalong -side right -padx 5
#   button $froutput.bttplotacross -text {Plot ACROSS the pass}
#   pack $froutput.bttplotacross -side right -padx 5
  pack $main.froutput -padx 4 -pady 4 -ipadx 4 -ipady 4 -expand 1 -fill both
  #END: OUTPUT FRAME



  button $main.bttclose -text {Close} -command "destroy $main" -underline 0
  pack $main.bttclose -padx 5 -pady 5 -side right
  bind $main <Alt-c> "$main.bttclose invoke"
  focus -force $frstate.btt
  bind $main <Alt-a> "$frstate.btt invoke"
  #setup the frame
}

proc ChangeProcess {} {
  # Modifies the matrices A, B, B0, C, D, D0, Alpha, Beta.
  global LRP_Help_ChangeProcessResult
  global LRP_Help_CreateResult
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_Alpha LRPModel_Beta;
  # create the fonts
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #make sure, there are no duplicates of this window
  catch {bind .rc.changeprocess <Destroy> {} }
  catch {destroy .rc.changeprocess}
  # setup main window
  toplevel .rc.changeprocess
  bind .rc.changeprocess <Destroy> { \
    set LRP_Help_ChangeProcessResult 10; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title .rc.changeprocess {Change system matrices}
  wm resizable .rc.changeprocess 0 0
  #prepare the main frame
  set main .rc.changeprocess.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Enter the system matrices:}
  pack $main.info
  set frcontrol $main.frcontrol
  #pack two buttons
  frame $frcontrol
  #Pack the picture of LRP
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  #Create some free space
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2
  #Create matrices A, B, B0
  frame $ent.state -relief groove -borderwidth 2
  pack $ent.state -padx 2 -pady 2 -expand 1 -fill both

  #Matrix A
  set frmatrixa $ent.state.frmatrixa
  frame $frmatrixa
  label $frmatrixa.lblmatrixa -text {Matrix A:}

#
#
#
#
#
#
# set matrix to zero
#SetToZero LRPModel_A $LRPModel_n $LRPModel_n;
  entry $frmatrixa.entmatrixa -textvariable {LRPModel_A}
  #-validate all -validatecommand {regsub -all {^[0-9]} $LRPModel_A {} LRPModel_A}
# if {[regexp {[^[:digit:]]} $LRPModel_n] == 0 } {return 1} else {return 0;}
#  set LRPModel_A {[1 2 3]}
  button $frmatrixa.bttmatrixa -text {...} -command {RunCreate {matrix A} $LRPModel_A $LRPModel_n $LRPModel_n;   set LRPModel_A $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixa.bttrandmatrixa -text {rand} -command {RandomMatrix {A}}
  bind $frmatrixa.entmatrixa <Control-Shift-space> "$frmatrixa.bttmatrixa invoke"
  pack $frmatrixa -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixa.lblmatrixa -side left
  pack $frmatrixa.bttmatrixa -side right
  pack $frmatrixa.bttrandmatrixa -side right
  pack $frmatrixa.entmatrixa -side right
  #matrix B
  set frmatrixb $ent.state.frmatrixb
  frame $frmatrixb
  label $frmatrixb.lblmatrixb -text {Matrix B:}
  # set matrix to zero
# SetToZero LRPModel_B $LRPModel_n $LRPModel_r;
  entry $frmatrixb.entmatrixb -textvariable {LRPModel_B}
  button $frmatrixb.bttmatrixb -text {...} -command {RunCreate {matrix B} $LRPModel_B $LRPModel_n $LRPModel_r; set LRPModel_B $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixb.bttrandmatrixb -text {rand} -command {RandomMatrix {B}}
  bind $frmatrixb.entmatrixb <Control-Shift-space> "$frmatrixb.bttmatrixb invoke"
  pack $frmatrixb -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixb.lblmatrixb -side left
  pack $frmatrixb.bttmatrixb -side right
  pack $frmatrixb.bttrandmatrixb -side right
  pack $frmatrixb.entmatrixb -side right
  #matrix B0
  set frmatrixb0 $ent.state.frmatrixb0
  frame $frmatrixb0
  label $frmatrixb0.lblmatrixb0 -text {Matrix B0:}
  # set matrix to zero
#  SetToZero LRPModel_B0 $LRPModel_n $LRPModel_m;
  entry $frmatrixb0.entmatrixb0 -textvariable {LRPModel_B0}
  button $frmatrixb0.bttmatrixb0 -text {...} -command {RunCreate {matrix B0} $LRPModel_B0 $LRPModel_n $LRPModel_m; set LRPModel_B0 $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixb0.bttrandmatrixb0 -text {rand} -command {RandomMatrix {B0}}
  bind $frmatrixb0.entmatrixb0 <Control-Shift-space> "$frmatrixb0.bttmatrixb0 invoke"
  pack $frmatrixb0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixb0.lblmatrixb0 -side left
  pack $frmatrixb0.bttmatrixb0 -side right
  pack $frmatrixb0.bttrandmatrixb0 -side right
  pack $frmatrixb0.entmatrixb0 -side right


  #Create matrices C, D, D0
  frame $ent.output -relief groove -borderwidth 2
  pack $ent.output -padx 2 -pady 2 -expand 1 -fill both
  set frmatrixc $ent.output.frmatrixc
  frame $frmatrixc
  label $frmatrixc.lblmatrixc -text {Matrix C:}
  # set matrix to zero
#  SetToZero LRPModel_C $LRPModel_m $LRPModel_n;
  entry $frmatrixc.entmatrixc -textvariable {LRPModel_C}
  #-validate all -validatecommand {regsub -all {^[0-9]} $LRPModel_A {} LRPModel_A}
#  set LRPModel_A {[1 2 3]}
  button $frmatrixc.bttmatrixc -text {...} -command {RunCreate {matrix C} $LRPModel_C $LRPModel_m $LRPModel_n;   set LRPModel_C $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixc.bttrandmatrixc -text {rand} -command {RandomMatrix {C}}
  bind $frmatrixc.entmatrixc <Control-Shift-space> "$frmatrixc.bttmatrixc invoke"
  pack $frmatrixc -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixc.lblmatrixc -side left
  pack $frmatrixc.bttmatrixc -side right
  pack $frmatrixc.bttrandmatrixc -side right
  pack $frmatrixc.entmatrixc -side right
  #matrix D
  set frmatrixd $ent.output.frmatrixd
  frame $frmatrixd
  label $frmatrixd.lblmatrixd -text {Matrix D:}
  # set matrix to zero
#  SetToZero LRPModel_D $LRPModel_m $LRPModel_r;
  entry $frmatrixd.entmatrixd -textvariable {LRPModel_D}
  button $frmatrixd.bttmatrixd -text {...} -command {RunCreate {matrix D} $LRPModel_D $LRPModel_m $LRPModel_r; set LRPModel_D $LRP_Help_SetterResult; } -takefocus 0
  button $frmatrixd.bttrandmatrixd -text {rand} -command {RandomMatrix {D}}
  bind $frmatrixd.entmatrixd <Control-Shift-space> "$frmatrixd.bttmatrixd invoke"
  pack $frmatrixd -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixd.lblmatrixd -side left
  pack $frmatrixd.bttmatrixd -side right
  pack $frmatrixd.bttrandmatrixd -side right
  pack $frmatrixd.entmatrixd -side right
  #matrix D0
  set frmatrixd0 $ent.output.frmatrixd0
  frame $frmatrixd0
  label $frmatrixd0.lblmatrixd0 -text {Matrix D0:}
  # set matrix to zero
#  SetToZero LRPModel_D0 $LRPModel_m $LRPModel_m;
  entry $frmatrixd0.entmatrixd0 -textvariable {LRPModel_D0}
  button $frmatrixd0.bttmatrixd0 -text {...} -command {RunCreate {matrix D0} $LRPModel_D0 $LRPModel_m $LRPModel_m; set LRPModel_D0 $LRP_Help_SetterResult;} -takefocus 0
  button $frmatrixd0.bttrandmatrixd0 -text {rand} -command {RandomMatrix {D0}}
  bind $frmatrixd0.entmatrixd0 <Control-Shift-space> "$frmatrixd0.bttmatrixd0 invoke"
  pack $frmatrixd0 -expand 1 -fill both -pady 2 -padx 2
  pack $frmatrixd0.lblmatrixd0 -side left
  pack $frmatrixd0.bttmatrixd0 -side right
  pack $frmatrixd0.bttrandmatrixd0 -side right
  pack $frmatrixd0.entmatrixd0 -side right


  # set focus and select the whole content of 1st entry
  focus $frmatrixa.entmatrixa
  $frmatrixa.entmatrixa selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.changeprocess; set LRP_Help_ChangeProcessResult -1;} -underline 0;
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.changeprocess; ProvideParameters;} -underline 0;
  #button $frcontrol.btnext -text {Next} -command {destroy .rc.changeprocess; set LRP_Help_ChangeProcessResult 1; RunMatrixCreator; UpdateMatrices; destroy .rc.changeprocess;} -underline 0;
  button $frcontrol.btrandall -text {Randomize all} -command {RandomMatrix {A}; RandomMatrix {B}; RandomMatrix {B0}; RandomMatrix {C}; RandomMatrix {D}; RandomMatrix {D0};} -underline 0
  button $frcontrol.btnext -text {Ok} -command {destroy .rc.changeprocess; UpdateMatrices;} -underline 0;

  #bind keyboard
#  bind .rc.changeprocess <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.changeprocess <Alt-o> "$frcontrol.btnext invoke"
  bind .rc.changeprocess <Alt-r> "$frcontrol.btrandall invoke"
#  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext -side right -padx 2 -pady 2
  pack $frcontrol.btrandall -side right -padx 2 -pady 2
#  grab set .rc.changeprocess
  
}

proc ChangeSimulationParameter {} {
  # Changes the parameters of LRPModel_n LRPModel_m LRPModel_r.
  #RETURN VALUES:
  # 1 - user clicked "Next"
  # -1 - user clicked "Previous"
  # 10 - user closed window. WARNING: NO RETURN VALUE. CHECK THE LRP_Help_ProvideParametersResult FOR RESULT!!!
  #Make sure, we are not running the Plots...
  catch { destroy .rc.plotoption;};
  global LRP_Help_ProvideSimulationParametersResult;
  global LRPModel_Alpha LRPModel_Beta;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global TMP_DIR;
  # create the fonts
  catch {font delete infofont}
  font create infofont -family Helvetica -size 12 -weight bold

  #unbind the handler
  catch {bind .rc.provideSimulationParameters <Destroy> {} }
  #make sure, there are no duplicates of this window
  catch {destroy .rc.provideSimulationParameters}

  # setup main window
  toplevel .rc.provideSimulationParameters
  #do not allow for closing
  wm protocol .rc.provideSimulationParameters WM_DELETE_WINDOW {#}
  #and bind the handler again
  bind .rc.provideSimulationParameters <Destroy> { \
    if [regexp {(^[0-9]{0,}[1-9][0-9]{0,}$)} $LRPModel_Alpha]==0 {
       tk_messageBox -message "Wrong pass length. Assumed alpha=10!" -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
       set LRPModel_Alpha {10};
    };
    if [regexp {(^[0-9]{0,}[1-9][0-9]{0,}$)} $LRPModel_Beta]==0 {
       tk_messageBox -message "Wrong number of passes. Assumed beta=10!" -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
       set LRPModel_Beta {20};
    };
    catch {bind .rc.provideSimulationParameters <Destroy> {} }; \
    catch {set LRP_Help_ProvideSimulationParametersResult 10 }; \
    ActivateMainWindow; \
  }
  DeactivateMainWindow;
  wm title .rc.provideSimulationParameters {Change system simulation parameters}
  wm resizable .rc.provideSimulationParameters 0 0
  #prepare the main frame
  set main .rc.provideSimulationParameters.mainframe
  frame $main -relief groove -borderwidth 2
  pack $main
  label $main.info -font {infofont} -text {Change the system simulation parameters:}
  pack $main.info
  frame $main.frpicture -relief groove -borderwidth 2
  label $main.frpicture.picture -image {LRP_Image}
  pack $main.frpicture.picture -padx 2 -pady 2
  pack $main.frpicture -padx 2 -pady 2
  frame $main.spacer -height 5
  pack $main.spacer -expand 1 -fill both
  set ent $main.entryframe
  frame $ent -relief groove -borderwidth 2
  pack $ent -fill both -padx 2 -pady 2

  set frcontrol $main.frcontrol
  frame $frcontrol
  button $frcontrol.btnext -text {Ok} -command { \
                                        destroy .rc.provideSimulationParameters; \
                                        CalculateInitialConditions; \
                                        SaveMatrices; \
  ExecInScilab "exec('$TMP_DIR/system.sci',-1);"; \
  ExecInScilab "TCL_EvalStr('ActivateMainWindow');"; \
	tk_messageBox -message {The initial conditions have been reset} -type ok -icon info -title {Parameter change}} -underline 0

## DO BUTTONA btnext...
#                                       ExecInScilab "lrp.dim.alpha=$LRPModel_Alpha";
#                                       ExecInScilab "lrp.dim.beta=$LRPModel_Beta";
#                                       ExecInScilab "lrp.dim.pmin=0";
#	 																			ExecInScilab "lrp.dim.pmax=$LRPModel_Alpha-1";
#                                       ExecInScilab "lrp.dim.kmin=0";
#																				ExecInScilab "lrp.dim.kmax=$LRPModel_Beta";
  #states
  set frstate $ent.frstate
  frame $frstate
  label $frstate.lblstates -text {Pass length (alpha):} -underline 17
  #validate pn
  entry $frstate.entstates -textvariable {LRPModel_Alpha} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Beta]} { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong pass length. It must be greater or equal to 1." -type ok -title {Wrong number of pass length} -icon warning -parent .rc.provideSimulationParameters; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Beta]} { \
             .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
       } else { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all

  pack $frstate -expand 1 -fill both -pady 2 -padx 2
  pack $frstate.lblstates -side left
  pack $frstate.entstates -side right
  #outputs
  set froutput $ent.froutput
  frame $froutput
  label $froutput.lbloutputs -text {Number of passes to simulate (Beta):} -underline 18
  entry $froutput.entoutputs -textvariable {LRPModel_Beta} -validatecommand { \
   if {1==[string compare '%V' 'focusin']} { \
    if {0==[string compare '%V' 'focusout']} { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Alpha]} { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
          return 1; \
       } else { \
         tk_messageBox -message "Wrong number of passes. It must be greater or equal to 1." -type ok -title {Wrong number of passes} -icon warning  -parent .rc.provideSimulationParameters; \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
         return 0; \
       } \
    } else { \
      if {1 == [regexp {^([0-9]{0,}$)} %P] } { \
       if {1 == [regexp {(^\d?\d*[1-9]\d*$)} %P]} { \
          if {1 == [regexp {(^\d?\d*[1-9]\d*$)} $LRPModel_Alpha]} { \
             .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state normal; \
          }; \
       } else { \
          .rc.provideSimulationParameters.mainframe.frcontrol.btnext configure -state disabled; \
       }; \
   return 1; \
      } else { \
   return 0; \
      } \
   }} else {return 1;} \
  } -validate all

  pack $froutput -expand 1 -fill both -pady 2 -padx 2
  pack $froutput.lbloutputs -side left
  pack $froutput.entoutputs -side right

  # set focus and select the whole content of 1st entry
  focus $frstate.entstates
  $frstate.entstates selection range 0 end
  frame $main.frline -height 2 -relief flat -borderwidth 2
  pack $main.frline -padx 2 -pady 2 -expand 1 -fill both
  pack $frcontrol -pady 2 -padx 2 -expand 1 -fill both
  # Next and Prev buttons
#   button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideSimulationParameters; set LRP_Help_ProvideSimulationParametersResult -1;} -underline 0
#   button $frcontrol.btnext -text {Finish} -command {destroy .rc.provideSimulationParameters; set LRP_Help_ProvideSimulationParametersResult 1; } -underline 0
#  button $frcontrol.btprev -text {Previous} -command {destroy .rc.provideSimulationParameters; ProvideMatrices} -underline 0
#  pack $frcontrol.btprev -side left -padx 2 -pady 2
  pack $frcontrol.btnext  -padx 2 -pady 2


  #bind keyboard
#  bind .rc.provideSimulationParameters <Alt-p> "$frcontrol.btprev invoke"
  bind .rc.provideSimulationParameters <Alt-o> "$frcontrol.btnext invoke"
  bind .rc.provideSimulationParameters <Alt-a> "focus $frstate.entstates;"
  bind .rc.provideSimulationParameters <Alt-b> "focus $froutput.entoutputs;"
  grab set .rc.provideSimulationParameters
}

proc SaveAAGSystem {} {
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_X0 LRPModel_Y0;
  global LRPModel_Beta LRPModel_Alpha;
  global LRP_PATH
  # display the save requester
  ExecInScilab "exec('$LRP_PATH/gui/SetValues.sci');"
  set types {
      {"Latex files" {.tex}}
      {"All files"    {*}  }
  }
  ScilabEval "exec('generatePdfModels.sci');" "sync" "seq"
#  set file_name [tk_getSaveFile -filetypes $types -parent .rc \
#      -initialfile "system.tex" -defaultextension ".tex" -title {Save Latex}]
#
#============================================================
set MatrixXA [string trim "\\begin{displaymath}
A=
[subst -nocommand [MatrixToLatex "$LRPModel_A" "$LRPModel_n"  "$LRPModel_n"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixXB [string trim "\\begin{displaymath}
B=
[subst -nocommand [MatrixToLatex "$LRPModel_B" "$LRPModel_n"  "$LRPModel_r"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixXB0 [string trim "\\begin{displaymath}
B_0=
[subst -nocommand [MatrixToLatex "$LRPModel_B0" "$LRPModel_n"  "$LRPModel_m"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixXC [string trim "\\begin{displaymath}
C=
[subst -nocommand [MatrixToLatex "$LRPModel_C" "$LRPModel_m"  "$LRPModel_n"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixXD [string trim "\\begin{displaymath}
D=
[subst -nocommand [MatrixToLatex "$LRPModel_D" "$LRPModel_m"  "$LRPModel_r"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixXD0 [string trim "\\begin{displaymath}
D_0=
[subst -nocommand [MatrixToLatex "$LRPModel_D0" "$LRPModel_m"  "$LRPModel_m"]]
\\end{displaymath}
"
]
#============================================================
#============================================================
set MatrixX0 [string trim "\\begin{displaymath}
X_0=
[subst -nocommand [MatrixToLatex "$LRPModel_X0" "$LRPModel_n"  "[expr \"$LRPModel_Beta\"]"]]
\\end{displaymath}
"
]
#============================================================
set MatrixY0 [string trim "\\begin{displaymath}
Y_0=
[subst -nocommand [MatrixToLatex "$LRPModel_Y0" "$LRPModel_m"  "$LRPModel_Alpha"]]
\\end{displaymath}
"
]
#============================================================

#puts $MatrixXA
set isStable {1}
#system is stable
set isUnstable {1}
#system is unstable
  set file_name {template.tex}
    if {[string compare $file_name ""] != 0} {
   #catch {file delete $file_name; }
   set myfile [open $file_name r]
   set st {}
   while {[gets $myfile str] >= 0} {
    #puts "$str"
    append st $str "\n"
  }
#  puts "$str"
  close $myfile
    if {$isStable==0} {
      #isStable = FALSE
      regsub -all {\\IFSTABLE.*\\END_IFSTABLE\n} $st {} st
      regsub -all {\\IFSTABLE.*\\END_IFSTABLE} $st {} st
    } else {
      #isStable = TRUE
      regsub -all {\\IFSTABLE\n} $st {} st
      regsub -all {\\IFSTABLE} $st {} st
      regsub -all {\\END_IFSTABLE\n} $st {} st
      regsub -all {\\END_IFSTABLE} $st {} st
    }
    if {$isUnstable==0} {
      #isUnstable = FALSE
      regsub -all {\\IFUNSTABLE.*\\END_IFUNSTABLE\n} $st {} st
      regsub -all {\\IFUNSTABLE.*\\END_IFUNSTABLE} $st {} st
    } else {
      #isUnstable = TRUE
      regsub -all {\\IFUNSTABLE\n} $st {} st
      regsub -all {\\IFUNSTABLE} $st {} st
      regsub -all {\\END_IFUNSTABLE\n} $st {} st
      regsub -all {\\END_IFUNSTABLE} $st {} st
    }
    #regsub -all {\\MATRIX_A\n} $st $MatrixA st
    regsub -all {\\MATRIX_A} $st "$MatrixXA" st
    regsub -all {\\MATRIX_D0} $st "$MatrixXD0" st
    regsub -all {\\MATRIX_B0} $st "$MatrixXB0" st
    regsub -all {\\MATRIX_B} $st "$MatrixXB" st
    regsub -all {\\MATRIX_C} $st "$MatrixXC" st
    regsub -all {\\MATRIX_D} $st "$MatrixXD" st
    regsub -all {\\MATRIX_X0} $st "$MatrixX0" st
    regsub -all {\\MATRIX_Y0} $st "$MatrixY0" st
    set st [string trim $st]

  set file_name {System}
   #catch {file delete $file_name; }
   set myfile [open $file_name.tex w]
    puts $myfile "$st"
  close $myfile
  ExecInScilab "unix('pdflatex $file_name.tex');"
  #ExecInScilab "unix('pdfread.bat $file_name.pdf');"
  ExecInScilab "winopen('$file_name.pdf');"
  TCL_dialog .foo "Info" "File \"$file_name\" saved!" \
        questhead 0 OK
#Create the model
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "A="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_A" "$LRPModel_n"  "$LRPModel_n"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "B="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_B" "$LRPModel_n"  "$LRPModel_r"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "B_0="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_B0" "$LRPModel_n"  "$LRPModel_m"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "C="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_C" "$LRPModel_m"  "$LRPModel_n"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "D="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_D" "$LRPModel_m"  "$LRPModel_r"]]
#   puts $myfile "\\end\{displaymath\}"
#     puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "D_0="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_D0" "$LRPModel_m"  "$LRPModel_m"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "X_0="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_X0" "$LRPModel_n"  "$LRPModel_Beta"]]
#   puts $myfile "\\end\{displaymath\}"
#
# puts $myfile ""
#    puts $myfile "\\begin\{displaymath\}"
#    puts $myfile "Y_0="
#    #Insert the variables : ALPHA VALUE
#   puts $myfile [subst -nocommand [MatrixToLatex "$LRPModel_Y0" "$LRPModel_m"  "$LRPModel_Alpha"]]
#   puts $myfile "\\end\{displaymath\}"

 }
}


#
#   CREATE THE MAIN WINDOW
#
MainWindow
#
#   END: CREATE THE MAIN WINDOW
#
