// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2004-03-02 16:09:00

//clear
tohome();

// getf("matr_rand.sci");           //generate random model LRP
// getf("genMatrixXY.sci");         //generate 'matrix state X' and 'matrix output Y'
// getf("genGridXY.sci");
// getf("checkSizeGridXY.sci");     //check size matrix X or Y and grid_X or grid_Y
// getf("plotState.sci");           //plot state
// getf("plotOutput.sci");          //plot output
// getf("plotHorProfile.sci");      //plot horizontal profile
// getf("plotVertProfile.sci");     //plot vertical profile
// getf("figToEps.sci");             //create EPS and PDF file
// getf("isDebug.sci");          //debug information has printed


//load library
//******************************************************************************
//global LRP_PATH
//LRP_PATH='D:\uczelnia\blazej\doktorat\dzialanie\projekt scilab\LRP'
//if isempty(fileinfo(LRP_PATH+'/contrib/LRP/library/loadLibrary.sci')) then
//  LRP_PATH=WSCI+"\contrib\LRP";
//endif
//LRP_PATH=WSCI+"\contrib\LRP";
//exec(LRP_PATH+"/library/loadLibrary.sci");
//******************************************************************************


isDebug(1);

// We set up a initial value the generator of Scilab ===========================
rand('seed',getdate("x"));



WORKING_DIRECTORY='C:\Program Files\scilab-3.1.1\contrib\lrp\example';


////////////////////////////////////////////////////////////////////////////////
//            save latex
////////////////////////////////////////////////////////////////////////////////
title='Subject of some intresing topic';
author='Blazej Cichy';
nameOfLatexFile='new_1.tex';
[path,directory,extension]=fileparts(nameOfLatexFile);
////////////////////////////////////////////////////////////////////////////////





//close all open graphics windows
close_windows=winsid();
xdel(close_windows);

//========================================
//nuber of states
n=7;
//nuber of outputs
m=4;
//nuber of inputs
r=5;

//nuber of points on the pass (alpha)
number_of_points=15;
//nuber of passes (beta)
number_of_passes=25;
//boundary for random values in LRP matrices
boundaryValue=[-0.3, 0.3];
//========================================


//make matrices LRP ->  A,B,B0,C,D,D0
[matricesLRP]=genRandLRP(n, m, r, boundaryValue);
//make LRP
lrp=createLRPModel(matricesLRP, number_of_points, number_of_passes);


//generate random model LRP
// lrp=matr_rand(n,m,r,my_alpha,my_beta);

//bonduary condition
// lrp.x0=zeros(n,lrp.beta);  //for example
//initial pass
// lrp.Y0=ones(m,lrp.alpha);    //for example

//matrix inputs
U=zeros(lrp.alpha,lrp.beta+1,r); //for example

// // //calculate grid X Y
// [gridXY]=genGridXY(lrp.alpha,lrp.beta);

// // //calculate values of LRP process
// //[graphValueSO]=genMatrixXY(lrp,U);
// [graphValueSO]=genGraphValueSO(lrp,U);

// //create tlitst 'gso' - (g)rid and (s)tate and (o)utput graphics values
//[gso]=tlist(['grid_and_SO';'grid';'state';'output'], gridXY, graphValueSO(:));



[gso]=createGSO(lrp, U);



//------------------------------------------------------------------------------
// prepare empty structure for names of picture
//------------------------------------------------------------------------------
nameFigure=tlist(['name_figure'; 'state'; 'output'],...
     tlist(['name_state';  'state';  'vertical'; 'horizontal'], ...
        list(), list(), list()), ...
     tlist(['name_output'; 'output'; 'vertical'; 'horizontal'], ...
        list(), list(), list()) ...
);
//------------------------------------------------------------------------------
// prepare empty structure for which number have to be plot
//------------------------------------------------------------------------------
whichFigurePlot=tlist(['figure_nr'; 'state'; 'output'],...
     tlist(['state_nr';  'state';  'vertical'; 'horizontal'], ...
        [], list(), list()), ...
     tlist(['output_nr'; 'output'; 'vertical'; 'horizontal'], ...
        [], list(), list()) ...
);
//------------------------------------------------------------------------------
// I fill this above structure
//------------------------------------------------------------------------------
//state
whichFigurePlot.state.state=[3 5 3 5 2 1 2]; //here no unique states, some are repeated 3, 2 and 5

//state vertical
//this is for whichFigurePlot.state.state(1) == 3
whichFigurePlot.state.vertical(1)=[3 8 10];
//this is for whichFigurePlot.state.state(2) == 5
whichFigurePlot.state.vertical(2)=[12 3 7 11 9 3 7]; //here no unique vertical states, some are repeated 3 and 7
//this is for whichFigurePlot.state.state(3) == 2
whichFigurePlot.state.vertical(3)=[1 2 3 4 5 6 7 8 9 10];

//state horizontal
whichFigurePlot.state.horizontal(1)=[3 8 10];
whichFigurePlot.state.horizontal(2)=[3 8 10];



//output
whichFigurePlot.output.output=[2 1 3];
//output vertical
whichFigurePlot.output.vertical(1)=[1 2 3 4 ];
whichFigurePlot.output.vertical(2)=[5 6 7 8 ];
//output horizontal
whichFigurePlot.output.horizontal(1)=[3 8 2 12];
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
//check for unique values in 'whichFigurePlot' -- because some elements, by
//mistake can repated, then it will be remove
//------------------------------------------------------------------------------
//[whichFigurePlot]=checkWhichFigurePlot(whichFigurePlot);


//------------------------------------------------------------------------------
//create directory for latex files and pictures
//------------------------------------------------------------------------------
createDirectoryForLaTeX(directory, WORKING_DIRECTORY);




//nuber graphics widow
nrGW=0;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//plot the i-th state
//printf("Ploting states\n");
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // for i=1:length(whichFigurePlot.state.state)
// // // //     fvi=whichFigurePlot.state.state(i);
// // // //     if (fvi > n) | (fvi <= 0)
// // // //        error('No exist that state: '+ sci2exp(fvi, 0));
// // // //     end
// // // //     plotState(grid_X, grid_Y, X, fvi, nrGW, n, lrp);
// // // //     nameFigure.state.state(i)=getNamePicture(nameOfLatexFile,'state',fvi);
// // // //     nrGW=figToEps(nrGW,nameFigure.state.state(i));
// // // // end
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//plot the i-th output
//printf("\nPloting outputs\n");
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // for i=1:length(whichFigurePlot.output.output)
// // // //     fvi=whichFigurePlot.output.output(i);
// // // //     if (fvi > m) | (fvi < 0)
// // // //        error('No exist that output: '+ sci2exp(fvi,0));
// // // //     end
// // // //     plotOutput(grid_X, grid_Y, Y, fvi, nrGW, m, lrp);
// // // //     nameFigure.output.output(i)=getNamePicture(nameOfLatexFile,'output',fvi);
// // // //     nrGW=figToEps(nrGW,nameFigure.output.output(i));
// // // // end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////[nrGW]=plotSeriesSO(plotOutput, whichFigurePlot, nameOfLatexFile, nrGW, 'output', gridXY, Y, lrp);
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//--------------------------------------------
//plot horizontal profile (along the pass)
//---------------------------------------------
//printf("\nPloting horizontal profile (along the pass)\n");
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// soField='state';
// hvField='horizontal';
// [nrGW]=plotSeriesSOVH(plotHorProfile,  whichFigurePlot, nameOfLatexFile, nrGW, soField, hvField, X, lrp);
// hvField='vertical';
// [nrGW]=plotSeriesSOVH(plotVertProfile, whichFigurePlot, nameOfLatexFile, nrGW, soField, hvField, X, lrp);





//state
[nrGW]=plotSeriesSO('state', whichFigurePlot, nameOfLatexFile, nrGW, gso, lrp);
[nrGW]=plotSeriesSOVH('state', 'horizontal', whichFigurePlot, nameOfLatexFile, nrGW,  gso, lrp);
[nrGW]=plotSeriesSOVH('state', 'vertical', whichFigurePlot, nameOfLatexFile, nrGW,  gso, lrp);
//output
[nrGW]=plotSeriesSO('output',whichFigurePlot, nameOfLatexFile, nrGW, gso, lrp);
[nrGW]=plotSeriesSOVH('output', 'horizontal', whichFigurePlot, nameOfLatexFile, nrGW,  gso, lrp);
[nrGW]=plotSeriesSOVH('output', 'vertical', whichFigurePlot, nameOfLatexFile, nrGW,  gso, lrp);



[lrp]=controller1(lrp);


// [lrp]=controller2(lrp);
// [lrp]=controller3(lrp);



// soField='output';
// hvField='horizontal';
// [nrGW]=plotSeriesSOVH(plotHorProfile,  whichFigurePlot, nameOfLatexFile, nrGW, soField, hvField, Y, lrp);
// hvField='vertical';
// [nrGW]=plotSeriesSOVH(plotVertProfile, whichFigurePlot, nameOfLatexFile, nrGW, soField, hvField, Y, lrp);

// // // // ns=length(whichFigurePlot.state.state);
// // // // nsh=length(whichFigurePlot.state.horizontal);

// // // // for wnsh=1:nsh
// // // //     which_state=whichFigurePlot.state.state(wnsh);
// // // //     which_hor_profile=whichFigurePlot.state.horizontal(wnsh);
// // // //     nshr=length(which_hor_profile);

// // // //     //if given profil is empty - no plot
// // // //     if nshr ==0 then
// // // //         continue;
// // // //     end

// // // //     for wnshr=1:nshr
// // // //         fvi=which_hor_profile(wnshr);
// // // //         if (fvi > lrp.beta) | (fvi <= 0)
// // // //             printf('------------------------------------------------------------------------\n');
// // // //             printf('For state %d no horizontal profile %d exists.\n',which_state,fvi);
// // // //             printf('It (%d) should be beetwen 1 to %d.\n',fvi, lrp.beta);
// // // //             error('Check the number horizontal profile!');
// // // //         end
// // // //     end
// // // //     description='State X_' + sci2exp(which_state,0);
// // // //     plotHorProfile(X, which_hor_profile, which_state, nrGW, description);

// // // //     nameFigure.state.horizontal(wnsh)=getNamePicture(nameOfLatexFile,'state_horizontal_' + sci2exp(which_state,0) + '_GW_'+sci2exp(nrGW,0));
// // // //     nrGW=figToEps(nrGW,nameFigure.state.horizontal(wnsh));
// // // //     //only how many as states, no more
// // // //     if wnsh == ns then
// // // //         break;
// // // //     end
// // // // end
// // // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // ns=length(whichFigurePlot.state.state);
// // // // nsv=length(whichFigurePlot.state.vertical);

// // // // for wnsv=1:nsv
// // // //     which_state=whichFigurePlot.state.state(wnsv);
// // // //     which_vert_profile=whichFigurePlot.state.vertical(wnsv);
// // // //     nsvr=length(which_vert_profile);

// // // //     //if given profil is empty - no plot
// // // //     if nsvr ==0 then
// // // //         continue;
// // // //     end

// // // //     for wnsvr=1:nsvr
// // // //         fvi=which_vert_profile(wnsvr);
// // // //         if (fvi > lrp.beta) | (fvi <= 0)
// // // //             printf('------------------------------------------------------------------------\n');
// // // //             printf('For state %d no vertical profile %d exists.\n',which_state,fvi);
// // // //             printf('It (%d) should be beetwen 1 to %d.\n',fvi, lrp.alpha);
// // // //             error('Check the number vertical profile!');
// // // //         end
// // // //     end
// // // //     description='State X_' + sci2exp(which_state,0);
// // // //     plotVertProfile(X, which_vert_profile, which_state, nrGW, description);

// // // //     nameFigure.state.vertical(wnsv)=getNamePicture(nameOfLatexFile,'state_vertical_' + sci2exp(which_state,0) + '_GW_'+sci2exp(nrGW,0));
// // // //     nrGW=figToEps(nrGW,nameFigure.state.vertical(wnsv));
// // // //     //only how many as states, no more
// // // //     if wnsv == ns then
// // // //         break;
// // // //     end
// // // // end
// // // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




//
//
// //nameFiguresStateHorizontal(1)="X_"+sci2exp(i,0)+"_hor_profile_"+sci2exp(which_hor_profile,0);
// name=getNamePicture(nameOfLatexFile,strcat(['state_', sci2exp(which_state,0)]),which_hor_profile);
// figToEps(nrGW,name);

// // //--------------------------------------------
// // //plot vertical profile - across the passes
// // //--------------------------------------------
// // which_state=1;
// // which_vert_profile=[1:15];

// // description=strcat(["State X_" sci2exp(which_state,0) ]);
// // nrGW=nrGW+1;
// // plotVertProfile(X, which_vert_profile, which_state, nrGW, description);
// // //nameFiguresStateVertical(1)="X_"+sci2exp(i,0)+"_vert_profile_"+sci2exp(which_vert_profile,0);
// // name=getNamePicture(nameOfLatexFile,strcat(['state_', sci2exp(which_state,0)]),which_vert_profile);
// // figToEps(nrGW,name);


// // //---------------------------------------------
// // //plot horizontal profile  - along the pass
// // //---------------------------------------------
// // //printf("\nPloting outputs\n=========================\n")
// // which_output=1;
// // which_hor_profile=[1:6];

// // description=strcat(["Otput Y_" sci2exp(which_output,0) ]);
// // nrGW=nrGW+1;
// // plotHorProfile(Y, which_hor_profile, which_state, nrGW, description);
// // //nameFiguresOutputHorizontal(1)="Y_"+sci2exp(i,0)+"_hor_profile_"+sci2exp(which_hor_profile,0);
// // name=getNamePicture(nameOfLatexFile,strcat(['output_', sci2exp(which_output,0)]),which_hor_profile);
// // figToEps(nrGW,name);

// // //-------------------------------------------
// // //plot vertical profile - across the passes
// // //-------------------------------------------
// // nameFiguresOutputVertical=list();
// // which_output=1;
// // which_vert_profile=[1:6];

// // description=strcat(["Otput Y_" sci2exp(which_output,0) ]);
// // nrGW=nrGW+1;
// // plotVertProfile(Y, which_vert_profile, which_state, nrGW, description);
// // //nameFiguresOutputVertical(1)="X_"+sci2exp(i,0)+"_vert_profile_"+sci2exp(which_vert_profile,0);
// // name=getNamePicture(nameOfLatexFile,strcat(['output_', sci2exp(which_output,0)]),which_vert_profile);
// // figToEps(nrGW,name);




// //==============================================================================
// //go out from directory of picture to latex file
// //==============================================================================
// chdir('..');



// [f]=latexHeader(title,author,nameOfLatexFile);

// mfprintf(f,"\\section{Sekcja}\n");
// mfprintf(f,"Some stiupid text.\n");


// mfprintf(f,"\\section{Sekcja}\n");
// mfprintf(f,"Some stiupid text.\n");

// latexFooter(f);



// //==============================================================================
// //go to directory of picture
// //==============================================================================
// chdir(previous_directory);
// clear previous_directory
// //==============================================================================




// // //zapisuje w tej formie by sprawdzic czy sa dobre wykresy z toolboxem Gramackich
// // f=mopen("wyniki.txt","w");
// //  mfprintf(f,"a=%s\n", sci2exp(lrp.alpha,0));
// //  mfprintf(f,"a=%s\n", sci2exp(lrp.beta,0));
// //     mfprintf(f,"a=%s\n", sci2exp(lrp.A,0));
// //     mfprintf(f,"b=%s\n", sci2exp(lrp.B,0));
// //     mfprintf(f,"b0=%s\n", sci2exp(lrp.B0,0));
// //     mfprintf(f,"c=%s\n", sci2exp(lrp.C,0));
// //     mfprintf(f,"d=%s\n", sci2exp(lrp.D,0));
// //     mfprintf(f,"d0=%s\n", sci2exp(lrp.D0,0));
// // mclose(f);



cd(WORKING_DIRECTORY);
