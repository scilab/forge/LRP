// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-04-03 16:09:00


//cleaning, setting, closing
clear
clc
//******************************************************************************
//it must be  after the command clear !!!
global LRP_OPTIONS
//load library
printf("Loading library to Scilab envirnoment.\n");
exec(LRP_OPTIONS.path+"/library/loadLibrary.sci");
//******************************************************************************

// We set up a initial value the generator of Scilab
rand("seed",getdate("x"));

//close all open graphics windows
//close_windows=winsid();
xdel(winsid());




//******************************************************************************
//working directory
//******************************************************************************
//pathExample="example";
WORKING_DIRECTORY=LRP_OPTIONS.path;

//chnge current directory to: WORKING_DIRECTORY
printf("Changing current directory to WORKING_DIRECTORY:\n\t%s\n", WORKING_DIRECTORY);
chdir(WORKING_DIRECTORY);
//******************************************************************************


//******************************************************************************
// LaTeX
//******************************************************************************
title="Subject of some intresing topic";
author="Blazej Cichy";
// nameOfLatexFile="new_2.tex";

//create directory for latex article
// printf("Creating directory for LaTeX article:\n");
// [dirOfLaTeXArt,nameOfLatexFileFull]=createDirectoryForLaTeX(WORKING_DIRECTORY, nameOfLatexFile);
// printf("\t%s\n",dirOfLaTeXArt);
//******************************************************************************





//******************************************************************************
// Parameters of our model
//******************************************************************************
printf("Setting parameters for the process:\n");
//nuber of states
n=2;
//nuber of inputs
r=1;
//nuber of outputs
m=2;

//nuber of passes (beta)
numberOfPasses=50;
//nuber of points on the pass (alpha)
numberOfPoints=25;
//boundary for random values in LRP matrices
rangeForValues=[-0.8, 0.8];

printf("\tnumber of states n=%d\n",n);
printf("\tnumber of inputs r=%d\n",r);
printf("\tnumber of outputs m= %d\n",m);
printf("\tnumber of points alpha=%d\n", numberOfPoints);
printf("\tnumber of passes beta=%d\n", numberOfPasses);
printf("Creating model matrices: A, B, B0, C, D, D0\n\twith randomly generated values (with normal distribution)\n\tfrom %g to %g\n",rangeForValues(:)');
//========================================


//make matrices LRP ->  A,B,B0,C,D,D0
printf("\tcreating structure for LRP model as ""lrp"" variable...\n");
//[lrp]=genRandLRP(n, m, r, boundaryValue);
[lrp]=genRandLRP(n, m, r, numberOfPasses, numberOfPoints, rangeForValues);

requireMatrixSize("lrp.ini.x0",lrp.ini.x0, [lrp.dim.n, lrp.dim.beta+1]);
requireMatrixSize("lrp.ini.y0",lrp.ini.y0, [lrp.dim.m, lrp.dim.alpha]);

printf("\tFinished creating lrp.\n");




// Caution: assumes that lrp.r is correct number of inputs
printf("Creating input signal ""U"" as a zero matrix \n\t(i.e. ""beta+1"" vectors, ""alpha"" points each -""r"" dimensional matrix).\n");
//for example
//[U]=zeros(lrp.dim.alpha, lrp.dim.kmax-lrp.dim.kmin+1, lrp.dim.r);
lrp=setGenerator(lrp,"u","uZero");


lrpPlotDataResult=createPlotDataStub();

// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "3DPlot", "state", ...
//    [1 2 1], ...
//    list("all", [1 2 3 8 9 10 11 12 13 14],[1 2 10 13]), ...
//    list([ 1  3], "all", [0 11 ]), ...
//    lrpPlotDataResult ...
// );

lrpPlotDataResult=lrpAddPlot(...
   lrp, ...
   "3DPlot", "state", ...
   [1], ...
   list([5]), ...
   list([ 7 ]), ...
   lrpPlotDataResult ...
);


// 
// s=round( rand(1, round(rand(1,1) * 7) * (n-1))+1)
// passesList=list();
// pointsRangeList=list();
// for i=1: length(s)
//    passesList(i)=round( rand(1, round(rand(1,1) * (numberOfPasses-1)+1)) * (numberOfPasses-1));
//    pointsRangeList(i)=gsort(round( rand(1, 2) * (numberOfPoints-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "alongthepass", "state", ...
//    s, ...
//    passesList, ...
//    pointsRangeList, ...
//    lrpPlotDataResult ...
// );
// 
// 
// o=round( rand(1, round(rand(1,1) * 7) * (m-1))+1)
// passesList=list();
// pointsRangeList=list();
// for i=1: length(o)
//    passesList(i)=round( rand(1, round(rand(1,1) * (numberOfPasses-1)+1)) * (numberOfPasses-1));
//    pointsRangeList(i)=gsort(round( rand(1, 2) * (numberOfPoints-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "alongthepass", "output", ...
//    o, ...
//    passesList, ...
//    pointsRangeList, ...
//    lrpPlotDataResult ...
// );
// 
// 
// s=round( rand(1, round(rand(1,1) * 7) * (n-1))+1)
// pointsList=list();
// passesRangeList=list();
// for i=1: length(s)
//    pointsList(i)=round( rand(1, round(rand(1,1) * (numberOfPoints-1)+1)) * (numberOfPoints-1));
//    passesRangeList(i)=gsort(round( rand(1, 2) * (numberOfPasses-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "passtopass", "state", ...
//    s, ...
//    pointsList, ...
//    passesRangeList, ...
//    lrpPlotDataResult ...
// );
// 
// 
// o=round( rand(1, round(rand(1,1) * 7) * (m-1))+1)
// pointsList=list();
// passesRangeList=list();
// for i=1: length(o)
//    pointsList(i)=round( rand(1, round(rand(1,1) * (numberOfPoints-1)+1)) * (numberOfPoints-1));
//    passesRangeList(i)=gsort(round( rand(1, 2) * (numberOfPasses-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "passtopass", "output", ...
//    o, ...
//    pointsList, ...
//    passesRangeList, ...
//    lrpPlotDataResult ...
// );
// 
// 
// 
// 
// 
// 
// 
// 
// 
// s=round( rand(1, round(rand(1,1) * 7) * (n-1))+1)
// pointsRangeList=list();
// passesRangeList=list();
// for i=1: length(s)
//    pointsRangeList(i)=gsort(round( rand(1, 2) * (numberOfPoints-1)),"g", "i");
//    passesRangeList(i)=gsort(round( rand(1, 2) * (numberOfPasses-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "3D", "state", ...
//    s, ...
//    pointsRangeList, ...
//    passesRangeList, ...
//    lrpPlotDataResult ...
// );
// 
// 
// o=round( rand(1, round(rand(1,1) * 7) * (m-1))+1)
// pointsRangeList=list();
// passesRangeList=list();
// for i=1: length(o)
//    pointsRangeList(i)=gsort(round( rand(1, 2) * (numberOfPoints-1)),"g", "i");
//    passesRangeList(i)=gsort(round( rand(1, 2) * (numberOfPasses-1)),"g", "i");
// end
// 
// lrpPlotDataResult=lrpAddPlot(...
//    lrp, ...
//    "3D", "output", ...
//    o, ...
//    pointsRangeList, ...
//    passesRangeList, ...
//    lrpPlotDataResult ...
// );









showInfoAboutPlot=%T;
plotBlackOrColor3DPlot="color";
//plotLRP(lrp, lrpPlotDataResult, showInfoAboutPlot);
lrpPlot(lrp, lrpPlotDataResult, plotBlackOrColor3DPlot, showInfoAboutPlot);
