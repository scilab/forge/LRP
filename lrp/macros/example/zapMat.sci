//print matrices LRP and LRP_1D in scilab form
[fd]=mopen("macierze.txt","w") 
for i=2:size(matricesLRP)
	mfprintf(fd,"%s=...\n",matricesLRP(1)(i))
	pm(fd,matricesLRP(i),2);
end

for i=2:size(matricesLRP_1D)
	mfprintf(fd,"%s=...\n",matricesLRP_1D(1)(i))
	pm(fd,matricesLRP_1D(i),4);
end
mclose(fd)