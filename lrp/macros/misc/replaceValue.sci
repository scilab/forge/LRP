function result=replaceValue(variable, lstSearchFor, replaceBy, matchCase)
   if (argn(2)<3) | (argn(2)>4) then
      error("Wrong number of arguments. Expected 3 or 4, got: "+ int2str(argn(2)));
      result="**WRONG ARGUMENTS**";
   end
   if argn(2)==3 then
      matchCase=%F;
   end
   // We select the initial value based on the original one
   requireType("matchCase", matchCase,"boolean");
   requireType("lstSearchFor", lstSearchFor, "list");
   lstSearchFor=getLowerCase(lstSearchFor,matchCase);
   if typeof(variable)=="list" then
      for index=1:length(variable)
          variable(index)=replaceValue(variable(index), lstSearchFor, replaceBy, matchCase);
      end
      result=variable;
      return;
   end;
   variableLowerCase=getLowerCase(variable,matchCase);
   variable=replaceMatrix(variable, variableLowerCase,lstSearchFor, replaceBy, matchCase)
result=variable
return;
   variableLowerCase=getLowerCase(variable,matchCase);
   for index=1:vecSize(lstSearchFor)
      if lcIsScalar(lstSearchFor(index)) then
         variable=replaceAllScalars(variable, variableLowerCase,lstSearchFor(index), replaceBy, matchCase)
         variableLowerCase=getLowerCase(variable,matchCase);
      end
   end
   result=variable;
endfunction
function result=replaceVector(variable, variableLowerCase, lstSearchFor, replaceBy, matchCase)
  for index=1:vecSize(lstSearchFor)
      //Two vectors are equal if they have the same number of rows and columns
      //  and the corresponding elements are identical
      if and(size(variableLowerCase)==size(lstSearchFor(index))) then
        if and(variableLowerCase==lstSearchFor(index)) then
           result=replaceBy;
           return;
        end
        else
            if lcIsScalar(lstSearchFor(index)) then
               variable=replaceAllScalars(variable, variableLowerCase, lstSearchFor(index), replaceBy, matchCase)
            end
      end
  end
  result=variable;
endfunction
function result=getLowerCase(variable,matchCase)
  if matchCase==%t then
     result=variable;
     return;
  end
  if type(variable)==10 then
     len=size(variable,1)*size(variable,2);
  else
      len=length(variable);
  end;
  result=variable;
  for index=1:len
      if type(variable(index))==10 then
         result(index) = convstr(variable(index),"l");
      end
  end
endfunction
function result=augmentVector(vectorA, vectorB)
   result=vectorA;
   for index=1:vecSize(vectorB)
       result($+1)=vectorB(index);
   end;
endfunction
function result=replaceAllScalars(vector, vectorLowerCase, sclSearchFor, replaceBy, matchCase)
  if lcIsScalar(sclSearchFor)==%F then
     error("sclSearchFor must be a scalar" )
  end
  Xvector=[];
  for index=1:vecSize(vector)
      if vectorLowerCase(index)==sclSearchFor then
         //Xvector=augmentVector(Xvector,replaceBy);
         Xvector=glueMatrix(Xvector,replaceBy);
      else
         Xvector($+1)=vector(index);
      end
  end
  if size(vector,2)~=1 then
     result=Xvector';
  else
      result=Xvector;
  end
endfunction
function result=replaceScalar(variable, variableLowerCase, lstSearchFor, replaceBy, matchCase)
  if lcIsScalar(variable)==%F then
     error("Not a scalar");
  end
  for index=1:vecSize(lstSearchFor)
      if variableLowerCase==lstSearchFor(index) then
         result=replaceBy;
         return
      end
  end;
  result=variable;
endfunction
function result=glueMatrix(matrixA, matrixB)
   if and(size(matrixA)==[0,0]) then
      result=matrixB;
      return;
   end
   if and(size(matrixB)==[0,0]) then
      result=matrixA;
      return;
   end
   if size(matrixA,2)==size(matrixB,2) then
      result=[matrixA;matrixB];
   else
       if size(matrixA,1)==size(matrixB,1) then
          result=[matrixA,matrixB];
       else
           printf('MatrixA=');
           disp(matrixA);
           printf('MatrixB=');
           disp(matrixB);
           error('unable to glue');
       end
   end;
endfunction
function result=glueMatrix2(matrixA, matrixB)
   if and(size(matrixA)==[0,0]) then
      result=matrixB;
      return;
   end
   if and(size(matrixB)==[0,0]) then
      result=matrixA;
      return;
   end
   if size(matrixA,1)==size(matrixB,1) then
      result=[matrixA,matrixB];
   else
       if size(matrixA,2)==size(matrixB,2) then
          result=[matrixA;matrixB];
       else
           printf('MatrixA=');
           disp(matrixA);
           printf('MatrixB=');
           disp(matrixB);
           error('unable to glue');
       end
   end;
endfunction
function result=replaceMatrix(variable, variableLowerCase, lstSearchFor, replaceBy, matchCase)
   Xvariable=replaceVector(variable(1,:), variableLowerCase(1,:), lstSearchFor, replaceBy, matchCase);
   for index=2:size(variable,1)
//       Xvariable(index,:)=replaceVector(variable(index,:), variableLowerCase(index,:), lstSearchFor, replaceBy, matchCase);
       Xvariable=glueMatrix(Xvariable,replaceVector(variable(index,:), variableLowerCase(index,:), lstSearchFor, replaceBy, matchCase));
   end
      variableLowerCase =getLowerCase(Xvariable,matchCase);
   Yvariable=replaceVector(Xvariable(:,1), variableLowerCase(:,1), lstSearchFor, replaceBy, matchCase);
   for index=2:size(Xvariable,2)
       //Yvariable(:,index)=replaceVector(Xvariable(:,index), variableLowerCase(:,index), lstSearchFor, replaceBy, matchCase);
       Yvariable=glueMatrix2(Yvariable,replaceVector(Xvariable(:,index), variableLowerCase(:,index), lstSearchFor, replaceBy, matchCase));
   end
   result=Yvariable;
endfunction
function result=vecSize(vector)
   if type(vector)==10 then
      result=max(size(vector));
   else
       result=length(vector);
   end;
endfunction
function result=lcIsScalar(variable)
// Scalar is either:
//   a number (type 1)
//   a string (type 10)
if (type(variable)==1) | (type(variable)==10) then
			if and(size(variable)==[1,1]) then
					result=%T;
			else
					result=%F;
			end
else
			result=%F;
end
endfunction
