// result=isGoodTList(parent, parentName, failOnError);
//    parent - strukturka
//    failOnError - zmienna boolowska o warto�ci domy�lnej TRUE.
//                Je�li failOnError = TRUE, je�li w strukturze parent jest
//                CHO� JEDNO pole = STUB **O Z�YM ROZMIARZE** (a wi�c NIE macierz z
//                jedn� warto�ci� STUB tylko pojedy�czy string) to zwr��
//         error('Wrong parent structure. Field'+nazwaPolaparent+' does not contain useful data');
//         i ustaw warto�� result na FALSE
//                Je�li failOnError=FALSE, sprawd� jak wy�ej, ale zamiast zwraca� error'a, ustaw
//                 result na FALSE.
//
//                 Je�li wszystko OK, to zwr�� TRUE.
//    parentName = nazwa zmiennej parent (jako string)
//
// Przyk�ad:
//  result=isGoodTList(lrp, 'lrp', %T);

// STUB musi byc byc napisane duzymi literami

function [result]=isGoodTList(parent, parentName, failOnError)
nargin=argn(2);

if ((nargin<1) | (nargin > 3)) then
  error('Wrong number of input arguments. Usage: [result]=isGoodparent(parent, failOnError)');
end


//constants
STUB='STUB';
//see: 'help type'
TYPE_LIST_NUMBER=15;
TYPE_TYPED_LIST_NUMBER=16;
TYPE_MATRIX_LIST_NUMBER=17;

//default
result=%T;

select nargin
case 1 then
	failOnError=%T;
	parentName='arg';
case 2 then
	failOnError=%T;
end

typeOfParent=type(parent);
select typeOfParent
	case TYPE_TYPED_LIST_NUMBER then
		// This is a TLIST, so we can safely skip the type names
		startFrom=2;
		numberOfElements=size(parent);
//		disp('Jestem TLIST: '+parentName+'...');
		for index=startFrom:numberOfElements
			if size(parent(1),1)>=index then
				result=result & isGoodTList(parent(index), parentName+'.'+parent(1)(index), failOnError);
				if result==%F then return; end;
			else
				//Stupid Scilab error - number of names in TLIST might NOT correspond to
				// number of values, e.g.
				//      tlist(['NAME'; 'f1';'f2'],1)
				//      tlist(['NAME'; 'f1';'f2'],1,2,3,4,5,,,,) // Gosh, what a call....
				if type(parent(index))==0 then
					printf('\n********************************************************************\n");
					printf('!!!!! WARNING: EMPTY TLIST element: >>"+parentName+'('+int2str(index)+')'+"<< !!!!!\n");
					printf("\n          Check your code for something like: \n\n          x=tlist([''TYPENAME'';''field1'';''field2''],''my value'',); \n\n          NOTE THE ERROR - DELETE THE LAST COMMA AFTER ''my value'' OR \n           GIVE A VALUE AFTER THE COMMA");
					printf('\n********************************************************************\n");
				else
					result=result & isGoodTList(parent(index), parentName+'('+int2str(index)+')', failOnError);
					if result==%F then return; end;
					printf('      Caution: Unnamed TLIST element: >>"+parentName+'('+int2str(index)+')'+'<<\n');
				end
			end
		end
	case TYPE_LIST_NUMBER then
		startFrom=1;
///		disp('Jestem  LIST: '+parentName+'...');
		numberOfElements=size(parent);
		for index=startFrom:numberOfElements
			result=result & isGoodTList(parent(index), parentName+'('+int2str(index)+')', failOnError);
		end
	else
//		printf('    Checking: '+parentName+'\n');
		result=result & IsNotStub(parent,parentName,failOnError);
		if result==%F then return; end;
	end
endfunction

function res=IsNotStub(element, elementName, failOnError)
   if element=='STUB' then
      res=%F;
        if failOnError==%T then
      printf('\n****************************************');
        printf('\n***** STUB value found at: '+elementName+' ******\n');
        printf('  type: \n\n'+elementName+'=some data \n\n');
        printf('  to correct this error\n\n');
        error('STUB value found');
      end
   else
      res=%T;
   end
endfunction

