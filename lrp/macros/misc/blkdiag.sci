function [M]=blkdiag(varargin)
M=[];
for i=1:length(varargin)
   a=varargin(i);
   [r1,c1]=size(M);
   [r2,c2]=size(a);
   M=[ ...
         M,                zeros(r1,c2); ...
         zeros(r2,c1),     a ...
   ];
end
endfunction