function lrpAddPointToPlot(x, y, z, markSize, markStyle, markBackground, nrGraphicWindow)
requireType("x",x,["real"]);
requireType("y",y,["real"]);
requireType("z",z,["real","empty"]);
requireType("markSize",markSize,["integer"]);
  requireRange("markSize",markSize,1,30);
requireType("markBackground",markBackground,["integer"]);
  requireRange("markBackground",markBackground,0,%inf);
requireType("nrGraphicWindow",nrGraphicWindow,["integer"]);
  requireRange("nrGraphicWindow",nrGraphicWindow,0,%inf);

figHandle=scf(nrGraphicWindow)


// 3D plot
if ((~isempty(z)) & (figHandle.children.children(1).type == "Fac3d")) then

    //create vector 2 elements where each element has this same value for example [3 3]
    x=ones(1,2) * x;
    y=ones(1,2) * y;
    z=ones(1,2) * z;

    //create matrix as [gridPoints; gridPoints] etc.
    x=x(ones(1,2),:);
    y=y(ones(1,2),:);
    z=z(ones(1,2),:);

// figHandle.immediate_drawing="off";
// figHandle.visible="off";

    mesh(x, y, z);

// figHandle.immediate_drawing="on";
// figHandle.visible="on";

    figHandle.children.children(1).line_mode="off";
    figHandle.children.children(1).mark_mode="on";
    figHandle.children.children(1).mark_size_unit="point";
    figHandle.children.children(1).mark_size=markSize;
    figHandle.children.children(1).mark_style=markStyle;    //circle
    figHandle.children.children(1).mark_foreground=-1;      //black
    figHandle.children.children(1).mark_background=markBackground;  //background circle

// 2D plot
elseif (figHandle.children.children(1).type == "Compound")  & ...
               (figHandle.children.children(1).children.type == "Polyline") then

// figHandle.immediate_drawing="off";
// figHandle.visible="off";

    plot(x,y);

// figHandle.immediate_drawing="on";
// figHandle.visible="on";

    figHandle.children.children(1).children.line_mode="off";
    figHandle.children.children(1).children.mark_mode="on";
    figHandle.children.children(1).children.mark_size_unit="point";
    figHandle.children.children(1).children.mark_size=markSize;
    figHandle.children.children(1).children.mark_style=markStyle;
    figHandle.children.children(1).children.mark_foreground=-1;  //black
    figHandle.children.children(1).children.mark_background=markBackground;  //background circle

else
    error("Not implemented");
end

endfunction

