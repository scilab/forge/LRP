function display(var)
	DisplayInternal(var,"");
endfunction
function DisplayInternal(var,strIndex)
	s=size(var);
	if length(s)==2 then
		s=s(1).*s(2);
	end
	for idx=1:s
		// We have a custom display function that we want to skip
		if ExistsPrintFunctionName(var(idx)) then
			DisplayInternal(var(idx),strIndex+sprintf("(%d)",idx));
		else
			// A list can also contain sth.nasty, so
			if type(var(idx))==15 then
//				printf("*** A LIST\n");
				DisplayInternal(var(idx),strIndex+sprintf("(%d)",idx));
			else
				// We do not need to display information about 
				//    anything non-indexable (like numbers)
				if type(var)==16 | type(var)==17  
					printf("var%s(%d)=\n",strIndex,idx);
				end
				disp(var(idx));
			end
		end	
	end
endfunction
function res=ExistsPrintFunctionName(var)
// SAFE - works for all possible arguments
	typeName=typeof(var);
	if exists('%'+typeName+'_p') then
		res=%T;
	else
		// A hack for Scilab bug - only the first 8 characters in the type 
		//	 name are compared.
		if length(typeName)<8 then
			res=%F;
		else
			typeName=part(typeName,[1:8]);
			if exists('%'+typeName+'_p') then
				res=%T;
			else
				res=%F;
			end
		end
	end
endfunction
