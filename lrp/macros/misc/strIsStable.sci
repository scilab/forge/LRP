// LRP Toolkit for Scilab. This function returns a quoted string with information
//  about stability. Internally it checks all "stability" elements for field
//  "functionName" equal to the argument of the same name. It then returns 
//  nicely formatted information in the "solutionExists" field of the found stability 
//  result. 
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 20:00:00
function [str]=strIsStable(lrp, whichField, functionName)
	if isThisEntry(lrp(1), 'stability') == %T then
		ind=-1;
		for idx=1:length(lrp.stability)
			if lrp.stability(idx).functionName==functionName then
				ind=idx;
				idx=-2;
				break;
			end
		end
		if ind==-1 then
      ind=-1;
      str="""Not available.""";
      return		
    end
	else
		ind=-1;
  	str="""Not available.""";
		return		
	end
	if (lrp(whichField)(ind).solutionExists==1) then
		str="""System is stable.""";
	else
    if (lrp(whichField)(ind).solutionExists==0) then
		   str="""System is unstable.""";
    else
        str="""Test is inconclusive - probably unstable.""";
    end
	end
endfunction
