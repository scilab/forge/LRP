function [result]=getVectorFromNDMatrix(matrixND)



//------------------- check arguments -------------------------------
[nargout, nargin]=argn();

if nargin > 1
  error("Functin require only 1 input argument.");
end

if nargin == 0
  error("Not enough input arguments.");
end

if nargout > 1 then
	error("Functin require only 1 output argument.");
end

requireType("matrixND",matrixND,"nD matrix");  // remember that n >= 3. Classic 2D matrix will NOT work!

//------------------- main parts -------------------------------

if ndims(matrixND)>2,
  sizeMatrixND = size(matrixND);
  sizeMatrixND(sizeMatrixND==1)=[];     // Remove ones dimensions.
  sizeMatrixND = [sizeMatrixND ones(1,2-length(sizeMatrixND))];  // it must be minimum 2D
  result = matrix(matrixND, sizeMatrixND);
else
  result = matrixND;
end
endfunction
