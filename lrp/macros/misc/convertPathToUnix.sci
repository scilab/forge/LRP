function [path_to_file]=convertPathToUnix(path_to_file)

//create full length of the path to program "BEpsf"
if (MSDOS) then
    path_to_file=pathconvert(path_to_file,%f,%t,"w");
	numbers=ascii(path_to_file);
	ind=find(numbers==ascii("\"));
	numbers(ind)=ascii("/");
	path_to_file=char(numbers);
else
    //create full length of the path to program "BEpsf"
    path_to_file=pathconvert(path_to_file,%f,%t,"u");
end
endfunction