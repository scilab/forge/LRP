// description:
// ============
// The function generates 1D equivalence matrices of LRP


//example
// n=2;
// m=3;
// r=4;
// boundaryValue=[-1,1];
// number_of_points=5;    //number of points on a pass

// // make matrices LRP ->  A,B,B0,C,D,D0
// // and then return them in typed list (tlist) - matricesLRP
//
// [matricesLRP]=genRandLRP(n, m, r, boundaryValue);
//
// [matricesLRP1D]=genMatrLRP1D(matricesLRP,number_of_points);


function [matricesLRP1D]=genMatrLRP1D(matricesLRP,number_of_points)
[lhs,rhs]=argn();
//checking inputs
if (rhs ~= 2) then 
	error('Wrong number of inputs parameters - it must be two.');
end
//checking outputs
if (lhs>1) then
	error('Wrong number of outputs parameter. It must be only one.');
end;
//typed list
if (type(matricesLRP)~=16) then
	error('Wrong type of first argument. It must be typed list.');
end
//typed list of 'matrices_LRP'
if (matricesLRP(1)(1)~='matrices_LRP') then
	error('Wrong typed list. It must be ''matrices_LRP''.');
end
	

// Galko's notation
// alpha = my_alpha
// my_alpha=number_of_points;

// ===================================================================
// Blocks in matrices :
// "dim" means dimension of one block within the whole matrix
// ===================================================================
// PhiDim      =size(D0);
// DeltaDim    =size(D);
// ThetaDim    =size(C);
// GammaDim    =size(B0);
// SigmaDim    =size(B);
// PsiDim      =size(A);

// ===================================================================
// Matrices dimensions :
// "size" means dimension of the whole matrix
// ===================================================================
// PhiSize     =PhiDim*alpha;
// DeltaSize   =DeltaDim*alpha;
// ThetaSize   =[ThetaDim(1)*alpha ThetaDim(2)];
// GammaSize   =GammaDim*alpha;
// SigmaSize   =SigmaDim*alpha;
// PsiSize     =[PsiDim(1)*alpha PsiDim(2)];



// extract matrices from typed list (tlist)
//[type_of_matrices,A,B,B0,C,D,D0]=matricesLRP(:)
// we can also use following write
// matricesLRP.A <==> A
// matricesLRP.B <==> B
// etc.
[A,B,B0,C,D,D0]=getfield(2:7,matricesLRP);

//chcking sizes
checkSizesOfMatrices(A,B,B0,C,D,D0);

//matrices 1D
Phi=genMatrPhi(A, B0, C, D0, number_of_points);
Delta=genMatrDelta(A, B, C, D, number_of_points);
Theta=genMatrTheta(A, C, number_of_points);
Gamma=genMatrGamma(A, B0, number_of_points);
Sigma=genMatrSigma(A, B, number_of_points);
Psi=genMatrPsi(A, number_of_points);

//create typed list (tlist)
matricesLRP1D=tlist(['matrices_LRP_1D';'Phi';'Delta';'Theta';'Gamma';'Sigma';'Psi'],...
   Phi,Delta,Theta,Gamma,Sigma,Psi);
endfunction
