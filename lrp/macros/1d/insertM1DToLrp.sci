// lrp - model LRP
// matrices1D -- typed list of matrices 1D LRP (see: genRandLRP1D.sci)


//=====================
//example
//=====================
// n=2;
// m=3;
// r=4;
// boundaryValue=[-1,1];
// number_of_points=5;
// number_of_passes=10;
// //make matrices LRP ->  A,B,B0,C,D,D0
// [matricesLRP]=genRandLRP(n, m, r, boundaryValue);
// //make LRP
// lrp=createLRPModel(matricesLRP, number_of_points, number_of_passes);
// //make matrices LRP for 1D
// [matricesLRP1D]=genMatrLRP1D(matricesLRP,number_of_points);
//
//// inserting
// lrp=insertM1DToLrp(lrp, matricesLRP1D)

function [lrp]=insertM1DToLrp(lrp, matricesLRP1D)

[lhs,rhs]=argn();

//checking inputs
if (rhs ~= 2) then 
	error('Wrong number of inputs parameters - it must be two.');
end

//checking outputs
if (lhs>1) then
	error('Wrong number of outputs parameter. It must be only one.');
end;


//checking that lrp is typed list (16 mins typedd list - see: help type)
if (type(lrp)~=16) then
	error('Wrong parameter ''lrp''. It must be typed list.');
end
//checking that it is type of 'lrp_model'
if (lrp(1)(1)~='lrp_model') then
	error('Wrong parameter ''lrp''. It must be typed list of ''lrp_model''.');
end

//checking that lrp is typed list (16 mins typedd list - see: help type)
if (type(matricesLRP1D)~=16) then
	error('Wrong parameter ''matricesLRP1D''. It must be typed list.');
end
//checking that lrp is typed list of 'matrices_LRP_1D'
if (matricesLRP1D(1)(1)~='matrices_LRP_1D') then
	error('Wrong parameter ''matricesLRP1D''. It must be typed list of ''matrices_LRP_1D''.');
end
	
//checking that entry 'm1d' is present or not in lrp
if (isThisEntry(lrp,'m1d')) then
    lrp.m1d=matricesLRP1D; 
else //not present
	lrp(1)($+1)='m1d';
	lrp($+1)=matricesLRP1D;
end
endfunction
