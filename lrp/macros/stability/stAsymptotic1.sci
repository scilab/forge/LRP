// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00 by L.Hladowski
// 
// ======================================================================
// Stabillity - asymptotic 1
// ======================================================================
//
// r(D_0) < 1
//
// where:
//    r(D_0) = \max { | \lambda |: \lambda \in \Lambda(D_0) }
//
// ======================================================================
function [lrp]=stAsymptotic1(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

[ind,last]=findLRPIndex(lrp,"stability","stAsymptotic1");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end


eigenValues=spec(lrp.mat.D0);
lambdaMax= max(abs(eigenValues));
info= lambdaMax < 1;

TCL_SetVar("LRP_Help_Status","Ready");
if (~info) then
   //save results to lrp.stability(indStability)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_A1";...
				 "functionName"; ...
         "solutionExists"; ...
         "eigenValues"; "lambdaMax"],...
				 "stAsymptotic1", ...
         0, ... // asymptotically unstable
         eigenValues, lambdaMax);
else
   //save results to lrp.stability(indStability)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_A1";...
				 "functionName"; ...
         "solutionExists"; ...
         "eigenValues"; "lambdaMax"],...
				 "stAsymptotic1", ...
         1, ... //solution exists - true
         eigenValues, lambdaMax);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction
