// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00 by L.Hladowski
// 
// ======================================================================
// Stabillity - practical LMI 1
// ======================================================================
//
// Q=Q' > 0
// D0'*Q*D0 - Q < 0
//
// ======================================================================
function [lrp]=stAsymptoticLMI1(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

[ind,last]=findLRPIndex(lrp,"stability","stAsymptoticLMI1");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end

//initial values of variables
Qi=zeros(lrp.mat.D0);
XListIn=list(Qi);

options=lmiSolverOptions();

errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,stAsymptoticLMI1_eval,options);
TCL_SetVar("LRP_Help_Status","Ready");

if (iserror()) then

   //save results to lrp.controller(indController)
   lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LA1";...
			"functionName"; ...
      "solutionExists"], "stAsymptoticLMI1", 2);    //test inconclusive

   errclear();
else
    [Q]=XListOut(:);

    //save results to lrp.controller(indController)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LA1";...
				 "functionName"; ...
         "solutionExists"; ...
         "Q"],...
         "stAsymptoticLMI1", 1, ... //solution exists - true
         Q);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction


//function for LMI
function [LME,LMI,OBJ]=stAsymptoticLMI1_eval(XListIn)
[Q]=XListIn(:);

OBJ=[];

LME=list(...
    Q-Q'...
);

LMI=list(...
    -[lrp.mat.D0'*Q*lrp.mat.D0 - Q]-eye(),...
    Q-eye()...
);
endfunction
