// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00
// 
// Display function for the asymptotic stability
function []=%lrp_Sys_Cla_S_A1_p(var)
printf('  Asymptotic stability, calculated by the spectral value test\n');
select var.solutionExists
	case 0 then
		printf('System is asymptotically UNSTABLE (as calculated via '+var.functionName+')\n');
		printf('Eigenvalues of D0 matrix:\n');
		disp(var.eigenValues);
		printf('Maximum absolute value of those eigenvalues:%f\n',lambdaMax);
	case 1 then 
		printf('System is stable along the pass (as calculated via '+var.functionName+')\n');
		printf('Eigenvalues of D0 matrix:\n');
		disp(var.eigenValues);
		printf('Maximum absolute value of those eigenvalues:%f\n',var.lambdaMax);
	else
		warning(sprintf('\nvar.solutionExists=%d not appliciable for this stability test\n See function []=%%lrp_Sys_Cla_S_A1_p(var)\n\n',var.solutionExists));
		error('Wrong value')
end
endfunction
