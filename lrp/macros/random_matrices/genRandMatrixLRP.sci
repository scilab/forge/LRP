// n - states
// m - outputs
// r - inputs
// boundaryValue - boundary values within will be generate random values
//                 for example:  boundaryValue=[-1 1];
//                 the random values will be within this bondary from -1 to 1

function [matricesLRP]=genRandMatrixLRP(n, m, r, boundaryValue)

[lhs,rhs]=argn();

//checking inputs
if (rhs ~= 4) then
	error("Wrong number of inputs parameters - it must be four.");
end

//checking outputs
if (lhs>1) then
	error("Wrong number of outputs parameter. It must be only one.");
end;

//initialize random generator
rand("seed",sum(getdate()));

//default number of digits
digits=2;

A=genRandMatrix([n n],boundaryValue,digits)
B=genRandMatrix([n r],boundaryValue,digits)
B0=genRandMatrix([n m],boundaryValue,digits)
C=genRandMatrix([m n],boundaryValue,digits)
D=genRandMatrix([m r],boundaryValue,digits)
D0=genRandMatrix([m m],boundaryValue,digits)

matricesLRP=tlist(["lrp_Sys_Cla_Mat";"A";"B";"B0";"C";"D";"D0"],A,B,B0,C,D,D0);
endfunction
