// Autor Cichy Blazej
// e-mail: cblasius@poczta.onet.pl
// Uniwersytet Zielonogorski 2003
//


function M=genRandMatrix(sizeMatrix,boundaryValue,digits,information)

//number of digits after point
dig=2;
//floating number
info=2;


rhs=argn(2)

select rhs,
 case 4 then
        dig=digits;
        info=information;
 case 3 then
        dig=digits;
end

constVal=10^dig;

M=rand(sizeMatrix(1),sizeMatrix(2),"uniform")*(boundaryValue(2)-boundaryValue(1))+boundaryValue(1);
M=round(M*constVal)/constVal;

if info==1, M=fix(M); end

endfunction