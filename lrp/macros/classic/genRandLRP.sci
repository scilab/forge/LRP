// n - states
// m - outputs
// r - inputs
// rangeForValues - boundary values within will be generate random values
//                 for example:  rangeForValues=[-1 1];
//                 the random values will be within this bondary from -1 to 1
//
// If no arguments are given the following values are assumed:
//	n=2; // 2 states
//	m=3; // 3 outputs
//	r=4; // 4 inputs
//	numberOfPasses=20; // beta =20 passes
//	numberOfPoints=10; // alpha=10 points
//	rangeForValues=[-1 1]; // All matrices from -1 to 1
function [lrp]=genRandLRP(varargin)

[lhs,rhs]=argn();

//checking inputs
if ((rhs ~= 6) & (rhs ~=0)) then
	error("Wrong number of inputs parameters - it must be zero or four.");
end

//checking outputs
if (lhs>1) then
	error("Wrong number of outputs parameter. It must be only one.");
end;

// Set the default values, when user did not give us anything
if (rhs == 0) then
	n=2; // 2 states
	m=3; // 3 outputs
	r=4; // 4 inputs
	numberOfPasses=20; // beta =20 passes
	numberOfPoints=10; // alpha=10 points
	rangeForValues=[-1 1]; // All matrices from -1 to 1
	rhs=6; // Pretend that the user has supplied the arguments. 
else
	n=varargin(1);
	m=varargin(2);
	r=varargin(3);
	numberOfPasses=varargin(4);
	numberOfPoints=varargin(5);
	rangeForValues=varargin(6);
end

requireType("n",n,"positive integer");
requireType("m",m,"positive integer");
requireType("r",r,"positive integer");
requireType("numberOfPasses",numberOfPasses,"positive integer");
requireType("numberOfPoints",numberOfPoints,"positive integer");
requireType("rangeForValues",rangeForValues,"range vector");

//initialize random generator
rand("seed",sum(getdate()));

//default number of digits
digits=2;

A=genRandMatrix([n n],rangeForValues,digits)
B=genRandMatrix([n r],rangeForValues,digits)
B0=genRandMatrix([n m],rangeForValues,digits)
C=genRandMatrix([m n],rangeForValues,digits)
D=genRandMatrix([m r],rangeForValues,digits)
D0=genRandMatrix([m m],rangeForValues,digits)

lrp=createStubLRP();
lrp.mat.A=A;
lrp.mat.B=B;
lrp.mat.B0=B0;
lrp.mat.C=C;
lrp.mat.D=D;
lrp.mat.D0=D0;

lrp.dim.n=n;
lrp.dim.m=m;
lrp.dim.r=r;

lrp.dim.kmin=0;
lrp.dim.pmin=0;

lrp.dim.kmax=numberOfPasses;
lrp.dim.pmax=numberOfPoints-1;

lrp.dim.alpha=numberOfPoints;
//lrp.dim.pmax-lrp.dim.pmin+1;
lrp.dim.beta=numberOfPasses;
//lrp.dim.kmax-lrp.dim.kmin+1;

lrp.ini.x0=zeros(n,lrp.dim.beta+1);
lrp.ini.y0=ones(m,lrp.dim.alpha);

// Note that we cannot use the setNone function,
//   as it requires the values that we set below.
lrp.controller(1).A=lrp.mat.A;
lrp.controller(1).B=lrp.mat.B;
lrp.controller(1).B0=lrp.mat.B0;
lrp.controller(1).C=lrp.mat.C;
lrp.controller(1).D=lrp.mat.D;
lrp.controller(1).D0=lrp.mat.D0;

lrp.indController=1;
//matricesLRP=tlist(["lrp_Sys_Cla_Mat";"A";"B";"B0";"C";"D";"D0"],A,B,B0,C,D,D0);
endfunction
