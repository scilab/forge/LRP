function []=%lrp_Plt_Description_p(var)
// LRP Toolkit for Scilab. This function is used to display the description for the plot
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:29:00

printf('    Title: ''%s''\n',var.title);
printf('  x_label: ''%s''\n',var.x_label);
printf('  y_label: ''%s''\n',var.y_label);
printf('  z_label: ''%s''\n',var.z_label);
printf('  legend : ''%s''\n',var.legend);

endfunction
