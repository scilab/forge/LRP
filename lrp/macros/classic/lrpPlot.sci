// function lrpPlot(lrp, lrpPlotDataResult, plotBlackOrColor3DPlot, showInfoAboutPlot)
// function lrpPlot(lrp, lrpPlotDataResult)
// function lrpPlot(lrp)
//   lrp - the lrp structure
//   lrpPlotDataResult - use lrpAddPlot function to generate this structure
//   plotBlackOrColor3DPlot - either "black" (for black and white )
//                             or "color" (for a color plot), defaults to "color"
//   showInfoAboutPlot - boolean value, %T means that a short summary of the plot
//                        will be printed, defaults to %F
// If lrpPlot is called with one argument, it shows a set of color 3D output plots 
//   containing all the outputs.
function lrpPlot(lrp, lrpPlotDataResult, plotBlackOrColor3DPlot, showInfoAboutPlot)

[nargout, nargin] = argn();
if ((nargin ~=2 ) & (nargin ~= 4) & (nargin ~= 1)) then
  error("Wrong number of inputs argument. It should be 1, 2 or 4.");
end
if (nargin==1) then 
  lrpPlotDataResult=lrpAddPlot(lrp,'system','3D','output',1:lrp.dim.m,list('all'),list('all'));
  lrpPlot(lrp, lrpPlotDataResult);
  // We have already plotted, no more work required so we exit the function
  return;
end

// ==================== Constants ===========================
// Structure types
ALONGTHEPASS_PLOT="lrp_Sys_Cla_P_AP";
PASSTOPASS_PLOT="lrp_Sys_Cla_P_PP";
THREED_PLOT="lrp_Sys_Cla_P_3D";

STATE="state";
OUTPUT="output"
INPUT="input"
MIN_INDEX=1;
MAX_INDEX=2;
// ==================== Arguments check =====================
requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
requireType("lrpPlotDataResult",lrpPlotDataResult,"tlist","lrp_Sys_Cla_Plt");

if nargin == 2 then
   plotBlackOrColor3DPlot="color";
   showInfoAboutPlot=%F;
end
requireType("plotBlackOrColor3DPlot", plotBlackOrColor3DPlot, "string");
requireValue("plotBlackOrColor3DPlot", plotBlackOrColor3DPlot, list("black", "color"));
requireType("showInfoAboutPlot", showInfoAboutPlot, "boolean");

// ==================== Main ================================
global surface3D
//calculate surface 3D
tic
printf("Calculating surface 3D. This may take a while...");
  [surface3D]=lrpCalculateSurface3D(lrp, lrpPlotDataResult);
    //surface3D=createGSO(lrp, U);
printf(" done.\n");
printf("Calculaton time=%g\n",toc());


//for plotNumber=1 : length(lrpPlotDataResult.plotData)
for plotNumber=1 : lstsize(lrpPlotDataResult.plotData)
     select typeof(lrpPlotDataResult.plotData(plotNumber));
       case ALONGTHEPASS_PLOT then
//            //lrpAlongThePassPlot=lrpPlotDataResult.plotData(plotNumber);

    lrpPlotAlongThePass(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber,showInfoAboutPlot);

//            // Get "x" and "y" values
//            [x, y]=lcLrpPlotAlongThePass(lrp, lrpAlongThePassPlot, surface3D);
//            description=lrpAlongThePassPlot.description;
//
//            lrpPlot2D(x, y, description);

       case PASSTOPASS_PLOT then
            lrpPlotPassToPass(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber, showInfoAboutPlot);
       case THREED_PLOT then
            lrpPlot3D(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber, plotBlackOrColor3DPlot, showInfoAboutPlot);
       else
           error("Unknown plot type: "+ typeof(lrpPlotDataResult.plotData(plotNumber)));
      end
end
endfunction


function [x, y]=lcLrpPlotAlongThePass(lrp, lrpAlongThePassPlot, surface3D)
// *** UNSAFE ***

//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;

select lrpAlongThePassPlot.variableName
  case STATE then
      M=surface3D.state;
  case OUTPUT then
      M=surface3D.output;
  case INPUT then
      M=surface3D.input;
  else
     error("Unknown field: "+plotData.variableName+".");
end

// The lrpAlongThePassPlot.pointsRange is [min,max] (for example [1,4]) but
//  we need the continuous range for points i.e. [1,2,3,4] - hence
//  the need for convertion
// This is the real range, as shown on the legend of the plot
pointsRange=lrpAlongThePassPlot.pointsRange(MIN_INDEX) : lrpAlongThePassPlot.pointsRange(MAX_INDEX);

// This will be used for indexing the matrix, so it MUST start from 1. Hence
//   we add the offset
pointsIndicesForMatrix=pointsRange + offsetForPoints;
passesIndicesForMatrix=lrpAlongThePassPlot.passes + offsetForPasses;

values=M( ...
   pointsIndicesForMatrix , ...
   passesIndicesForMatrix , ...
   lrpAlongThePassPlot.variableNumber ...
);

x=pointsRange;
y=values;

endfunction
