//create empty typed list to merge data to plots

function [lrpPlotDataResult]=createPlotDataStub()
//
//type lrpPlotData = record
//     plotData : list of {lrpThreeDimensionalPlot, lrpPassToPassPlot, lrpAlongThePassPlot};
//     maxPoints : integer; //for a polytop, consider a vector of integers...
//     maxPasses : integer;
//end;

[numberOutputsArguments, numberInputsArguments]=argn()

if numberOutputsArguments ~= 1 then
	error("Function requires only one output argument.");
end

if numberInputsArguments ~= 0 then
	error("Function does not require any inputs arguments.");
end

lrpPlotDataResult=tlist(["lrp_Sys_Cla_Plt"; "plotData"; "maxPoints";"maxPasses"], ...
    list(), -%inf, -%inf ...
);

endfunction
