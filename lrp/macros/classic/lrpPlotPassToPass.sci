// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 23:55:00 by L. Hladowski

// function lrpPlot2D(...
//    whatToPlot, ...  //string: what to plot: 'state' or 'output'
//    grid_X, ...          //matrix grid on OX
//    grid_Y, ...          //matrix grid on OY
//    X, ...                  //what  is going to be painted
//    lrpPassToPassPlot.variableNumber, ...  //scalar - which number shuld be plot
//    whichGraphWindow, ...  //scalar: number of grapchic window
//    lrp,... //system lrp
//    rangeOnOX,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
//    rangeOnOY,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// )


// function lrpPlotPassToPass(...
//    lrpPassToPassPlot.variableName, ...  //string: what to plot: 'state' ('x') or 'output' ('y')
//    directionStr, ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
//    M,...                   //what has to be painted:  matrix 3 dimensional
//    lrpPassToPassPlot.variableNumber, ...  //scalar - which number shuld be plot
//    lrpPassToPassPlot.points,...    //which profiles
//    lrpPassToPassPlot.passesRange, ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
//    whichGraphWindow, ...  //scalar: number of grapchic window
//    lrp... //system lrp
// )
// error('Not implemented')


function lrpPlotPassToPass(...
   lrp, lrpPassToPassPlot, surface3D, whichGraphWindow , showInfoAboutPlot...
)


// ==================== Constants ===========================
PASSTOPASS="lrp_Sys_Cla_P_PP";
STATE="state";
OUTPUT="output";
INPUT="input";
MIN_INDEX=1;
MAX_INDEX=2;
// ==================== Requirements ===========================
//check types
requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");
requireType("lrpPassToPassPlot", lrpPassToPassPlot, "tlist", PASSTOPASS);
requireType("lrpPassToPassPlot.variableName", lrpPassToPassPlot.variableName, "string");
// States, inputs and outputs are numbered from 1, hence "positive integer"
requireType("lrpPassToPassPlot.variableNumber", lrpPassToPassPlot.variableNumber, "positive integer");
requireType("lrpPassToPassPlot.points", lrpPassToPassPlot.points, ["integer vector", "integer"]);
requireType("lrpPassToPassPlot.passesRange", lrpPassToPassPlot.passesRange, ["two element integer vector"]);
//check values
lrpPassToPassPlot.variableName=replaceValue(lrpPassToPassPlot.variableName, list("state","x"), STATE);
lrpPassToPassPlot.variableName=replaceValue(lrpPassToPassPlot.variableName, list("output","y"), OUTPUT);
lrpPassToPassPlot.variableName=replaceValue(lrpPassToPassPlot.variableName, list("input","u"), INPUT);
requireValue("lrpPassToPassPlot.variableName", lrpPassToPassPlot.variableName, list(STATE,OUTPUT,INPUT));

select lrpPassToPassPlot.variableName
   case STATE then
	      requireRange("lrpPassToPassPlot.variableNumber", lrpPassToPassPlot.variableNumber, [1, lrp.dim.n]);
   case OUTPUT then
	      requireRange("lrpPassToPassPlot.variableNumber", lrpPassToPassPlot.variableNumber, [1, lrp.dim.m]);
   case INPUT then
	      requireRange("lrpPassToPassPlot.variableNumber", lrpPassToPassPlot.variableNumber, [1, lrp.dim.r]);
   else
        error('Not implemented')
end

requireRange("lrpPassToPassPlot.passes", lrpPassToPassPlot.points, [lrp.dim.pmin, lrp.dim.pmax]);
requireRange("lrpPassToPassPlot.passesRange", lrpPassToPassPlot.passesRange, lrp.dim.kmin, lrp.dim.kmax);

// Ensure we get the row vectors, not column ones; change (and report a warning) if necessary
lrpPassToPassPlot.points=makeRowVector("lrpPassToPassPlot.points",lrpPassToPassPlot.points);
lrpPassToPassPlot.passesRange=makeRowVector("lrpPassToPassPlot.passesRange",lrpPassToPassPlot.passesRange);
// ==================== Arguments check =====================
requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");
requireType("lrpPassToPassPlot", lrpPassToPassPlot, "tlist", PASSTOPASS);

if argn(2)==4 then
	showInfoAboutPlot=%F;
else
	requireType("showInfoAboutPlot", showInfoAboutPlot, "boolean");
end
// ==================== Main ================================

// description=" - pass to pass profile";

//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;



//lrpPassToPassPlot.variableName
select lrpPassToPassPlot.variableName
    case STATE then
// 				stateOrOutputName="State";
//     		dimStateOrOutput=lrp.dim.n;
    		M=surface3D.state;
case OUTPUT  then
//     		stateOrOutputName="Output";
//     		dimStateOrOutput=lrp.dim.m;
    		M=surface3D.output;
case INPUT  then
//     		stateOrOutputName="Output";
//     		dimStateOrOutput=lrp.dim.m;
    		M=surface3D.input;
else
   			error("Unknown field: "+lrpPassToPassPlot.variableName+".");
end

// The lrpPassToPassPlot.passesRange is [min,max] (for example [1,4]) but
//  we need the continuous range for passes i.e. [1,2,3,4] - hence
//  the need for convertion
// This is the real range, as shown on the legend of the plot
passesRange=lrpPassToPassPlot.passesRange(MIN_INDEX) : lrpPassToPassPlot.passesRange(MAX_INDEX);
// This will be used for indexing the matrix, so it MUST start from 1. Hence
//   we add the offset
passesIndicesForMatrix=passesRange + offsetForPasses;
pointsIndicesForMatrix=lrpPassToPassPlot.points + offsetForPoints;

values=M( ...
                pointsIndicesForMatrix , ...
                passesIndicesForMatrix , ...
                lrpPassToPassPlot.variableNumber ...
)';   // Mind the transpose operator (')


//create graphic window
figHandle=scf(whichGraphWindow);
//clear graphic window
//clf(whichGraphWindow);
figHandle.immediate_drawing="off";
figHandle.visible="off";

if length(passesRange) ~=1 then
   plotD2Line(passesRange, lrpPassToPassPlot.points, values, whichGraphWindow, lrpPassToPassPlot.description.legend);
else
   plotD2Circle(passesRange, lrpPassToPassPlot.points, values, whichGraphWindow, lrpPassToPassPlot.description.legend);
end

//show info
if showInfoAboutPlot then
	lrpShowPlotInfo(lrpPassToPassPlot);
end

//nameOfFigure of window
nameOfFigure=strcat(["%d: " lrpPassToPassPlot.description.title " " sci2exp(lrpPassToPassPlot.points,0)]);

//this is because title of figure is bounded by 79 characters
nameOfFigure=shortenString(nameOfFigure, 79-floor(log10(whichGraphWindow)+1)+1); //+1 because (%d) have 2 characters
set(figHandle,"figure_name",nameOfFigure);

xtitle(lrpPassToPassPlot.description.title, lrpPassToPassPlot.description.x_label, ...
                  lrpPassToPassPlot.description.y_label, lrpPassToPassPlot.description.z_label);


figHandle.immediate_drawing="on";
figHandle.visible="on";
endfunction
