function []=%lrp_Sys_Cla_Dim_p(var)
// LRP Toolkit for Scilab. This function is used to display the dimensions of the classic LRP
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-29 20:48

//constant
STUB="STUB";

if var.n<>STUB then
   printf('states  : n=%d',var.n);
else
   printf('states  : n= *** ??? *** ')
end

if var.alpha<>STUB then
   printf('\t\tpass length   : alpha=%d',var.alpha);
else
   printf('\tpass length   : alpha= *** ??? ***')
end
// Alpha is used only to avoid comparing STUB value with a number
if var.alpha<>STUB then
	alpha = var.alpha
else
	alpha = 10 // Stub is quite long, hence to avoid excessive tabulation
						 //   we will use a large "alpha" to fool the code below  
end 
if alpha<9 then
	if var.pmin<>STUB then
	  printf('\t\tp='+sci2exp(var.pmin)+'..');
	else
	  printf('\t\tp=*** ??? *** ...');
	end
else
	if var.pmin<>STUB then
	  printf('\tp='+sci2exp(var.pmin)+'..');
	else
	  printf('\tp=*** ??? *** ...');
	end
end
// Alpha is no longer needed - remove it to avoid future errors
clear alpha

if var.pmax<>STUB then
  printf(sci2exp(var.pmax)+'\n');
else
  printf('*** ??? ***\n');
end


if var.r<>STUB then
   printf(sprintf('inputs  : r=%d',var.r));
else
   printf('outputs : r= *** ??? *** ')
end

if var.beta<>STUB then
   printf('\t\tpasses simul. :  beta=%d',var.beta);
else
   printf('\tpasses simul. :  beta= *** ??? ***')
end


// theBeta is used only to avoid comparing STUB value with a number
//   Note that there already exists a function named beta, hence the name
//   of our variable.
if var.beta<>STUB then
	theBeta = var.beta
else
	theBeta = 10 // Stub is quite long, hence to avoid excessive tabulation
						 //   we will use a large "theBeta" to fool the code below  
end 
if theBeta<10 then
	if var.kmin<>STUB then
	  printf('\t\tk='+sci2exp(var.kmin)+'..');
	else
	  printf('\t\tk=*** ??? *** ...');
	end
else
if var.kmin<>STUB then
	  printf('\tk='+sci2exp(var.kmin)+'..');
	else
	  printf('\tk=*** ??? *** ...');
	end
end

// Beta is no longer needed - remove it to avoid future errors
clear theBeta

if var.kmax<>STUB then
  printf(sci2exp(var.kmax)+'\n');
else
  printf('*** ??? ***\n');
end

if var.m<>STUB then
   printf(sprintf('outputs : m=%d\n',var.m));
else
   printf('inputs  : m= *** ??? *** ')
end
endfunction
