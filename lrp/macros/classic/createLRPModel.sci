// matricesLRP - matrices --> matricesLRP=tlist(A,B,B0,C,D,D0)
// numberOfPoints - number of points on a pass
// numberOfPasses - number of passes


// Function can be called in 4 ways (Format: NumberOfArguments - HowToCall):
//  0 - [lrp]=createLRPModel()
//  3 - [lrp]=createLRPModel([n,r,m], numberOfPoints, numberOfPasses)
//  4 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses)
//  5 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,functionForBoundary)
//  6 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,boundaryX0, boundaryY0)
//  2 - [lrp]=createLRPModel(numberOfPoints, numberOfPasses)
//  3 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses)
//  4 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, functionForBoundary)
//  5 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)

//  8 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses)
//  9 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, functionForBoundary)
//  10 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)

//  3 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses)
//  4 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, functionForBoundary)
//  5 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)

function [lrp]=createLRPModel(varargin)
// **************** The default values ************
DEFAULT_X0=0;
DEFAULT_Y0=1;
DEFAULT_RANGE_FOR_VALUES=[-1 1];
DEFAULT_N=2;
DEFAULT_R=3;
DEFAULT_M=4;
DEFAULT_NUMBER_OF_POINTS=10;
DEFAULT_NUMBER_OF_PASSES=15;
DIGITS=2;
// **************** END: The default values *******

[nargout, nargin]=argn();

if ~or(nargin==[0,2,3,4,5,6,8,9,10])
	error(sprintf('Wrong number of arguments. Required: 0,3,4,5,6,8,9 or 10. Got: %d.',nargin));
	lrp=%nan;
	return;
end
	FUNCTION_IS_USED=%F;
	MATRIX_OR_REAL_IS_USED=%F;                    

if nargin==0
	// No arguments - let's generate a random system
  DIGITS=2;
  rangeForValues=DEFAULT_RANGE_FOR_VALUES;
	n=DEFAULT_N; // states
	r=DEFAULT_R; // inputs
	m=DEFAULT_M; // outputs
	A=genRandMatrix([n n],rangeForValues,DIGITS)
	B=genRandMatrix([n r],rangeForValues,DIGITS)
	B0=genRandMatrix([n m],rangeForValues,DIGITS)
	C=genRandMatrix([m n],rangeForValues,DIGITS)
	D=genRandMatrix([m r],rangeForValues,DIGITS)
	D0=genRandMatrix([m m],rangeForValues,DIGITS)
	numberOfPoints=DEFAULT_NUMBER_OF_POINTS;
	numberOfPasses=DEFAULT_NUMBER_OF_PASSES;
	OPTIONAL_PARAMETERS=0;
  NEXT_PARAMETER=1; // This value points at the next argument. It will be used 
                    //   to give the default values.
	boundaryX0=DEFAULT_X0;
	boundaryY0=DEFAULT_Y0;
	MATRIX_OR_REAL_IS_USED=%T;                    
else
	if (typeof(varargin(1))=="lrp_Sys_Cla_Mat")==%F then
		// We do not have the matricesLRP type. Time to create it
		if and(size(varargin(1))==[1,3]) then			
			//  3 - [lrp]=createLRPModel([n,r,m], numberOfPoints, numberOfPasses)
			//  4 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses)
			//  5 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,functionForBoundary)
			//  6 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,boundaryX0, boundaryY0)
			n=varargin(1)(1);
			r=varargin(1)(2);
			m=varargin(1)(3);			
			if nargin==3 then
				//  3 - [lrp]=createLRPModel([n,r,m], numberOfPoints, numberOfPasses)
				rangeForValues=DEFAULT_RANGE_FOR_VALUES;
				NEXT_PARAMETER=2;							
			else
				//  4 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses)
				//  5 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,functionForBoundary)
				//  6 - [lrp]=createLRPModel([n,r,m], rangeForValues, numberOfPoints, numberOfPasses,boundaryX0, boundaryY0)
				rangeForValues=varargin(2);
				NEXT_PARAMETER=3;							
			end
		else
			if nargin<=5 then
				// We do not have sizes, so we will use the defaults
				//  2 - [lrp]=createLRPModel(numberOfPoints, numberOfPasses)
				//  3 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses)
				//  4 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, functionForBoundary)
				//  5 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
				n=DEFAULT_N;
				r=DEFAULT_R;
				m=DEFAULT_M;				
				if nargin>=3 then
					rangeForValues=varargin(1);
					NEXT_PARAMETER=2;
				else
					rangeForValues=DEFAULT_RANGE_FOR_VALUES;				
					NEXT_PARAMETER=1;
				end
			else
				//  8 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses)
				//  9 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, functionForBoundary)
				//  10 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
				NEXT_PARAMETER=7;
			end				
		end
		if NEXT_PARAMETER<7 then
			requireType("rangeForValues", rangeForValues, "range vector");
			requireType("n", n, "positive integer");
		  requireType("r", r, "positive integer");
	    requireType("m", m, "positive integer");		    
	
			A=genRandMatrix([n n],rangeForValues,DIGITS)
			B=genRandMatrix([n r],rangeForValues,DIGITS)
			B0=genRandMatrix([n m],rangeForValues,DIGITS)
			C=genRandMatrix([m n],rangeForValues,DIGITS)
			D=genRandMatrix([m r],rangeForValues,DIGITS)
			D0=genRandMatrix([m m],rangeForValues,DIGITS)
		else
			A=varargin(1);
			B=varargin(2);
			B0=varargin(3);			
			C=varargin(4);
			D=varargin(5);
			D0=varargin(6);
		end
		requireType("A", A, "2D matrix");
		requireType("B", B, "2D matrix");
		requireType("B0", B0, "2D matrix");
		requireType("C", C, "2D matrix");
		requireType("D", D, "2D matrix");
		requireType("D0", D0, "2D matrix");
				
	else
		//  3 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses)
		//  4 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, functionForBoundary)
		//  5 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)

		requireType("matricesLRP", matricesLRP, "tlist", "lrp_Sys_Cla_Mat");
		requireType("matricesLRP.A", matricesLRP.A, "2D matrix");
		requireType("matricesLRP.B", matricesLRP.B, "2D matrix");
		requireType("matricesLRP.B0", matricesLRP.B0, "2D matrix");
		requireType("matricesLRP.C", matricesLRP.C, "2D matrix");
		requireType("matricesLRP.D", matricesLRP.D, "2D matrix");
		requireType("matricesLRP.D0", matricesLRP.D0, "2D matrix");

		A=varargin(1).A;
		B=varargin(1).B;
		B0=varargin(1).B0;		
		C=varargin(1).C;
		D=varargin(1).D;
		D0=varargin(1).D0;
		NEXT_PARAMETER=2;								
	end

	
	//     At this point we are only concerned with the calls shown below. 
	//    WARNING! USE ONLY THE >>NEXT_PARAMETER<< VARIABLE, 
	//     NO DIRECT CALLS, LIKE (varargin(2)) etc.
	//     USE varargin(NEXT_PARAMETER+1) etc.
	
	//                                                 | NEXT_PARAMETER
	//                                                \/                                             
	//   8 - [lrp]=createLRPModel(???????????, numberOfPoints, numberOfPasses)
	//   9 - [lrp]=createLRPModel(???????????, numberOfPoints, numberOfPasses, functionForBoundary)
	//  10 - [lrp]=createLRPModel(???????????, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
	
	
		numberOfPoints=varargin(NEXT_PARAMETER);
		numberOfPasses=varargin(NEXT_PARAMETER+1);
		NEXT_PARAMETER=NEXT_PARAMETER+2;
	
	// Do we have any more parameters?
	if (nargin>NEXT_PARAMETER) then
		// Yes, check, if we have the values 
		if type(varargin(NEXT_PARAMETER))==1 then
			// Yes, we have the values. How many of them?
			if (nargin==NEXT_PARAMETER+1)
				// Two - OK
				boundaryX0=varargin(NEXT_PARAMETER);
				boundaryY0=varargin(NEXT_PARAMETER+1);
				OPTIONAL_PARAMETERS=2;
				NEXT_PARAMETER=NEXT_PARAMETER+2;
				MATRIX_OR_REAL_IS_USED=%T;                    			
			else
				// Any other value - error. We need both parameters
				error('Both boundaryX0 and boundaryY0 need to be specified. Give values for boundaryY0.');
			end
		else
			// No, we have the function
			FUNCTION_FOR_BOUNDARY=varargin(NEXT_PARAMETER);
		  OPTIONAL_PARAMETERS=1;			
			NEXT_PARAMETER=NEXT_PARAMETER+1;
			FUNCTION_IS_USED=%T;                   		
		end
	else
		// No. We need to provide the default values for X0 and Y0
		boundaryX0=DEFAULT_X0;
		boundaryY0=DEFAULT_Y0;
		OPTIONAL_PARAMETERS=2; // We have set both optional parameters
		MATRIX_OR_REAL_IS_USED=%T						
	end

end

//=================== check part================================

		requireType("numberOfPoints", numberOfPoints, "positive integer");
		requireType("numberOfPasses", numberOfPasses, "positive integer");

	// Deal with the optional arguments
	if OPTIONAL_PARAMETERS==1 then
    requireType("boundaryX0", boundaryX0, ["function"]);
	else
		if OPTIONAL_PARAMETERS==2 then
  	  requireType("boundaryX0", boundaryX0, ["2D matrix", "real"]);
	    requireType("boundaryY0", boundaryY0, ["2D matrix", "real"]);
		end
	end	

//checking outputs
if (nargout > 1) then
    error("Wrong number of output parameters. It must be only one.");
end;


//sizes of matrices
[rA, cA]=size(A);
if (rA~=cA) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""A"" are diffrent. It shold be equal [n by n].\n");
   printf("Current size is: [%d, %d]\n", rA, cA);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""A""");
end

[rB ,cB]=size(B);
if (rA ~= rB) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""B"" are diffrent from ""A"".\n");
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("Size of matrix ""A"" is: [%d, %d]\n",rA, cA);
   printf("Correct size of matrix ""B"" should be: [%d, %d]\n",rA, cB);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""B""");
end

[rB0 ,cB0]=size(B0);
if (rA ~= rB0) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""B0"" are diffrent from ""A"".\n");
   printf("Size of matrix ""B0"" is: [%d, %d]\n",rB0, cB0);
   printf("Size of matrix ""A"" is: [%d, %d]\n",rA, cA);
   printf("Correct size of matrix ""B0"" should be: [%d, %d]\n",rA, cB0);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""B0""");
end

[rC ,cC]=size(C);
if (cA ~= cC) | (cB0 ~= rC) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""C"" are diffrent from:\n\n    ""A"" or ""B0"".\n\n");
   printf("Size of matrix ""C"" is: [%d, %d]\n",rC, cC);
   printf("Size of matrix ""A"" is: [%d, %d]\n",rA, cA);
   printf("Size of matrix ""B0"" is: [%d, %d]\n",rB0, cB0);
   printf("Correct size of matrix ""C"" should be: [%d, %d]\n",cB0, cA);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""C""");
end

[rD ,cD]=size(D);
if (rC ~= rD) | (cB ~= cD) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""D"" are diffrent from:\n\n    ""C"" or ""B"".\n\n");
   printf("Size of matrix ""D"" is: [%d, %d]\n",rD, cD);
   printf("Size of matrix ""C"" is: [%d, %d]\n",rC, cC);
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("Correct size of matrix ""D"" should be: [%d, %d]\n",rC, cB);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""D""");
end

[rD0 ,cD0]=size(D0);
if (rC ~= rD0) | (cB0 ~= cD0) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""D0"" are diffrent from:\n\n    ""C"" or ""B0"".\n\n");
   printf("Size of matrix ""D0"" is: [%d, %d]\n",rD0, cD0);
   printf("Size of matrix ""C"" is: [%d, %d]\n",rC, cC);
   printf("Size of matrix ""B0"" is: [%d, %d]\n",rB0, cB0);
   printf("Correct size of matrix ""D0"" should be: [%d, %d]\n",rC, cB0);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""D0""");
end

//=================== main part ===================================

//======================== size of system =====================
// [rB, cB]=size(B);
// [rD]=size(D0, 1);

n = rB;  //number of states
r = cB;  //number of inputs
m = rD; //number of outputs


//CREATE STUB =======================================
[lrp]=createStubLRP();

//SETTING VALUES =======================================
//================= DIM structure ========================
lrp.dim.n=n;
lrp.dim.r=r;
lrp.dim.m=m;

lrp.dim.alpha=numberOfPoints;
lrp.dim.beta=numberOfPasses,

//range on axis
//points 0 <= p <= alpha - 1
lrp.dim.pmin=0;
lrp.dim.pmax=numberOfPoints-1;   // We are counting from 0, so length(0..numberOfPoints-1)=numberOfPoints

//passes 0 <= k <= beta
lrp.dim.kmin=0;
lrp.dim.kmax=numberOfPasses-1;   // We are counting from 0, so length(0..numberOfPasses-1)=numberOfPasses

//================= MAT structure ========================
lrp.mat.A=A;
lrp.mat.B=B;
lrp.mat.B0=B0;
lrp.mat.C=C;
lrp.mat.D=D;
lrp.mat.D0=D0;

//================= CONTROLLER NONE structure ========================
lrp.controller(1).A=A;
lrp.controller(1).B=B;
lrp.controller(1).B0=B0;
lrp.controller(1).C=C;
lrp.controller(1).D=D;
lrp.controller(1).D0=D0;




//====================================================
//BOUNDARY CONDITIONS
//====================================================

//=================== boundary conditions ===========================
if FUNCTION_IS_USED then
		//THE FUNCTION_FOR_BOUNDARY MUST BE DEFINED AS FOLLOWS:
		//
		// function [x0, y0]=FUNCTION_FOR_BOUNDARY( ...
		//                                          numberOfPoints, ...
		//                                          numberOfPasses, ...
		//                                          n_numberOfStates,...
		//                                          r_numberOfInputs, ...
		//                                          m_numberOfOutputs ...
		//            )
		//
		// It must return correct dimensioned matrices x0 and y0. 
		//    Sizes of those output arguments should be
		// x0 - [n x numberOfPasses ]
		// y0 - [m x numberOfPoints ]
	
   try
      [lrp]=FUNCTION_FOR_BOUNDARY(lrp);
   catch
      printf("******************************************************\n");
      printf("Probably used incorect function\n    for boundary conditions in LRP model.");
      printf("\n\nYou should define function such as below:\n\n");
      printf("function [lrp]=FUNCTION_FOR_BOUNDARY(lrp)\n");
      printf("\n\n   BODY OF FUNCTION\n\nendfunction\n");
      printf("\n\nor use functions from directory:\n");
      printf("%s\n\n", fullfile(LRP_OPTIONS.path,"boundary_conditions"));
      printf("******************************************************\n");
      error(lasterror(%t));
   end

    //size are OK ?
    if ~and(size(lrp.ini.x0)==[n numberOfPasses]) then
        printf("*********************************************************\n");
        printf("Size of ""lrp.ini.x0"" is incorrect.\n") ;
        printf("Size of ""lrp.ini.x0"" is currently: [%d, %d].\n", size(boundaryX0)) ;
        printf("Correct size of ""lrp.ini.x0"" should by: [%d, %d].\n", [n numberOfPasses]) ;
        printf("*********************************************************\n");
        error("Wrong size.") ;
    end
    if ~and(size(lrp.ini.y0)==[m numberOfPoints]) then
        printf("*********************************************************\n");
        printf("Size of ""lrp.ini.y0"" is incorrect.\n") ;
        printf("Size of ""lrp.ini.y0"" is currently: [%d, %d].\n", size(boundaryY0)) ;
        printf("Correct size of ""lrp.ini.y0"" should by: [%d, %d].\n", [m numberOfPoints]) ;
        printf("*********************************************************\n");
        error("Wrong size.") ;
    end
end

if MATRIX_OR_REAL_IS_USED then
  if size(boundaryX0,"*") > 1 then
    //MATRICES ARE USED
         if ~and(size(boundaryX0)==[n numberOfPasses]) then
              printf("*********************************************************\n");
              printf("Size of ""boundaryX0"" is incorrect.\n") ;
              printf("Size of ""boundaryX0"" is currently: [%d, %d].\n", size(boundaryX0)) ;
              printf("Correct size of ""boundaryX0"" should by: [%d, %d].\n", [n numberOfPasses]) ;
              printf("*********************************************************\n");
              error("Wrong size.") ;
         end
         lrp.ini.x0=boundaryX0;
   else
    //REAL SCALARS ARE USED
     // k (passes) start from 0 to beta-1 <0..beta-1>, so it is equal to beta points
     lrp.ini.x0 = ones(n, numberOfPasses) .* boundaryX0;
	end         
  if size(boundaryY0,"*") > 1
         if ~and(size(boundaryY0)==[m numberOfPoints]) then
              printf("*********************************************************\n");
              printf("Size of ""boundaryY0"" is incorrect.\n") ;
              printf("Size of ""boundaryY0"" is currently: [%d, %d].\n", size(boundaryY0)) ;
              printf("Correct size of ""boundaryY0"" should by: [%d, %d].\n", [m numberOfPoints]) ;
              printf("*********************************************************\n");
              error("Wrong size.") ;
         end
         lrp.ini.y0=boundaryY0;
   else
     //REAL SCALARS ARE USED
     // p (points) start from 0 to alpha-1 <0..alpha-1>, so it is equal to alpha points
	   lrp.ini.y0 = ones(m, numberOfPoints) .* boundaryY0;
   end
end


//result=isGoodLRP(lrp, "lrp", %T);

endfunction
