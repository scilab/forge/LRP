// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 10-10-2008, 10:40

function [surface3D]=lrpCalculateSurface3D(lrp, lrpPlotData)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
//------------------check arguments------------------------------------------------
checkLRP(lrp);

[nargout,nargin]=argn();
if (nargin==0 | nargin>2) then
  error("Function requires 1 or 2 input arguments.");
end

if (nargout>1) then
  error("Function requires only 1 output argument.");
end

requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");
if nargin==2 then
	requireType("lrpPlotData", lrpPlotData, "tlist", "lrp_Sys_Cla_Plt");
	requireRange("lrpPlotData.maxPoints", lrpPlotData.maxPoints, lrp.dim.pmin, lrp.dim.pmax);
	requireRange("lrpPlotData.maxPasses", lrpPlotData.maxPasses, lrp.dim.kmin, lrp.dim.kmax);
end
//requireMatrixSize("lrp.ini.x0",lrp.ini.x0, [lrp.dim.n, lrp.dim.beta+1]);
requireMatrixSize("lrp.ini.x0",lrp.ini.x0, [lrp.dim.n, lrp.dim.beta]);
requireMatrixSize("lrp.ini.y0",lrp.ini.y0, [lrp.dim.m, lrp.dim.alpha]);

//------------------------ main part -----------------------------------------------------

// !!!!!!!!!!!!!!!!!!!!!!!!!!
//we start from:  pmin (kmin) up to lrpPlotData.maxPoins (lrpPlotData.maxPasses)
// !!!!!!!!!!!!!!!!!!!!!!!!!!
if nargin==2 then
	pointsRange=lrp.dim.pmin : lrpPlotData.maxPoints;
	passesRange=lrp.dim.kmin : lrpPlotData.maxPasses;
else
	pointsRange=lrp.dim.pmin : lrp.dim.pmax;
	passesRange=lrp.dim.kmin : lrp.dim.kmax;
end


//create grid !!!!!!!!!!!
[pointsGrid, passesGrid]=ndgrid(pointsRange, passesRange);

//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;
//points and passes for matrix indices
pointsIndicesForMatrix = pointsRange + offsetForPoints;
passesIndicesForMatrix = passesRange + offsetForPasses;


// both passesIndicesForMatrix and pointsIndicesForMatrix are 1-based vectors
requireRange("passesIndicesForMatrix",passesIndicesForMatrix, [1, size(lrp.ini.x0,2)]);
requireRange("pointsIndicesForMatrix",pointsIndicesForMatrix, [1, size(lrp.ini.y0,2)]);

//in general
//======================
// X=zeros(lrp.dim.alpha, lrp.dim.beta+1, lrp.dim.n) * %nan;  //state
// Y=zeros(lrp.dim.alpha, lrp.dim.beta+1, lrp.dim.m) * %nan;  //output

//to maximal range on points and passes
X=zeros(pointsIndicesForMatrix($), passesIndicesForMatrix($), lrp.dim.n) * %nan;  //state
U=zeros(pointsIndicesForMatrix($), passesIndicesForMatrix($), lrp.dim.r) * %nan;  //input
Y=zeros(pointsIndicesForMatrix($), passesIndicesForMatrix($), lrp.dim.m) * %nan;  //output
//
//in general bonduary condition can be calculated as
//
//x0=ones(n_numberOfStates, numberOfPasses);  //for example
//y0=zeros(m_numberOfOutputs, numberOfPoints);    //for example
//
//========================================================================
// //setting boundaries on matrices: X (state surface for 3D) and Y (output surface for 3D)
// //
//X(1,:,:)=x0';  //see the transpose sign ' here
// //
// //the first column (pass) of every matrix state we don't know, we set to 'NaN'
// //the first column of every matrix is 'NaN', bacause, x_{0}(p) does not exixst
// X(:,1,:)=%nan;
// //
//Y(:,1,:)=y0' ;   //note the transpose sign >>'<< here
//========================================================================

[surfaceForStates, surfaceForOutputs, surfaceForInputs]=lcCalculateSurface( ...
          lrp, ...
                  pointsIndicesForMatrix, passesIndicesForMatrix, ...
                  X, Y, U ...
);

surface3D=tlist( ...
["lrp_Sys_Cla_PS_3D"; ...
      "passes"; ...                      //grid on axis OX (passes)
      "points"; ...                      //grid on axis OY (points)
      "state"; "output";"input" ...
], ...
   passesGrid, pointsGrid, ...
   surfaceForStates, surfaceForOutputs, surfaceForInputs ...
);

TCL_SetVar("LRP_Help_Status","Ready");

endfunction



function [X, Y, U]=lcCalculateSurface(lrp, pointsIndicesForMatrix, passesIndicesForMatrix, X, Y, U)
// **** UNSAFE **** - no type check is performed

// disp(lrp.ini)
// disp(passesIndicesForMatrix)
// disp(pointsIndicesForMatrix)
// disp(lrp.dim)

x0=lrp.ini.x0(:, passesIndicesForMatrix);
y0=lrp.ini.y0(:, pointsIndicesForMatrix);

funUGenerator = eval(lrp.ext.uGenerator);


for k=passesIndicesForMatrix(1)  : passesIndicesForMatrix($) - 1  // minus 1, because 'X' starts from 'k+1', but 'Y' starts from 'k'

    //static boundary conditions - initial pass
  if k==passesIndicesForMatrix(1) then
    Y(:,k,:)=y0' ;   //mind the transpose sign ' here
  end


  //disp(pointsIndicesForMatrix)

  for p=pointsIndicesForMatrix(1) : pointsIndicesForMatrix($)

    //static boundary conditions - initial state points
    if p == pointsIndicesForMatrix(1) then
      X(p,:,:)=x0'; //mind the transpose sign ' here
    end

         x=lcGetVectorFromNDMatrix(X(p, k+1, :));  //I choose row 'p' and column 'k+1' from each dimension of matrix X
         //u=lcGetVectorFromNDMatrix(U(p, k+1, :));
         [u,parameters] = funUGenerator(lrp,k+1,p, X, U, Y); // Calculate the input vector u
         lrp.ext.uParameters = parameters;
         U(p-pointsIndicesForMatrix(1)+1,k+1-passesIndicesForMatrix(1)+1,:) = u;
         y = lcGetVectorFromNDMatrix(Y(p, k, :));

         Y(p, k+1, :)     = lrp.mat.C * x + lrp.mat.D * u + lrp.mat.D0 * y;

         if p < pointsIndicesForMatrix($),
            X(p+1, k+1, :) = lrp.mat.A * x + lrp.mat.B * u + lrp.mat.B0 * y;
         end

    end
end
endfunction



function [result]=lcGetVectorFromNDMatrix(matrixND)
// **** UNSAFE **** - no type check is performed. A global
// version (with full arguments checking)  of this
// function exists - getVectorFromNDMatrix
THROW_ERROR_FOR_NAN=%T;

if ndims(matrixND)>2,
  sizeMatrixND = size(matrixND);
  sizeMatrixND(sizeMatrixND==1)=[];     // Remove one dimension.
  sizeMatrixND = [sizeMatrixND ones(1,2-length(sizeMatrixND))];  // it must be minimum 2D
  result = matrix(matrixND, sizeMatrixND);
else
  result = matrixND;
end

if or(isnan(result)) & THROW_ERROR_FOR_NAN then
  error("%NaN encountered.");
end
endfunction
