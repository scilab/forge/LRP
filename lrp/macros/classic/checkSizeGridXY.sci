// 'M' can be:  'X' or 'Y'
function checkSizeGridXY(lrp,M,grid_X,grid_Y,is_X)

//misc. informations
if (is_X==1) then 
  inf1="''X''";
  inf2="n"
else 
  inf1="''Y''";
  inf2="m";
end   
[n,r]=size(lrp.mat.B);
m=size(lrp.mat.C,1);

//dimensions of matrix X or Y
if (ndims(M)~=3) then  
   //message error
   disp(strcat(["Don''t match sizes matrix: " inf1 "."]))
   error(strcat(["It has to be equal 3 ''[lrp.dim.alpha x (lrp.dim.beta+1) x " inf2 "]''."]));
end   

//dimension grid X
if (ndims(grid_X)~=2)   then
   error("Don''t match sizes matrix: ''grid_X''. It has to be equal 2 ''[lrp.dim.alpha x (lrp.dim.beta+1)]''.");
end   

//dimension grid Y
if (ndims(grid_Y)~=2)   then
   error("Don''t match sizes matrix: ''grid_Y''. It has to be equal 2 ''[lrp.dim.alpha x (lrp.dim.beta+1)]''.");
end   

//size X or Y
[my_alpha,my_beta,my_nm]=size(M); 
if (((lrp.dim.alpha ~= my_alpha) | (lrp.dim.beta+1 ~= my_beta)) & ((n ~= my_nm) | (m ~= my_nm))) then
   error(strcat(["The size matrix " inf1 " has to be equal ''[lrp.dim.alpha x (lrp.dim.beta+1) x " inf2 "]''."]));
end
	
//size grid X 
[my_alpha,my_beta]=size(grid_X)
if (lrp.dim.alpha ~= my_alpha) | (lrp.dim.beta+1 ~= my_beta) then
   error(strcat(["The size matrix ''grid_X'' has to be equal ''[lrp.dim.alpha x (lrp.dim.beta+1)]''."]));
end

//size grid Y 
[my_alpha,my_beta]=size(grid_Y)
if (lrp.dim.alpha ~= my_alpha) | (lrp.dim.beta+1 ~= my_beta) then
   error(strcat(["The size matrix ''grid_Y'' has to be equal ''[lrp.dim.alpha x (lrp.dim.beta+1)]''."]));
end
endfunction
