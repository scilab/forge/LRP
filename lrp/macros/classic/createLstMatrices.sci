//matrices "A","B","B0" must be sizes of "n" by "n","r","m" respectively
//matrices "C","D","D0" must be sizes of "m" by "n","r","m" respectively


//inputs parameters: (two way)
//==========================
//1.
//"A","B","B0","C","D","D0"
//
//for example
//[matricesLRP]=makeMatricesLRP(A,B,B0,C,D,D0)

//2.
//"A", where "A" is only one parameter and "A" is "lrp" model
//
//for example
//[matricesLRP]=makeMatricesLRP(lrp)



function [matricesLRP]=createLstMatrices(A,B,B0,C,D,D0)
[lhs,rhs]=argn();


//checking outputs
if (lhs>1) then
   error("Incorrect number of outputs parameter. It must be only one.");
end;

//checking that "A" is "lrp"
if (rhs==1) then
   if ((type(A)==16) & (typeof(A)=="lrp_model")) then
      matricesLRP=tlist(["lrp_Sys_Cla_Mat";"A";"B";"B0";"C";"D";"D0"],lrp.A,lrp.B,lrp.B0,lrp.C,lrp.D,lrp.D0);
      return;
   else
      error("Wrong type of input parametr. It must be typed list of ""lrp_model"".");
   end
end

//checking inputs
if (rhs ~= 6) then
   error("Wrong number of inputs parameters - it must be six.");
end

//checking types of arguments it must be matrices
if ~((type(A)==type(B) & type(B)==type(C) & type(C)==type(D) & type(D) == type(B0) & type(B0)==type(D0)) ...
   & (type(A)==1 | type(A)==5 )) then
   error("Wrong types of parameters - it must be matrices.");
end


//checking sizes of matrices
//matrices "A","B","B0" must be sizes of "n" by "n","r","m" respectively
//matrices "C","D","D0" must be sizes of "m" by "n","r","m" respectively
// //============================
checkSizesOfMatrices(A,B,B0,C,D,D0);

matricesLRP=tlist(["lrp_Sys_Cla_Mat";"A";"B";"B0";"C";"D";"D0"],A,B,B0,C,D,D0);
endfunction
