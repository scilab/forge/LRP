function []=%lrp_Sys_Cla_P_PP_p(var)
// LRP Toolkit for Scilab. This function is used to display the pass to pass plot data
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:59:00

printf('                          Pass to pass plot\n');
printf('variableName  : %s\n',var.variableName);
printf('variableNumber: %d\n',var.variableNumber);
printf('points        : %d\n',var.points);
printf('passesRange   : %s\n',sci2exp(var.passesRange));
printf('description   : ');
disp(var.description);

endfunction
