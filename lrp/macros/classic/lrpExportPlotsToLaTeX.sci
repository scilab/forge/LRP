// LRP Toolkit for SciLab. This file is used to read the LRP model into the Tcl/Tk toolkit.
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2008-10-10 15:08:00

function lrpExportPlotsToLaTeX(rootDir, lrp, lrpPlotDataResult, U, plotBlackOrColor3DPlot, showInfoAboutPlot)

[nargout, nargin] = argn();
if ((nargin > 6) | (nargin < 5)) then
  error("Wrong number of input arguments. It should be 5 or 6.");
end
disp(fullfile(rootDir,'figures'));
rootDir=fullfile(rootDir,'figures');
[result,ierr]=fileinfo(rootDir)
if ierr~=0 then
	printf("*************************************************\n");
  printf("Directory\n");
  printf(" %s\n",quoteStr(rootDir));
  printf("  does not exist. Use createDirForResult() to create.\n");
  printf("*************************************************\n");
	error("Directory does not exist.");
end
// ==================== Constants ===========================
// Structure types
ALONGTHEPASS_PLOT="lrp_Sys_Cla_P_AP";
PASSTOPASS_PLOT="lrp_Sys_Cla_P_PP";
THREED_PLOT="lrp_Sys_Cla_P_3D";

STATE="state";
OUTPUT="output"
MIN_INDEX=1;
MAX_INDEX=2;
// ==================== Arguments check =====================
requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
requireType("lrpPlotDataResult",lrpPlotDataResult,"tlist","lrp_Sys_Cla_Plt");

requireType("plotBlackOrColor3DPlot", plotBlackOrColor3DPlot, "string");
requireValue("plotBlackOrColor3DPlot", plotBlackOrColor3DPlot, list("black", "color"));

if nargin == 4 then
  showInfoAboutPlot=%F;
else
  requireType("showInfoAboutPlot", showInfoAboutPlot, "boolean");
end

// ==================== Main ================================
global surface3D

for controller=1:length(lrp.controller)
execstr('lrp=set'+lrp.controller(controller).functionName+'(lrp);');
printf('Controller: %s\n',lrp.controller(controller).functionName);
	//calculate surface 3D
	tic
  if lrp.controller(controller).solutionExists==%T then
    	printf("Calculating surface 3D. This may take a while...");

    	  [surface3D]=lrpCalculateSurface3D(lrp, lrpPlotDataResult);
    	    //surface3D=createGSO(lrp, U);
    	printf(" done.\n");
    	printf("Time of calculating=%g\n",toc());

    	//for plotNumber=1 : length(lrpPlotDataResult.plotData)
    	for plotNumber=1 : lstsize(lrpPlotDataResult.plotData)

    	     select typeof(lrpPlotDataResult.plotData(plotNumber));
    	       case ALONGTHEPASS_PLOT then
    	//            //lrpAlongThePassPlot=lrpPlotDataResult.plotData(plotNumber);

    	    lrpPlotAlongThePass(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber,showInfoAboutPlot);

    	//            // Get "x" and "y" values
    	//            [x, y]=lcLrpPlotAlongThePass(lrp, lrpAlongThePassPlot, surface3D);
    	//            description=lrpAlongThePassPlot.description;
    	//
    	//            lrpPlot2D(x, y, description);

    	       case PASSTOPASS_PLOT then
    	            lrpPlotPassToPass(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber, showInfoAboutPlot);
    	       case THREED_PLOT then
    	            lrpPlot3D(lrp, lrpPlotDataResult.plotData(plotNumber), surface3D, plotNumber, plotBlackOrColor3DPlot, showInfoAboutPlot);
    	       else
    	           error("Unknown plot type: "+ typeof(lrpPlotDataResult.plotData(plotNumber)));
    	      end
    	      fig=gcf();
    	      name=lrpPlotDataResult.plotData(plotNumber).description.name;
    	      mkdir("",quoteStr(fullfile(rootDir,lrp.controller(controller).functionName)));
    				saveLaTeXFigure(fig.figure_id,fullfile(rootDir,lrp.controller(controller).functionName,name));
    	end
     else
         	printf("NO SOLUTIONS FOUND...\n");
   end
end
printf('Result saved in: %s\n',rootDir);
disp('*** DONE ***');
endfunction


function [x, y]=lcLrpPlotAlongThePass(lrp, lrpAlongThePassPlot, surface3D)
// *** UNSAFE ***

//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;

select lrpAlongThePassPlot.variableName
  case STATE then
      M=surface3D.state;
  case OUTPUT then
      M=surface3D.output;
  else
     error("Unknown field: "+plotData.variableName+".");
end

// The lrpAlongThePassPlot.pointsRange is [min,max] (for example [1,4]) but
//  we need the continuous range for points i.e. [1,2,3,4] - hence
//  the need for convertion
// This is the real range, as shown on the legend of the plot
pointsRange=lrpAlongThePassPlot.pointsRange(MIN_INDEX) : lrpAlongThePassPlot.pointsRange(MAX_INDEX);

// This will be used for indexing the matrix, so it MUST start from 1. Hence
//   we add the offset
pointsIndicesForMatrix=pointsRange + offsetForPoints;
passesIndicesForMatrix=lrpAlongThePassPlot.passes + offsetForPasses;

values=M( ...
   pointsIndicesForMatrix , ...
   passesIndicesForMatrix , ...
   lrpAlongThePassPlot.variableNumber ...
);

x=pointsRange;
y=values;

endfunction
