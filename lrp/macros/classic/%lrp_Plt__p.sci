function []=%lrp_Plt__p(lrp)
// LRP Toolkit for Scilab. This function is used to call an appropriate
//  display function. The reason is that the Scilab takes at most 8 characters as
//  a type name for overloading.
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-05 15:12:00

//   disp('Calling!');
//   disp('%'+typeof(lrp)+'_p(l)');
  if (exists('%'+typeof(lrp)+'_p')) then
    execstr('%'+typeof(lrp)+'_p(lrp)');
  else
      warning(sprintf('%s\n%s','No custom display function for type '+typeof(lrp)+'.','  Define: function []=%'+typeof(lrp)+'_p(var)'));
      display(lrp);
  end
endfunction

