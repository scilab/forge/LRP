function []=%lrp_Sys_Cla_P_3D_p(var)
// LRP Toolkit for Scilab. This function is used to display the 3D plot data
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:59:00

printf('                          3D plot\n');
printf('variableName  : %s\n',var.variableName);
printf('variableNumber: %d\n',var.variableNumber);
printf('pointsRange   : %s\n',sci2exp(var.pointsRange));
printf('passesRange   : %s\n',sci2exp(var.passesRange));
printf('description   : ');
disp(var.description);

endfunction
