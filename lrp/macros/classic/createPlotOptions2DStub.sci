// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: Mon Aug 21 14:58:30 CEST 2006


// //-----------------------------------------------------
// //          Default property to plot 2D
// //-----------------------------------------------------
// // You can obtain it from the folowing code:
//
// xdel(winsid());
// x=1:10;
// y=rand(3,10);
// plot(x, y);
// h=scf(0);
//
// //from plot 1 - first row of y we have
//
// h.children.children.children(1)


// //------------------------------------------------------------------
// //PROPERTY_NAME,       DEFAULT_VALUE
// //------------------------------------------------------------------
// // "parent": Compound            ---> we omit this property
// // "children": []                ---> we omit this property
// // "visible"                     => "on"
// // "data" = matrix 10x2          ---> we omit the data value, of course you can change this parametr,
// //                                    but we do not perform any check on this operation
// // "closed"                      => "off"
// // "line_mode"                   => "on"
// // "fill_mode"                   => "off"
// // "line_style"                  => 1
// // "thickness"                   => 1
// // "arrow_size_factor"           => 1
// // "polyline_style"              => 1
// // "foreground"                  => 5
// // "background"                  => -2
// // "interp_color_vector"         => []
// // "interp_color_mode"           => "off"
// // "mark_mode"                   => "off"
// // "mark_style"                  => 0
// // "mark_size_unit"              => "point"
// // "mark_size"                   => 0
// // "mark_foreground"             => -1
// // "mark_background"             => -2
// // "x_shift"                     => []
// // "y_shift"                     => []
// // "z_shift"                     => []
// // "bar_width"                   => 0
// // "clip_state"                  => "clipgrf"
// // "clip_box"                    => []
// // "user_data"                   => []


// //And a "color_map" is obtained from:
// h
// //or these values are obtained from
// h.color_map
//
//-----------------------------------------
// R                    G                  B
//-----------------------------------------
//     0.                  0.                  0.
//     0.                  0.                  1.
//     0.                  1.                  0.
//     0.                  1.                  1.
//     1.                  0.                  0.
//     1.                  0.                  1.
//     1.                  1.                  0.
//     1.                  1.                  1.
//     0.                  0.                  0.5647059
//     0.                  0.                  0.6901961
//     0.                  0.                  0.8156863
//     0.5294118    0.8078431    1.
//     0.                  0.5647059    0.
//     0.                  0.6901961    0.
//     0.                  0.8156863    0.
//     0.                  0.5647059    0.5647059
//     0.                  0.6901961    0.6901961
//     0.                  0.8156863    0.8156863
//     0.5647059    0.                  0.
//     0.6901961    0.                  0.
//     0.8156863    0.                  0.
//     0.5647059    0.                  0.5647059
//     0.6901961    0.                  0.6901961
//     0.8156863    0.                  0.8156863
//     0.5019608    0.1882353    0.
//     0.627451      0.2509804    0.
//     0.7529412    0.3764706    0.
//     1.                  0.5019608    0.5019608
//     1.                  0.627451      0.627451
//     1.                  0.7529412    0.7529412
//     1.                  0.8784314    0.8784314
//     1.                  0.8431373    0.
//     0.                  0.5                0.


// //property for:
// //x_label, y_label, z_label - only setting 'text' property, and 'font_size'
//
// //these can be obtained from:
// h.children.x_label  //smilarly the rest
//
// //and we have
// // Handle of type "Label" with properties:
// // =======================================
// // parent: Axes
// // visible = "on"
// // text = ""                          <-- we can setting only this value
// // foreground = -1
// // background = -2
// // fill_mode = "off"
// // font_style = 6
// // font_size = 1                      <-- we can setting only this value
// // font_angle = 0
// // auto_position = "on"
// // position = [5.5,-0.0827910]
// // auto_rotation = "on"

// -------------------------------------------------------------------
// THE REST OF PARAMETRS OF FIGURE
// -------------------------------------------------------------------
// you can set individually by returned handle to this graphic window
// for example a legend (see: help legend; or help legends)
// POKAZAC KONKRETNY PRZYKLAD
// -------------------------------------------------------------------


function [lrpPlotOptions2D]=createPlotOptions2DStub()

PROPERTY=[...
    "visible";           "data";            "closed";          "line_mode";...
    "fill_mode";         "line_style";      "thickness";       "arrow_size_factor"; ...
    "polyline_style";    "foreground";      "background";      "interp_color_vector"; ...
    "interp_color_mode"; "mark_mode";       "mark_style";      "mark_size_unit";  ...
    "mark_size";         "mark_foreground"; "mark_background"; "x_shift"; ...
    "y_shift";           "z_shift";         "bar_width";       "clip_state"; ...
    "clip_box";          "user_data";  ...
    "color_map";  ...
    "x_label";           "y_label";         "z_label" ...
];

//default values for x_label, y_label, z_label
propXYZLabel=tlist(...
[...
"lrp_Plt_Options2DL"; ...
    "text"; "font_size" ...
],...
    "", 1 ...
);


//DEFAULT VALUES FOR PROPERTIES
lrpPlotOptions2D=tlist(...
[ ...
"lrp_Plt_Options2D"; ...
PROPERTY ...
], ...
    "on",   [],     "off",      "on", ...
    "off",  1,      1,          1, ...
    1,      5,      -2,         [], ...
    "off",  "off",  0,          "point",  ...
    0,      -1,     -2,         [],...
    [],     [],     0,          "clipgrf", ...
    [],     [], ...
    [], ...
    propXYZLabel, propXYZLabel, propXYZLabel ...
);
endfunction
