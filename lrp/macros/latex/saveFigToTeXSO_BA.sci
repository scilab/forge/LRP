// parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
//  'nameOfLatexFile';'nameOfController';'wFigPlot'],...
//   nameOfLatexFile,  nameOfController,  wFigPSO);

function saveFigToTeXSO_BA(f,lrp,parToLaTeXSaveFig,howManyPlotSO)

[nargout, nargin]=argn();


if parToLaTeXSaveFig.nameOfController=='none' then
    error('Cannot use this controller: ' + parToLaTeXSaveFig.nameOfController + ' with this function: saveFigToTeXSO_AB.');
end

[ind]=findLRPIndex(lrp,'controller',parToLaTeXSaveFig.nameOfController);
if ind==-1 then
    error('No such conntroller: '+ parToLaTeXSaveFig.nameOfController);
end

//nothing to plot
//if (isempty(parToLaTeXSaveFig.wFigPlot.state.threeD) | isempty(parToLaTeXSaveFig.wFigPlot.output.threeD)) then
    //return;
//end


//nos - number of states
nos=length(parToLaTeXSaveFig.wFigPlot.state);
//noo - number of outputs
noo=length(parToLaTeXSaveFig.wFigPlot.output);
howManyPlotSOtmp=[nos noo];

if nargin==3 then
    howManyPlotSO=howManyPlotSOtmp;
else
    if (howManyPlotSO(1) > nos) | (nos<1)
        error('The number states to plots is wrong. It have to in [1, '+ sci2exp(nos,0) +'].')
    end
    if (howManyPlotSO(2) > noo) | (noo<1)
        error('The number outputs to plots is wrong. It have to in [1, '+ sci2exp(noo,0) +'].')
    end
end


dirForFigLatex=basename(parToLaTeXSaveFig.wFigPlot.name);

direction='3D';

//first state ten output
for k=1:2
    //wField=parToLaTeXSaveFig.wFigPlot(1)(k+1);

    if (k==1) then
        info1='x';
        info2='s';
        variable='state';
    else
        info1='y';
        info2='o';
        variable='output';
    end

    for i=1:length(parToLaTeXSaveFig.wFigPlot(variable).threeD)    //howManyPlotSO(k)
        mfprintf(f,'\n\\begin{figure}[!htb]\n\\centering\n');
        mfprintf(f,'\\begin{tabular}{cc}\n');

            desc=variable + '_' + sci2exp(parToLaTeXSaveFig.wFigPlot(variable).threeD(i),0) + '_' + direction;
            nameGF=strcat([dirForFigLatex '_fig_' 'none'  '_' desc]);  //none controller
            NAME_FILE='./'+dirForFigLatex+'/'+'none'+'/'+ nameGF;
            mfprintf(f,'\t\\includegraphics[scale=0.35]{%s}\n&%%\n', NAME_FILE);

            desc=variable + '_' + sci2exp(parToLaTeXSaveFig.wFigPlot(variable).threeD(i),0) + '_' + direction;
            nameGF=strcat([dirForFigLatex '_fig_' parToLaTeXSaveFig.nameOfController  '_' desc]);
            NAME_FILE='./'+dirForFigLatex+'/'+parToLaTeXSaveFig.nameOfController+'/'+ nameGF   //given controller
            mfprintf(f,'\t\\includegraphics[scale=0.35]{%s}\n',NAME_FILE);
        mfprintf(f,'\\end{tabular}\n');


        mfprintf(f,'\\caption{Simulation for open and closed loop process $%c_k^{%d}(p)$.}\n',...
            info1,parToLaTeXSaveFig.wFigPlot(variable).threeD(i));
        mfprintf(f,'\\label{fig:soba:%s:%c:%d}\n',parToLaTeXSaveFig.nameOfController,...
            info2,parToLaTeXSaveFig.wFigPlot(variable).threeD(i));
        mfprintf(f,'\\end{figure}\n');
    end
end
endfunction
