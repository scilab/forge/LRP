// zm(fid,name,mat,sepYes,frac)
//
// zm - zapisz mat do TeX-a
// fid - uchwyt do pliku, lub 1 - standardowe wyjscie (na ekran)
// name - name macierzy w TeX
// mat - matrix z wartosciami liczbowymi, ktora ma byc zapisana do TeX-a
// sepYes - czy ma byc ustawiony sep miedzy kolumnami na wartosc 1pt, 1 - tak, 0 - nie
// prezyzja - ile liczb po przecinku ma byc zapisanych do TeX-a
//
// Przyklady uzycia:
//
//  A=rand(3,5)
//  zm(1,'\widehat{A}',A)
//  zm(1,'A',A,[],2)
//  zm(1,'\widetilde{A}',A,'2cm',7)


function zm(fid,name,mat,sepYes,frac)

[nargout,nargin]=argn();

if nargin==3, // ma byc ustawiony sep
    sepYes='2pt';
    frac=4;
end

if nargin==4,
    frac=4;
end

[nrRows,nrCols]=size(mat);

// sep miedzy kolumnami
if isempty(sepYes)
    sep='';
else
    sep='{\setlength\arraycolsep{' + sepYes + '}';
end

poczMac='\begin{bmatrix}';
konMac='\end{bmatrix}';

prec1='% 2.' + sci2exp(frac,0) + 'g & ';
prec2='% 2.' + sci2exp(frac,0) + 'g ';

if (nrRows+nrCols==2)
   mfprintf(fid,'%s&='+prec2+'\n',name,mat);
   return;
end

mfprintf(fid,'%s&=\n%s%s\n', name, sep, poczMac);


countR=0;
for i=1:nrRows
   countR=countR+1;
   mfprintf(fid,'\t');
   for j=1:nrCols-1
      number=mat(i,j);
      if number==0,
         mfprintf(fid,'0 & ');
      else
         mfprintf(fid,prec1,number);
      end
      if modulo(j,8)==0, mfprintf(fid,'\n'); end
   end
   number=mat(i,nrCols);

   if number==0,
      mfprintf(fid,'0 ');
   else
      mfprintf(fid,prec2,number);
   end

   if countR ~= nrRows,
       mfprintf(fid,'\\\\\n');
   else
       mfprintf(fid,'\n');
   end
end
mfprintf(fid,'%s',konMac);

if ~isempty(sepYes)
    mfprintf(fid,'}%%');
end

mfprintf(fid,'\n');
endfunction
