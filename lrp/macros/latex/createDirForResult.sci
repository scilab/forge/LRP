function [dirName]=createDirForResult(resultRootDir);
// Creates a valid directory hierarchy for the LaTeX output. 
//  Returns the full path to the root of the hierarchy, i.e. the
//  the directory, in which the LaTeX file will be created  


[lhs, rhs]=argn();   
lhs=argn(1);   
rhs=argn(2);  
// If the user did not supply us the proper root directory, assume
//   LRP_OPTIONS.path/latex/results 

if (rhs==0)
	if ~isdef('LRP_OPTIONS') then
		error('variable ""LRP_OPTIONS"" is undefined. Restart the Toolkit.');
		return;
	end
	resultRootDir=LRP_OPTIONS.results_path;
end
[info,err]=fileinfo(resultRootDir)
if err~=0 then
   // Unable to read the file information
   //Directory does not exist or we have no rights to read it
   // We'll try to create it
   [status, errMessage] = mkdir(quoteStr(fullfile(resultRootDir,"")),"");
   if status==1 then
      warning(sprintf("Directory %s not found - created",quoteStr(resultRootDir)));
   else
       dirName="";
       printf("********************************************\n");
       printf("Unable to create directory\n");
       printf(" %s\n",quoteStr(resultRootDir));
       printf("  for LaTeX output.\n");
       printf("\n Check ''ResultsDir'' entry in file\n");
       printf(" %s\n",fullfile(LRP_OPTIONS.path,"config"));
       printf("********************************************\n");
       error(sprintf("Unable to create directory %s.",quoteStr(resultRootDir)));
       return
   end
else
  // We could read the information.
  if ~isdir(resultRootDir) then
     //This is not a directory (or does not exist)
     dirName="";
     error(sprintf("%s is not a directory!",quoteStr(resultRootDir)));
     return;
  end
end
// Now the resultRootDir is an existing directory that we can write into

//try to create the result directory 10 times
for x=1:10
	dat=getdate();
	dirName=sprintf("%04d-%02d-%02d %02d_%02d_%02d_%03d",dat(1),dat(2),dat(6),dat(7),dat(8),dat(9),dat(10));
	// Create directory; check, if it exists
	[status, errMessage] = mkdir(quoteStr(fullfile(resultRootDir,dirName)),"");
	if status==0 then
		//Critical error - report it and finish
		printf('%s\n',quoteStr(fullfile(resultRootDir,dirName)));
		printf('** Error creating the directory shown above. **\n');
		error(errMessage);
		dirName="";
		return;
	end
	if status==1 then
		//Directory created! Success
		break
	end
	if status==2 then
		// Directory already exist - Wait a random number of seconds; max of about 1 second
		realtimeinit((rand()+0.1)/2); realtime(0); realtime(2);
	end
end
if status==2 then
		printf('%s',quoteStr(fullfile(resultRootDir,dirName)));
		printf('** Unable to create directory shown above. **');
		error(errMessage);
		dirName="";
		return;
end
// At this point the "dirName" directory is created. Create now the "figures" directory
status=mkdir(quoteStr(fullfile(resultRootDir,dirName,"figures")),"");
if status==0 then
		//Critical error - report it and finish
		printf('%s',quoteStr(fullfile(resultRootDir,dirName,"figures")));
		printf('** Error creating directory shown above. **');
		error(errMessage);
		dirName="";
		return;
end
if status==2 then
		//Critical error - report it and finish
		printf('%s',quoteStr(fullfile(resultRootDir,dirName,"figures")));
		error(sprintf('Directory shown above already exists,'));
		dirName="";
		return;
end
// Here we must return the unquoted string, as it will be appended by "\figures"
dirName=fullfile(resultRootDir,dirName);
endfunction
