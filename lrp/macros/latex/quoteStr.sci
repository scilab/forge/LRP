function [aDir]=quoteStr(aDir)
// ON WINDOWS - DOES NOTHING
// ON LINUX - Returns a quoted directory - \home\dir becomes "\home\dir".
// If the directory is already quoted - does nothing
requireType("aDir",aDir,"string");
if ~MSDOS then
   if (part(aDir,1)=="""") & (part(aDir,length(aDir))=="""") then
      //Already quoted - do nothing
      return;
   else
       aDir=sprintf("""%s""", aDir);
       return;
   end
else
      //Already quoted - do nothing
      return;
end
endfunction
