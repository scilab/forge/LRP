function createFigForLaTeX(lrp,lrpMergedDatToPlots,fileName)

cwd_copy=pwd();
chdir(path);




//copy of the current working directory
WORKING_DIRECTORY=pwd();

//__________________________________________________________________________________
//________________________DIRECTORY_________________________________________________
//__________________________________________________________________________________
//create directory for pictures (based on the name of latex file)
//for example: 'file_23.tex' --> then we have: dirForLaTeX == file_23
[path,dirForLaTeX,extension]=fileparts(nameOfLatexFile);

fileNameLaTeX=dirForLaTeX+extension;

if (MSDOS) then
    //is this absolute path
    ind_a=strindex(path,':');
    if ~isempty(ind_a) then
      pathToGraphicsFiles=fullfile(path,dirForLaTeX);
      [status,msg]=mkdir(path,dirForLaTeX);
    else
        pathToGraphicsFiles=fullfile(WORKING_DIRECTORY , path , dirForLaTeX);
        [status,msg]=mkdir(WORKING_DIRECTORY+path,dirForLaTeX);
    end
else
//LINUX
   pathToGraphicsFiles=fullfile(path,dirForLaTeX);
   [status,msg]=mkdir(path,dirForLaTeX);
end

if (status == 0) then
   chdir(WORKING_DIRECTORY);
   error(msg);
end

//setting path to pathToArticleInLaTeX
copy_cwd=pwd();
chdir(pathToGraphicsFiles);
chdir('..');
pathToArticleInLaTeX=pwd();
chdir(copy_cwd);






//**********************************************************************************
//latex
//**********************************************************************************
[f]=latexHeader(title,author,nameOfLatexFile);


//remember
remember_IndController=lrp.indController;

[ind, howManyControllers]=findLRPIndex(lrp,'controller','none');


endfunction