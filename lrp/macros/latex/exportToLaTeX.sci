//TCL_SetVar('LRP_Help_Status','Ready');
//TCL_SetVar('LRP_Help_Status','Working - 5% done');


function exportToLaTeX(lrp,lrpGeneratedFileNumbersForSavingToLaTeX,title,author)

cwd_copy=pwd();
chdir(lrpGeneratedFileNumbersForSavingToLaTeX.path);

nameOfLatexFile=lrpGeneratedFileNumbersForSavingToLaTeX.name;

//**********************************************************************************
//latex
//**********************************************************************************
[f]=latexHeader(title,author,nameOfLatexFile);


//remember
remember_IndController=lrp.indController;

[ind, howManyControllers]=findLRPIndex(lrp,'controller','none');


    k=1;
    //the first controller
    if (lrp.controller(k).solutionExists == %t) then
        nameOfController=lrp.controller(k)(1)(1);

        //description to none
        [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describe'+nameOfController);
        mfprintf(f,"\\input{%s}\n",path_to_file);
        //matrices of none
        mfprintf(f,"\\subsection{System matrices}\n");
        mfprintf(f,"The system matrices of \\eqref{model:DLRP} are:\n");
        execstr('write'+ nameOfController+ '(f,lrp);');

        //structure
        parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
            'nameOfLatexFile';'nameOfController';'wFigPlot'],...
            nameOfLatexFile,  nameOfController,  lrpGeneratedFileNumbersForSavingToLaTeX);

        mfprintf(f,"\\subsection{Simulations for system \\eqref{model:DLRP}}\n");
        //given controller (S)tate and (O)utput
        saveFigToTeX_SO(f,lrp,parToLaTeXSaveFig);
        mfprintf(f,"\\clearpage\n");
        saveFigToTeX_SO_AaP(f,lrp,parToLaTeXSaveFig);
        mfprintf(f,"\\clearpage\n");
    end




    for k=2:howManyControllers

        //if a given solution NOT exists then continue
        if (lrp.controller(k).solutionExists == %f) then
            continue;
        end

        //if a given solution EXISTS

        nameOfController=lrp.controller(k)(1)(1);
        //call apopriate setter
        execstr('[lrp]=set'+nameOfController+'(lrp);');

        [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describe'+nameOfController);
        mfprintf(f,"\\input{%s}\n",path_to_file);
        //matrices of none
        mfprintf(f,"\\subsection{System matrices}\n");
        //mfprintf(f,"The system matrices are:\n");
        execstr('write'+ nameOfController+ '(f,lrp);');

        //structure
        parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
            'nameOfLatexFile';'nameOfController';'wFigPlot'],...
            nameOfLatexFile,  nameOfController,  lrpGeneratedFileNumbersForSavingToLaTeX);

        mfprintf(f,"\\subsection{Simulations for closed loop system \\eqref{model:DLRP}}\n");
        //given controller (S)tate and (O)utput
        //saveFigToTeX_SO(f,lrp,parToLaTeXSaveFig);

        saveFigToTeXSO_BA(f,lrp,parToLaTeXSaveFig);

        mfprintf(f,"\\clearpage\n");
        saveFigToTeX_SO_AaP(f,lrp,parToLaTeXSaveFig);
        mfprintf(f,"\\clearpage\n");
    end


//close latex file
latexFooter(f);


//restore
nameContr=lrp.controller(remember_IndController)(1)(1);
execstr('[lrp]=set'+nameContr+'(lrp);');


//-------------
base_name=fileparts(nameOfLatexFile,'fname');
printf('Create PDF file: \n\t%s.\n\tThis may take a while... ', fullfile(pwd(), base_name + '.pdf'));
run_options='';
if (MSDOS) then
    command='""""'+LRP_OPTIONS.latex_path + '\texify""  --pdf --language=latex --batch -q ""'+ nameOfLatexFile + '""""';
    run_pdf='start';
    com_delete='del';
    com_batchmode='.pdf';
else
   //from help pdflatex: ---> -interaction=  ===> STRING=batchmode/nonstopmode/scrollmode/errorstopmode
   command='""'+LRP_OPTIONS.latex_path + '/pdflatex"" -interaction=batchmode ""'+ nameOfLatexFile + '""';

   //check which program can I run PDF file
   programs_which_can_open_PDF=['evince','xpdf','acroread','kpdf','kghostview','gpdf','gv'];
   run_pdf='';
   for i=1:size(programs_which_can_open_PDF,2)
      results=unix_g('whereis '+programs_which_can_open_PDF(i));
      [ind,which]=strindex(results, programs_which_can_open_PDF(i));
      [ind,which2]=strindex(results, 'bin'); //check that is in binary tree, for example in: /usr/bin
      if (length(which)>=2) & (length(which2)>=1) then
         if programs_which_can_open_PDF(i)=='xpdf' then
            run_options='-z width';
         end
         run_pdf=programs_which_can_open_PDF(i);
         break;
      end
   end
//    run_pdf='acroread';

   com_delete='rm -f';
   com_batchmode='.pdf &';
   //UNIX: run it the FIRST time
   info=unix(command+' > '+base_name+'_log.log');
   if (info ~= 0) then
      error('Error creating PDF-file from: '+fullfile(pwd(), nameOfLatexFile));
   end
end

//UNIX: run it the SECOND time
//WINDOWS: run only once, because texify take care about it!!!

//I add the command: +' > '+base_name+'_log.log' - to write logs to file not to screen
info=unix(command+' > '+base_name+'_log.log');
if (info ~= 0) then
   error('Error creating PDF-file from: '+fullfile(pwd(), nameOfLatexFile));
else
   what_ext_delete=['_log.log', '.log', '.aux', '.out'];
   //prepare files to delete: 'report.log report.aux report.out'
   what_files_delete=base_name+what_ext_delete;
   what_files_delete=strcat(what_files_delete,' ');
   //delete these files
   unix_s(com_delete+' '+what_files_delete)
   printf('done.\n');

   //call the pdfviwer
   if (run_pdf ~= '') then
      printf('Trying to open PDF file:\n\t%s.pdf.\n',fullfile(pwd(), base_name));
      if run_pdf=='acroread' then
         printf('\tStarting ''acroread''. This may take a while.\n');
      end
      unix_s(run_pdf + ' ' + run_options + ' ' + base_name + com_batchmode);
   end
end
//-------------

//go to previous dir
chdir(cwd_copy);
endfunction
