// zapMac(f,macierzSTC,zapisST)
//
// Zapisz macierze zawarte w 'macierzSTC' do pliku 'f' lub na ekran (f==1) w postaci TeX-a
//
// f - uchwyt do pliku
// macierzSTC - struktura komorkowa o nazwach macierzy i same macierze (patrz przyklad)
// zapisST - struktura zapisu
//
// Przyklad uzycia:
// f=mopen('big_matrices.tex','w');

// A=rand(2,30);
// B=rand(6,40);

// matricesLst=tlist(['MatricesLst';...
//     'nazwyMac';...
//     'mac'],...
//     list(),list());

// matricesLst.nazwyMac=list('\widetilde{A}', '\widetilde{B}');
// matricesLst.mac=list(A, B);

// saveLst=tlist(['SaveLst';...
//    'czcionka';...             //which kind of fonts must be set: \small, \footnotesize
//    'odstMiedzyKol';...        //sep beetwen cols in array envirnoment 'bmatrix'
//    'precyzja';...             //precision of value data in matrix
//    'lamacCoIleAlign';...      //cut (sep) in 'align' - how many matrix in one row
//    'lamacCoIleBmatrix'],...   //how many columns in 'bmatrix'
//    '\footnotesize','2pt',4,2,8 ...
//  );

// zapMacPodz(f,matricesLst);
// zapMacPodz(f,matricesLst,saveLst);

// mclose(f);

function zapMacPodz(f,macierzSTC,zapisST);

[nargout,nargin]=argn();

ile_macierzy=length(macierzSTC.mac);

if nargin==2
    saveLst=tlist(['SaveLst';...
      'czcionka';...             //which kind of fonts must be set: \small, \footnotesize
      'odstMiedzyKol';...        //sep beetwen cols in array envirnoment 'bmatrix'
      'precyzja';...             //precision of value data in matrix
      'lamacCoIleAlign';...      //cut (sep) in 'align' - how many matrix in one row
      'lamacCoIleBmatrix'],...   //how many columns in 'bmatrix'
      '\footnotesize','2pt',4,2,8 ...
    );
end

// ustawienie zawsze na 1 !!! (tak musi byc!)
zapisST.lamacCoIleAlign=1;

// czy ustawic czcionke i odstep miedzy kolumnami
if ~isempty(zapisST.czcionka) & ~isempty(zapisST.odstMiedzyKol)
    nap='\setlength\arraycolsep{' + zapisST.odstMiedzyKol + '}';
    mfprintf(f,'{%s%s',zapisST.czcionka,nap);

// czy ustawic odstepy miedzy kolumnami
elseif ~isempty(zapisST.odstMiedzyKol),
    nap='{\setlength\arraycolsep{' + zapisST.odstMiedzyKol + '}';
    mfprintf(f,'%s',nap);

// czy dolaczyc czcionke
elseif ~isempty(zapisST.czcionka)
    mfprintf(f,'{%s',zapisST.czcionka);
end


poczAL='\begin{align*}';
konAL='\end{align*}';

// zapis align
mfprintf(f,'%s\n',poczAL);

licznikM=0;
for i=1:ile_macierzy
    licznikM=licznikM+1;
    zmp(f,macierzSTC.nazwyMac(i), macierzSTC.mac(i), zapisST.lamacCoIleBmatrix, zapisST.precyzja);
    if (licznikM ~= ile_macierzy) & (modulo(i,zapisST.lamacCoIleAlign)~=0),
        mfprintf(f,',&%%\n');
    elseif (modulo(i,zapisST.lamacCoIleAlign) == 0) & (licznikM ~= ile_macierzy)
        mfprintf(f,',\\\\%%\n');
    end
end

// zapis konca align
mfprintf(f,'%s',konAL);

if ~isempty(zapisST.czcionka) & ~isempty(zapisST.odstMiedzyKol),
    mfprintf(f,'}%%');
elseif ~isempty(zapisST.czcionka),
    mfprintf(f,'}%%',nap);
elseif ~isempty(zapisST.odstMiedzyKol),
     mfprintf(f,'}%%',nap);
end

mfprintf(f,'\n');
endfunction
