function writeLMIAlongThePass2(f,lrp)
//------------------------------------------------------------------------------
name="lrp_Sys_Cla_C_L2";
[ind]=findLRPIndex(lrp,"controller",name);
if ind==-1 then
    error("No such conntroller: "+ name);
end
//------------------------------------------------------------------------------
c=lrp.controller(ind);
c1=lrp.controller(1);
//------------------------------------------------------------------------------
matricesLst=tlist(['MatricesLst';...
     'nameOfMat';...
     'mat'],...
     list(),list()...
);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of stable system are\n');
matricesLst.nameOfMat=list('A', 'B', 'B_0', 'C', 'D', 'D_0');
matricesLst.mat=list(c.newA, c1.B, c.newB0, c.newC, c1.D, c.newD0);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of data are\n');
matricesLst.nameOfMat=list('\widehat{A}', '\widehat{B}');
matricesLst.mat=list(c.Ad, c.Bd);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of new data are\n');
matricesLst.nameOfMat=list('new\widehat{A}');
matricesLst.mat=list(c.newAd);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices from LMI controller are\n');
matricesLst.nameOfMat=list('P', 'P_1', 'P_2', 'N', 'N_1', 'N_2');
matricesLst.mat=list(c.P, c.P1, c.P2, c.N, c.N1, c.N2);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of controller are\n');
matricesLst.nameOfMat=list('K', 'K_1', 'K_2');
matricesLst.mat=list(c.K, c.K1, c.K2);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
endfunction
