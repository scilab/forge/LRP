function writeLMIAlongThePass1(f,lrp)
//------------------------------------------------------------------------------
name="lrp_Sys_Cla_C_L1"; //name of controller
[ind]=findLRPIndex(lrp,"controller",name);
if ind==-1 then
    error("No such conntroller: "+ name);
end
//------------------------------------------------------------------------------
c=lrp.controller(ind);
c1=lrp.controller(1);
//------------------------------------------------------------------------------
matricesLst=tlist(['MatricesLst';...
     'nameOfMat';...
     'mat'],...
     list(),list()...
);
//------------------------------------------------------------------------------
// //this is the default values
// saveLst=tlist(['SaveLst';...
//   'fonts';...             //which kind of fonts must be set: \small, \footnotesize
//   'sepBtCols';...         //sep beetwen cols in array envirnoment 'bmatrix'
//   'frac';...              //precision of value data in matrix
//   'cutAlign';...          //cut (sep) in 'align' - how many matrix in one row
//   'cutBmatrix'],...       //how many columns in 'bmatrix'
//   '\small',...   //font
//   '2pt',...      //sep
//   4,...          //precision
//   2,...          //how many matrix in one row in 'align'
//   8 ...
// );
//------------------------------------------------------------------------------
saveLst=tlist(['SaveLst';...
  'fonts';...             //which kind of fonts must be set: \small, \footnotesize
  'sepBtCols';...         //sep beetwen cols in array envirnoment 'bmatrix'
  'frac';...              //precision of value data in matrix
  'cutAlign';...          //cut (sep) in 'align' - how many matrix in one row
  'cutBmatrix'],...       //how many columns in 'bmatrix'
  '\small',...   //font
  '2pt',...      //sep
  4,...          //precision
  1,...          //how many matrix in one row in 'align'
  8 ...
);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of stable system are\n');
matricesLst.nameOfMat=list('A', 'B', 'B_0', 'C', 'D', 'D_0');
matricesLst.mat=list(c.newA, c1.B, c.newB0, c.newC, c1.D, c.newD0);
saveMat(f,matricesLst,saveLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of data are\n');
matricesLst.nameOfMat=list(...
    '\widehat{A}', '\widehat{A}_1', '\widehat{A}_2' ...
);
matricesLst.mat=list(c.Ad, c.Ad1, c.Ad2);
saveMat(f,matricesLst,saveLst);
//------------------------------------------------------------------------------
matricesLst.nameOfMat=list(...
    '\widehat{B}', '\widehat{B}_1', '\widehat{B}_2' ...
);
matricesLst.mat=list(c.Bd, c.Bd1, c.Bd2);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of new data are\n');
matricesLst.nameOfMat=list(...
    'new\widehat{A}', 'new\widehat{A}_1', 'new\widehat{A}_2',...
);
matricesLst.mat=list(c.newAd, c.newAd1, c.newAd2);
saveMat(f,matricesLst,saveLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices from LMI controller are\n');
matricesLst.nameOfMat=list('Z', 'Y', 'N', 'N_1', 'N_2');
matricesLst.mat=list(c.Z, c.Y, c.N, c.N1, c.N2);
saveMat(f,matricesLst,saveLst);
//------------------------------------------------------------------------------
mfprintf(f,'Matrices of controller are\n');
matricesLst.nameOfMat=list('K', 'K_1', 'K_2');
matricesLst.mat=list(c.K, c.K1, c.K2);
saveMat(f,matricesLst,saveLst);
//------------------------------------------------------------------------------
endfunction
