\section{Controller 2}
The control law considered in this paper is given over $0\leq p
\leq \alpha-1, k \geq 0$ by
\begin{align}
  u_{k+1}(p)&=
  \begin{bmatrix}
    K_1 & K_2
  \end{bmatrix}
  \begin{bmatrix}
    x_{k+1}(p)\\
    y_{k}(p)
  \end{bmatrix}
  = K
  \begin{bmatrix}
    x_{k+1}(p)\\
    y_{k}(p)
  \end{bmatrix}
  \label{controller2:control:law}
\end{align}
where $K_1$ and  $K_2$ are appropriately dimensioned matrices to be designed.
In effect, this control law uses feedback of the current state vector (which
is assumed to be available for use) and `feedforward' of the previous pass
profile vector. Note that in the repetitive process literature the term
`feedforward' is used to describe the case where state or pass profile
information from the previous pass (or passes) is used as (part of) the input
to a control law applied on the current pass, i.e. to information which is
propagated in the pass-to-pass ($k$) direction.

\begin{theorem}
\label{th:controller1:0}
Suppose that a discrete linear repetitive process of the form described
by~\eqref{model:DLRP} and \eqref{b:condition} is subjected to a control law of
the form~\eqref{controller2:control:law}. Then the resulting closed loop process is stable
along the pass if $\exists$ matrices $P=P^T$ and $Q=Q^T$ such that
\begin{align}
\begin{bmatrix}
    (\widehat{A}^T_1+K^T\widehat{B}^T_1) P (\widehat{A}_1+\widehat{B}_1K)+Q-P
    & (\widehat{A}^T_1+K^T\widehat{B}^T_1) P (\widehat{A}_2+\widehat{B}_2K)
    \\
    (\widehat{A}^T_2+K^T\widehat{B}^T_2) P (\widehat{A}_1+\widehat{B}_1K)
    & (\widehat{A}^T_2+K^T\widehat{B}^T_2) P (\widehat{A}_2+\widehat{B}_2K)
\end{bmatrix} < 0.
\label{cl:controller1}
\end{align}
\end{theorem}

The major remaining difficulty with the matrix inequality of
Theorem~\ref{th:controller1:0} is that it is nonlinear in its parameters. It
can, however, be converted into the following result where the inequality is
a~strict LMI with a linear constraint which also gives a formula for computing
$K$ in~\eqref{controller2:control:law}.

\begin{theorem}
\label{th:controller2:1}
Suppose that a discrete linear repetitive process of the form described
by~\eqref{model:DLRP} and \eqref{b:condition} is subjected to a control law of
the form~\eqref{controller2:control:law}. Then the resulting closed loop process is stable
along the pass if $\exists$ matrices $P=\diag{P_1, P_2}>0$, $P_1=P_1^T>0$,
$P_2=P_2^T>0$ and $N=\begin{bmatrix}N_1 & N_2\\N_1 & N_2\end{bmatrix}$ such
that
\begin{align}
    \begin{bmatrix}
        -P & \widehat{A}P+\widehat{B}N\\
        P\widehat{A}^T+N^T\widehat{B}^T & -P
    \end{bmatrix}<0
    \label{lmi:controller2}
\end{align}
holds. Also a stabilizing $K$ in the control law \eqref{controller2:control:law} is given
by
\begin{align}
    K=NP^{-1}
    \label{K:controller2}
\end{align}
\end{theorem}

\begin{proof}
First apply the Schur's complement formula of \eqref{schur:2} to
\eqref{cl:controller2}, with
\begin{align*}
W&=\begin{bmatrix}
    Q-P & 0\\
    0 & -Q
\end{bmatrix},
&
V&=P^{-1},
&
L&=P \begin{bmatrix}
(\widehat{A}_1+\widehat{B}_1K) & (\widehat{A}_2+\widehat{B}_2K)
\end{bmatrix}.
\end{align*}
Now left and right multiply the result from this step by
$\diag{P^{-1},P^{-1},P^{-1}}$, and then make the following substitutions
\begin{align*}
    Z&=P^{-1}QP^{-1}>0, & Y&=P^{-1}>0,
\end{align*}
to yield
\begin{align}
    \begin{bmatrix}
        Z-Y & 0 & Y (\widehat{A}_1^T+K^T\widehat{B}_1^T)\\
        0 & -Z & Y (\widehat{A}_2^T+K^T\widehat{B}_2^T)\\
        (\widehat{A}_1+\widehat{B}_1K)Y & (\widehat{A}_2+\widehat{B}_2K)Y & -Y
    \end{bmatrix}< 0.
\end{align}
Application of the formula given by \eqref{K:controller1} now completes the
proof.
\end{proof}