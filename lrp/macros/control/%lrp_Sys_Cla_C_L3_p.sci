// LRP Toolkit for Scilab. This function is used to display the results of the LMIAlongThePass3
//   controller
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 02:05:00
function []=%lrp_Sys_Cla_C_L3_p(var)
printf('  %s:\n',var.displayName);
printf('  file: %s.sci:\n',var.functionName);
//printf('  LMI Along the pass 3:\n')
printf('Result:\n')
if var.solutionExists==%t then
		printf("P\n");
		disp(var.P);
		printf("Q\n");
		disp(var.Q);
		printf("G\n");
		disp(var.G);
		printf("G1\n");
		disp(var.G1);
		printf("G2\n");
		disp(var.G2);
		printf("N\n");
		disp(var.N);
		printf("N1\n");
		disp(var.N1);
		printf("N2\n");
		disp(var.N2);
		printf("K\n");
		disp(var.K);
		printf("K1\n");
		disp(var.K1);
		printf("K2\n");
		disp(var.K2);
    printf("Nd1\n");
    disp(var.Nd1);
    printf("Nd2\n");
    disp(var.Nd2);
    printf("Gd1\n");
    disp(var.Gd1);
    printf("Gd2\n");
    disp(var.Gd2);
		printf("As1\n");
		disp(var.As1);
		printf("As2\n");
		disp(var.As2);
		printf("Ad\n");
		disp(var.Ad);
		printf("Bd\n");
		disp(var.Bd);
    printf("newAd\n");
		disp(var.newAd);
printf('\nClosed loop matrices:\n')
    printf("newA\n");
    disp(var.newA);
		printf("newB0\n");
disp(var.newB0);		
		printf("newC\n");
disp(var.newC);		
		printf("newD0\n");
disp(var.newD0);		
else
	printf('UNABLE TO CALCULATE CONTROLLER USING %s\n',var.functionName);
end
endfunction
