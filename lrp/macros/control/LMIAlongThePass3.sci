// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 02:05:00 by L.Hladowski
//
//*****************************************************************************
// Stabilization 3
//*****************************************************************************
// Article:
// "LMI based stability analisis and robust controller design for discrete
//  linear repetetive processes"
// pp.7 eq. (29)
//*****************************************************************************
//
// P > 0
// Q > 0
// [Q-P,               0,                  As1*G1d+R*N1d]
// [0,                 -Q,                 As2*G2d+R*N2d] < 0
// [(As1*G1d+R*N1d)',  (As2*G2d+R*N2d)',   P-G-G'       ]


function [lrp]=LMIAlongThePass3(lrp)
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","**** WORKING ****");
end
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

  lrp=setnone(lrp); //Start from the original system, not the current, (possibly closed-loop) one.

[ind,last]=findLRPIndex(lrp,"controller","lrp_Sys_Cla_C_L3");
if ind==-1 then
    lrp.indController=last+1;
else
    lrp.indController=ind;
end

Bd=blkdiag(lrp.mat.B, lrp.mat.D);
As1=[lrp.mat.A zeros(lrp.mat.B0); lrp.mat.C zeros(lrp.mat.D0)];
As2=[zeros(lrp.mat.A) lrp.mat.B0; zeros(lrp.mat.C) lrp.mat.D0];

G1i=zeros(lrp.mat.A);
G2i=zeros(lrp.mat.D0);
N1i=zeros(lrp.mat.B');
N2i=zeros(lrp.mat.D');

rozm=size(lrp.mat.A)+size(lrp.mat.D0);

Pi=zeros(rozm(1),rozm(2));
Qi=zeros(rozm(1),rozm(2));

XListIn=list(G1i,G2i,N1i,N2i,Pi,Qi);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,LMIAlongThePass3_eval,options);
if (iserror()) then

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L3"; ...
    "functionName"; ...
    "solutionExists"],...
    "LMIAlongThePass3", ... // Always the current function name
    %f);    //no solution - false

    lrp.indController=1; // Set to NONE as no controller could be found
    //lrp=setnone(lrp)   // lrp=setnone(lrp) has been called in the beginning of
                         // this function; if not then uncomment this line (IMPORTANT).

    errclear();
else
    [G1,G2,N1,N2,P,Q]=XListOut(:);
    N=blkdiag(N1, N2);
    G=blkdiag(G1, G2);
    K=N*inv(G);

    [n,r]=size(lrp.mat.B);

    K1=K(1:r,1:n);
    K2=K(r+1:$,n+1:$);

    Nd1=[N1 zeros(N2); N1 zeros(N2)];
    Nd2=[zeros(N1) N2; zeros(N1) N2];
    Gd1=blkdiag(G1,zeros(lrp.mat.D0));
    Gd2=blkdiag(zeros(lrp.mat.A), G2);

    //closed loop
    Ad=As1+As2;
    nAd=Ad+Bd*K;
    nA=nAd(1:n,1:n);
    nB0=nAd(1:n,n+1:$);
    nC=nAd(n+1:$,1:n);
    nD0=nAd(n+1:$,n+1:$);

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L3";...
    "functionName"; ...
    "displayName"; ...
    "solutionExists"; ...
    "P";"Q";"G";"G1";"G2";"N";"N1";"N2";"K";"K1";"K2";...
    "Nd1";"Nd2";"Gd1";"Gd2";...
    "As1";"As2";"Ad";"Bd";...
    "newAd";...
    "newA";"newB0";"newC";"newD0"],...
    "LMIAlongThePass3", ...
    "Along the pass stabilising controller 3", ... //DisplayName
    %t, ... //solution exists - true
    P,Q,G,G1,G2,N,N1,N2,K,K1,K2,...
    Nd1,Nd2,Gd1,Gd2,...
    As1,As2,Ad,Bd,...
    nAd,...
    nA,nB0,nC,nD0);

    [lrp]=setLMIAlongThePass3(lrp);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","Ready");
end
endfunction



function [LME,LMI,OBJ]=LMIAlongThePass3_eval(XListIn)
[G1,G2,N1,N2,P,Q]=XListIn(:);

Nd1=[N1 zeros(N2); N1 zeros(N2)];
Nd2=[zeros(N1) N2; zeros(N1) N2];
Gd1=blkdiag(G1,zeros(lrp.mat.D0));
Gd2=blkdiag(zeros(lrp.mat.A), G2);
G=blkdiag(G1, G2);

OBJ=[];
LME=list(P-P',Q-Q');
LMI=list( -[Q-P,             zeros(P)            As1*Gd1+Bd*Nd1; ...
            zeros(Q),        -Q,                 As2*Gd2+Bd*Nd2; ...
            (As1*Gd1+Bd*Nd1)' (As2*Gd2+Bd*Nd2)'  P-G-G']-eye(),...
            P-eye(),...
            Q-eye());
endfunction
