// ====================================================================
// Copyright INRIA 2008
// This file is released into the public domain
// ====================================================================

//current_dir = pwd();
help_dir = get_absolute_file_path('builder_help.sce');
tbx_builder_help_lang("en_US", help_dir);
//tbx_builder_help_lang("fr_FR", help_dir);

clear help_dir;

//cd(fullfile(master_help_dir,'mtx')); exec('builder_help.sce'); cd(master_help_dir);
//cd(fullfile(master_help_dir,'library')); exec('builder_help.sce'); cd(master_help_dir);

//cd(current_dir);
