// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// Simon LIPP
// This file is released into the public domain
// ====================================================================

dos('del mtx\.last_successful_build_javaHelp');
dos('del library\.last_successful_build_javaHelp');
help_lang_dir = get_absolute_file_path('build_help.sce');

getf('C:\Program Files\scilab-5.1\modules\helptools\macros\xmltoformat2.sci');
//tbx_build_help('Library', help_lang_dir);
//exec('mtx\build_help.sce');
//getf('C:\Program Files\scilab-5.1\modules\helptools\macros\xmltoformat2.sci');
//xmltoformat2("javaHelp",[fullfile(help_lang_dir,'library'), fullfile(help_lang_dir,'mtx')] , ['Library','CoolMatrix']);
xmltoformat2("javaHelp",[fullfile(help_lang_dir,'library'), fullfile(help_lang_dir,'mtx')],[]);
//xmltoformat2("javaHelp",help_lang_dir,[]);
clear help_lang_dir;
